﻿using UnityEngine;
using System.Collections;

namespace KJ.Message.Bluetooth
{
    public sealed class ReceiveRawData
    {
        public byte[] raw
        {
            get;
            private set;
        }

        public ReceiveRawData(byte[] i_raw)
        {
            raw = i_raw;
        }
    }

    public sealed class SendRawData
    {
        public byte[] raw
        {
            get;
            private set;
        }

        public SendRawData(byte[] i_raw)
        {
            raw = i_raw;
        }
    }

    public sealed class FoundDevice
    {
        public BluetoothSerialize.FoundDevice foundDevice
        {
            get;
            private set;
        }

        public FoundDevice(string i_json)
        {
            foundDevice = KJ.Serializer.KJDataTableSerializer.LoadTableFromString<BluetoothSerialize.FoundDevice>(i_json);

            if (true == string.IsNullOrEmpty(foundDevice.name))
            {
                foundDevice.name = foundDevice.address;
            }
        }
    }
    public sealed class DiscoveryFinished
    {
    }

    public class ConnectionSuccess
    {
        public string deviceName
        {
            get;
            private set;
        }

        public ConnectionSuccess(string i_deviceName)
        {
            deviceName = i_deviceName;
        }
    }
    public class ConnectionFail
    {
    }
    public class ConnectionLost
    {
    }

	//#modelid
	public class ReceiveBoardType
	{
		public int boardType
		{
			get;
			private set;
		}
		public ReceiveBoardType(int id)
		{
			boardType = id;
		}
	}

    public class EndMemory
    {
    }

    public class ChangeState
    {
        public BluetoothSerialize.ChangeState changeState
        {
            get;
            private set;
        }

        public ChangeState(string i_json)
        {
            changeState = KJ.Serializer.KJDataTableSerializer.LoadTableFromString<BluetoothSerialize.ChangeState>(i_json);
        }
    }

    public interface IReceiveData
    {
        object receiveData
        {
            get;
        }
        string json
        {
            get;
        }
    }
    public abstract class AReceiveData<T> : IReceiveData
    {
        object IReceiveData.receiveData
        {
            get
            {
                return this.receiveData;
            }
        }

        public T receiveData
        {
            get;
            private set;
        }
        public string json
        {
            get;
            private set;
        }

        public AReceiveData(string i_json)
        {
            receiveData = KJ.Serializer.KJDataTableSerializer.LoadTableFromString<T>(i_json);
            json = i_json;
        }
        public AReceiveData(T i_receiveData)
        {
            receiveData = i_receiveData;
        }
    }
    public sealed class ReceiveDataGameBlock : AReceiveData<BluetoothSerialize.GameBlockData>
    {
        public ReceiveDataGameBlock(string i_json)
            : base(i_json)
        {
        }
        public ReceiveDataGameBlock(BluetoothSerialize.GameBlockData i_receiveData)
            : base(i_receiveData)
        {
        }
    }
    public sealed class ReceiveDataThreeDBlock : AReceiveData<BluetoothSerialize.ThreeDBlockData>
    {
        public ReceiveDataThreeDBlock(string i_json)
            : base(i_json)
        {
        }
        public ReceiveDataThreeDBlock(BluetoothSerialize.ThreeDBlockData i_receiveData)
            : base(i_receiveData)
        {
        }
    }
    public sealed class ReceiveDataArtBlock : AReceiveData<BluetoothSerialize.ArtBlockData>
    {
        public ReceiveDataArtBlock(string i_json)
            : base(i_json)
        {
        }
        public ReceiveDataArtBlock(BluetoothSerialize.ArtBlockData i_receiveData)
            : base(i_receiveData)
        {
        }
    }
    public sealed class ReceiveDataRgbBlock : AReceiveData<BluetoothSerialize.RgbBlockData>
    {
        public ReceiveDataRgbBlock(string i_json)
            : base(i_json)
        {
        }
        public ReceiveDataRgbBlock(BluetoothSerialize.RgbBlockData i_receiveData)
            : base(i_receiveData)
        {
        }
    }
    public sealed class ReceiveDataCensorBlock : AReceiveData<BluetoothSerialize.CensorBlockData>
    {
        public ReceiveDataCensorBlock(string i_json)
            : base(i_json)
        {
        }
        public ReceiveDataCensorBlock(BluetoothSerialize.CensorBlockData i_receiveData)
            : base(i_receiveData)
        {
        }
    }

    public sealed class ReceiveDataRemoveBlock : AReceiveData<BluetoothSerialize.RemoveBlockData>
    {
        public ReceiveDataRemoveBlock(string i_json)
            : base(i_json)
        {
        }
        public ReceiveDataRemoveBlock(BluetoothSerialize.RemoveBlockData i_receiveData)
            : base(i_receiveData)
        {
        }
    }

    public sealed class ReceiveDataTouchBlock : AReceiveData<BluetoothSerialize.TouchBlockData>
    {
        public ReceiveDataTouchBlock(string i_json)
            : base(i_json)
        {
        }
        public ReceiveDataTouchBlock(BluetoothSerialize.TouchBlockData i_receiveData)
            : base(i_receiveData)
        {
        }
    }
    /*public sealed class ReceiveDataLcdBlock : AReceiveData<BluetoothSerialize.LcdBlockData>
    {
        public ReceiveDataLcdBlock(string i_json)
            : base(i_json)
        {
        }
        public ReceiveDataLcdBlock(BluetoothSerialize.LcdBlockData i_receiveData)
            : base(i_receiveData)
        {
        }
    }*/
    public sealed class ReceiveDataMotorBlock : AReceiveData<BluetoothSerialize.MotorBlockData>
    {
        public ReceiveDataMotorBlock(string i_json)
            : base(i_json)
        {
        }
        public ReceiveDataMotorBlock(BluetoothSerialize.MotorBlockData i_receiveData)
            : base(i_receiveData)
        {
        }
    }

    //public sealed class ReceiveData
    //{
    //    public BluetoothSerialize.ABlockData receiveData
    //    {
    //        get;
    //        private set;
    //    }

    //    public ReceiveData(BluetoothSerialize.ABlockData i_receiveData)
    //    {
    //        receiveData = i_receiveData;
    //    }
    //}


    public abstract class AReceiveDataPosition
    {
        public int x
        {
            get;
            protected set;
        }
        public int y
        {
            get;
            protected set;
        }
        public int z
        {
            get;
            protected set;
        }

        /// <summary>
        /// signal block id
        /// </summary>
        public int blockID
        {
            get;
            protected set;
        }
    }


    public abstract class AReceiveDataSub : AReceiveDataPosition
    {
        public int at
        {
            get;
            private set;
        }

        /// <summary>
        /// signal sub block id
        /// </summary>
        public int id
        {
            get;
            private set;
        }
        public KJBluetoothValues.BlockState state
        {
            get;
            private set;
        }

        public virtual KJBluetoothValues.SubBlockType type
        {
            get
            {
                return KJBluetoothValues.SubBlockType.None;
            }
        }

        public bool isAttached
        {
            get
            {
                if (KJBluetoothValues.BlockState.On == state)
                {
                    return true;
                }

                return false;
            }
        }

        internal void SetData(int i_x, int i_y, int i_z, int i_blockID, int i_id, KJBluetoothValues.BlockState i_state, int i_at)
        {
            x = i_x;
            y = i_y;
            z = i_z;

            blockID = i_blockID;

            id = i_id;
            state = i_state;

            at = i_at;
        }
    }
    /*public sealed class ReceiveDataPlayer : AReceiveDataSub
    {
        public override KJBluetoothValues.SubBlockType type
        {
            get
            {
                return KJBluetoothValues.SubBlockType.Player;
            }
        }
    }*/

    /*public sealed class ReceiveDataItem : AReceiveDataSub
    {
        public override KJBluetoothValues.SubBlockType type
        {
            get
            {
                return KJBluetoothValues.SubBlockType.Item;
            }
        }
    }
     * */
    public sealed class ReceiveDataSkin : AReceiveDataSub
    {
        public override KJBluetoothValues.SubBlockType type
        {
            get
            {
                return KJBluetoothValues.SubBlockType.Skin;
            }
        }
    }

    public sealed class ReceiveDataRgbColor : AReceiveDataPosition
    {
        public int bright
        {
            get;
            private set;
        }

        public Color32 color
        {
            get;
            private set;
        }

        public ReceiveDataRgbColor(int i_x, int i_y, int i_z, int i_blockID, int i_bright, Color32 i_color)
        {
            x = i_x;
            y = i_y;
            z = i_z;

            blockID = i_blockID;

            bright = i_bright;
            color = i_color;
        }
    }

    public sealed class ReceiveDataTouchState
    {
        public BluetoothSerialize.TouchBlockData receiveTouchBlock
        {
            get;
            private set;
        }

        public ReceiveDataTouchState(BluetoothSerialize.TouchBlockData i_receiveTouchBlock)
        {
            receiveTouchBlock = i_receiveTouchBlock;
        }
    }
    
    public sealed class ReceiveDataMotorState
    {
        public BluetoothSerialize.MotorBlockData receiveMotorBlock
        {
            get;
            private set;
        }

        public ReceiveDataMotorState(BluetoothSerialize.MotorBlockData i_receiveMotorBlock)
        {
            receiveMotorBlock = i_receiveMotorBlock;
        }
    }
    public sealed class ReceiveDataCensorValue
    {
        public BluetoothSerialize.CensorBlockData receiveCensorBlock
        {
            get;
            private set;
        }

        public ReceiveDataCensorValue(BluetoothSerialize.CensorBlockData i_receiveCensorBlock)
        {
            receiveCensorBlock = i_receiveCensorBlock;
        }
    }


    public sealed class AckOk
    {
    }
} // namespace KJ.Message.Bluetooth

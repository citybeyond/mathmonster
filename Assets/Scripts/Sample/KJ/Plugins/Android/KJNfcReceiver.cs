﻿using UnityEngine;
using System.Collections;
using KJ.ExtensionMethods;


public class KJNfcReceiver : MonoBehaviour
{
    private void A_ResponseTagId(string i_tagId)
    {
        KJ.Handler.KJMessageHandler.SendMessage(new KJ.Message.Nfc.ResponseTagId(i_tagId), false);
    }

    private void A_ResponseTagData(string i_recordData)
    {
        KJ.Handler.KJMessageHandler.SendMessage(new KJ.Message.Nfc.ResponseTagData(i_recordData), false);
    }

    private void A_WriteTextResultSuccess(string i_text)
    {
        KJ.Handler.KJMessageHandler.SendMessage(new KJ.Message.Nfc.WriteTextResult(true, i_text), false);
    }
    private void A_WriteTextResultFail(string i_text)
    {
        KJ.Handler.KJMessageHandler.SendMessage(new KJ.Message.Nfc.WriteTextResult(false, i_text), false);
    }
}

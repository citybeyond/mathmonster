﻿using UnityEngine;
using System.Collections;
using KJ.ExtensionMethods;

//BlockDataManager에 함수의 기능 코딩됨.
public class KJBluetoothReceiver : MonoBehaviour
{
    public string deviceName;

    public enum State : int
    {
        NONE = 0,
        LISTEN,
        CONNECTING,
        CONNECTED,
        CONNECTFAIL,
    }

    private void A_ReceiveRawData(string i_message)
    {
		byte[] raw = i_message.ToBytes();
		string temp = System.Text.Encoding.ASCII.GetString(raw);
		Debug.Log ("A_ReceiveRawData "+temp);
		Controller_Moblo.Instance.AddMessage(temp);
        KJ.Handler.KJMessageHandler.SendMessage(new KJ.Message.Bluetooth.ReceiveRawData(raw), false);
    }

    private void A_SendRawData(string i_message)
    {
        byte[] raw = i_message.ToBytes();
        KJ.Handler.KJMessageHandler.SendMessage(new KJ.Message.Bluetooth.SendRawData(raw), false);
    }

    private void A_PrefareFail(string i_message)
    {
    }

    private void A_ConnectionSuccess(string i_deviceName)
    {
        KJ.Handler.KJMessageHandler.SendMessage(new KJ.Message.Bluetooth.ConnectionSuccess(i_deviceName), false);
        BluetoothManager.Instance.BluetoothConnect();
    }
    private void A_ConnectionFail(string i_message)
    {
        KJ.Handler.KJMessageHandler.SendMessage<KJ.Message.Bluetooth.ConnectionFail>(false);
    }
    private void A_ConnectionLost(string i_message)
    {
        KJ.Handler.KJMessageHandler.SendMessage<KJ.Message.Bluetooth.ConnectionLost>(false);
        BluetoothManager.Instance.BluetoothDisConnected();
    }

    private void A_FoundDevice(string i_message)
    {
        KJ.Message.Bluetooth.FoundDevice message = new KJ.Message.Bluetooth.FoundDevice(i_message);
        KJ.Handler.KJMessageHandler.SendMessage(message, false);
    }
    private void A_DiscoveryFinished(string i_message)
    {
        KJ.Handler.KJMessageHandler.SendMessage<KJ.Message.Bluetooth.DiscoveryFinished>(false);
    }
    private void A_ChangeState(string i_json)
    {
        KJ.Message.Bluetooth.ChangeState message = new KJ.Message.Bluetooth.ChangeState(i_json);
        KJ.Handler.KJMessageHandler.SendMessage(message, false);
    }

    private void A_ReceiveDataGameBlock(string i_json)
    {
        KJ.Message.Bluetooth.ReceiveDataGameBlock message = new KJ.Message.Bluetooth.ReceiveDataGameBlock(i_json);
        if (KJBluetoothValues.BlockState.Hold != message.receiveData.blockState)
        {
            KJ.Handler.KJMessageHandler.SendMessage(message, false);
        }

        if (KJBluetoothValues.BlockState.Off != message.receiveData.blockState)
        {
            
            if (KJBluetoothValues.BlockState.Hold != message.receiveData.skinState)
            {
                this.SendReceiveDataSub<KJ.Message.Bluetooth.ReceiveDataSkin>(message.receiveData.x, message.receiveData.y, message.receiveData.z, message.receiveData.blockID, message.receiveData.skinID, message.receiveData.skinState, 0);
            }
        }
    }
    /*private void A_ReceiveDataDummyBlock(string i_json)
    {
        KJ.Message.Bluetooth.ReceiveDataDummyBlock message = new KJ.Message.Bluetooth.ReceiveDataDummyBlock(i_json);
        if (KJBluetoothValues.BlockState.Hold != message.receiveData.blockState)
        {
            KJ.Handler.KJMessageHandler.SendMessage(message, false);
        }

        if (KJBluetoothValues.BlockState.Off != message.receiveData.blockState)
        {
            for (int i = 0; i < message.receiveData.itemID.Length; ++i)
            {
                if (KJBluetoothValues.BlockState.Hold == message.receiveData.itemState[i])
                {
                    continue;
                }

                this.SendReceiveDataSub<KJ.Message.Bluetooth.ReceiveDataItem>(message.receiveData.x, message.receiveData.y, message.receiveData.z, message.receiveData.blockID, message.receiveData.itemID[i], message.receiveData.itemState[i], i);
            }
        }
    }*/


    private void A_ReceiveDataThreeDBlock(string i_json)
    {
        KJ.Message.Bluetooth.ReceiveDataThreeDBlock message = new KJ.Message.Bluetooth.ReceiveDataThreeDBlock(i_json);
        if (KJBluetoothValues.BlockState.Hold != message.receiveData.blockState)
        {
            KJ.Handler.KJMessageHandler.SendMessage(message, false);
        }

        if (KJBluetoothValues.BlockState.Off != message.receiveData.blockState)
        {

        }
    }

    private void A_ReceiveDataArtBlock(string i_json)
    {
        KJ.Message.Bluetooth.ReceiveDataArtBlock message = new KJ.Message.Bluetooth.ReceiveDataArtBlock(i_json);
        if (KJBluetoothValues.BlockState.Hold != message.receiveData.blockState)
        {
            KJ.Handler.KJMessageHandler.SendMessage(message, false);
        }

        if (KJBluetoothValues.BlockState.Off != message.receiveData.blockState)
        {
            
        }
    }

    private void A_ReceiveDataRgbBlock(string i_json)
    {
        KJ.Message.Bluetooth.ReceiveDataRgbBlock message = new KJ.Message.Bluetooth.ReceiveDataRgbBlock(i_json);
        if (KJBluetoothValues.BlockState.Hold != message.receiveData.blockState)
        {
            KJ.Handler.KJMessageHandler.SendMessage(message, false);
        }

        if (KJBluetoothValues.BlockState.Off != message.receiveData.blockState)
        {
            KJ.Message.Bluetooth.ReceiveDataRgbColor message2 = new KJ.Message.Bluetooth.ReceiveDataRgbColor(message.receiveData.x, message.receiveData.y, message.receiveData.z, message.receiveData.blockID, message.receiveData.bright, message.receiveData.color);
            KJ.Handler.KJMessageHandler.SendMessage(message2, false);
        }
    }

    private void A_ReceiveDataCensorBlock(string i_json)
    {
        KJ.Message.Bluetooth.ReceiveDataCensorBlock message = new KJ.Message.Bluetooth.ReceiveDataCensorBlock(i_json);
        if (KJBluetoothValues.BlockState.Hold != message.receiveData.blockState)
        {
            KJ.Handler.KJMessageHandler.SendMessage(message, false);
        }

        if (KJBluetoothValues.BlockState.Off != message.receiveData.blockState)
        {
            KJ.Message.Bluetooth.ReceiveDataCensorValue message2 = new KJ.Message.Bluetooth.ReceiveDataCensorValue(message.receiveData);
            KJ.Handler.KJMessageHandler.SendMessage(message2, false);
        }
    }

    private void A_ReceiveDataRemoveBlock(string i_json)
    {
        KJ.Message.Bluetooth.ReceiveDataRemoveBlock message = new KJ.Message.Bluetooth.ReceiveDataRemoveBlock(i_json);
        KJ.Handler.KJMessageHandler.SendMessage(message, false);
    }

    private void A_ReceiveDataTouchBlock(string i_json)
    {
        KJ.Message.Bluetooth.ReceiveDataTouchBlock message = new KJ.Message.Bluetooth.ReceiveDataTouchBlock(i_json);
        if (KJBluetoothValues.BlockState.Hold != message.receiveData.blockState)
        {
            KJ.Handler.KJMessageHandler.SendMessage(message, false);
        }

        if (KJBluetoothValues.BlockState.Off != message.receiveData.blockState)
        {
            KJ.Message.Bluetooth.ReceiveDataTouchState message2 = new KJ.Message.Bluetooth.ReceiveDataTouchState(message.receiveData);
            KJ.Handler.KJMessageHandler.SendMessage(message2, false);
        }
    }
    /*private void A_ReceiveDataLcdBlock(string i_json)
    {
        KJ.Message.Bluetooth.ReceiveDataLcdBlock message = new KJ.Message.Bluetooth.ReceiveDataLcdBlock(i_json);
        if (KJBluetoothValues.BlockState.Hold != message.receiveData.blockState)
        {
            KJ.Handler.KJMessageHandler.SendMessage(message, false);
        }

        if (KJBluetoothValues.BlockState.Off != message.receiveData.blockState)
        {
            KJ.Message.Bluetooth.ReceiveDataLcdContent message2 = new KJ.Message.Bluetooth.ReceiveDataLcdContent(message.receiveData);
            KJ.Handler.KJMessageHandler.SendMessage(message2, false);
        }
    }*/
    private void A_ReceiveDataMotorBlock(string i_json)
    {
        KJ.Message.Bluetooth.ReceiveDataMotorBlock message = new KJ.Message.Bluetooth.ReceiveDataMotorBlock(i_json);
        if (KJBluetoothValues.BlockState.Hold != message.receiveData.blockState)
        {
            KJ.Handler.KJMessageHandler.SendMessage(message, false);
        }

        if (KJBluetoothValues.BlockState.Off != message.receiveData.blockState)
        {
            KJ.Message.Bluetooth.ReceiveDataMotorState message2 = new KJ.Message.Bluetooth.ReceiveDataMotorState(message.receiveData);
            KJ.Handler.KJMessageHandler.SendMessage(message2, false);
        }
    }

    private void A_AckOk(string i_ignore)
    {
        KJ.Handler.KJMessageHandler.SendMessage<KJ.Message.Bluetooth.AckOk>(false);
    }
    private void A_EndMemory(string i_message)
    {
        KJ.Handler.KJMessageHandler.SendMessage<KJ.Message.Bluetooth.EndMemory>(false);
    }

	private void A_ReceiveDataBoardType(string i_message)
	{
		int msg = int.Parse(i_message);
		KJ.Handler.KJMessageHandler.SendMessage(new KJ.Message.Bluetooth.ReceiveBoardType(msg));
		Debug.Log ("BoardType : "+msg);
		PlayerPrefs.SetInt ("BoardType",msg);
	}

    #region Inner Process
    private void SendReceiveDataSub<T>(int i_x, int i_y, int i_z, int i_blockID, int i_id, KJBluetoothValues.BlockState i_state, int i_at) where T : KJ.Message.Bluetooth.AReceiveDataSub, new()
    {
        T message = new T();
        message.SetData(i_x, i_y, i_z, i_blockID, i_id, i_state, i_at);

        KJ.Handler.KJMessageHandler.SendMessage(message, false);
    }
    #endregion Inner Process
}

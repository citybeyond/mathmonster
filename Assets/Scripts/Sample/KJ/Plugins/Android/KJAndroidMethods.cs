﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class KJAndroidMethods : KJAPluginMethods
{

    private const string ActivityClassName = "com.motionblue.plugins.UnityPlayerNativeActivity";
    private const string ActivityObjectName = "instance";

    private AndroidJavaClass mainActivityClass = null;
    private AndroidJavaObject mainActivity = null;

    public class Bluetooth : KJAPluginMethods.ABluetooth
    {
        private AndroidJavaObject bluetooth = null;

        public override void testSetAsciiCommand(byte[] i_arrBytes)
        {
            bluetooth.Call("testSetAsciiCommand", i_arrBytes);
        }

        public override void requestCommand(KJBluetoothValues.BluetoothCommands i_commandIndex)
        {
            bluetooth.Call("requestCommand", (int)i_commandIndex);
        }

        public override void ensureDiscoverable(int i_duration)
        {
            bluetooth.Call("ensureDiscoverable", i_duration);
        }

        public override void doDiscovery()
        {
            bluetooth.Call("doDiscovery");
        }

        public override void cancelDiscovery()
        {
            bluetooth.Call("cancelDiscovery");
	    }

        public override void openDefaultBluetoothActivity()
        {
            bluetooth.Call("openDefaultBluetoothActivity");
        }

        public override BluetoothSerialize.FoundDevice[] getBondedDevices()
        {
            List<BluetoothSerialize.FoundDevice> listFoundDevice = new List<BluetoothSerialize.FoundDevice>();

            string json = bluetooth.Call<string>("getBondedDevices");
            foreach (BluetoothSerialize.FoundDevice foundDevice in KJ.Serializer.KJDataTableSerializer.LoadTableFromString<BluetoothSerialize.FoundDevice[]>(json))
            {
                /*if (false == foundDevice.name.ToLower().StartsWith("pandora"))
                {
                    continue;
                }*/

                listFoundDevice.Add(foundDevice);
            }

            return listFoundDevice.ToArray();
	    }

        public override string connectDeviceByAddress(string i_address)
        {
            return bluetooth.Call<string>("connectDeviceByAddress", i_address);
	    }

        public override bool isConnected()
        {
            return bluetooth.Call<bool>("isConnected");
	    }

        public override void disconnect()
        {
            bluetooth.Call("stopBluetooth");
	    }

		public override void requestBoardType()
		{
			Debug.Log ("requestBoardType");
			bluetooth.Call ("requestBoardType");
		}

        [System.Obsolete("Not used anymore")]
        public override bool hasBlock()
        {
            return false;
        }

        /*public override void setLed(byte i_x, byte i_y, byte i_z, Color32 i_color, byte i_bright)
        {
            bluetooth.Call("setLED", ++i_x, ++i_y, ++i_z, i_color.r, i_color.g, i_color.b, i_bright);
        }*/

        /*[System.Obsolete("This is no longer in use. Using setLed(...)")]
        public override void setLED(byte i_x, byte i_y, byte i_z, Color32 i_color, byte i_bright)
        {
            this.setLed(i_x, i_y, i_z, i_color, i_bright);
        }*/

        public override void setBlink(byte i_x, byte i_y, byte i_z, KJBluetoothValues.BlinkState i_state, Color32 i_color, byte i_bright, KJBluetoothValues.BlinkTime i_blinkTime, KJBluetoothValues.BlinkInterval i_blinkInterval, KJBluetoothValues.BlinkTarget i_blinkTarget)
        {
            //bluetooth.Call("setBlink", ++i_x, ++i_y, ++i_z, (byte)'A', (byte)i_state, i_color.r, i_color.g, i_color.b, i_bright, (byte)i_blinkTime, (byte)i_blinkInterval, (byte)i_blinkTarget);
        }

        /*[System.Obsolete("setBlink is obsolete. Use another setBlink")]
        public override void setBlink(byte i_x, byte i_y, byte i_z, KJBluetoothValues.BlockType i_blockType, KJBluetoothValues.BlinkState i_state, Color32 i_color, byte i_bright, KJBluetoothValues.BlinkTime i_blinkTime, KJBluetoothValues.BlinkInterval i_blinkInterval, KJBluetoothValues.BlinkTarget i_blinkTarget)
        {
            this.setBlink(i_x, i_y, i_z, i_state, i_color, i_bright, i_blinkTime, i_blinkInterval, i_blinkTarget);
        }*/

        /*public override void setLcd(byte i_x, byte i_y, byte i_z, KJBluetoothValues.LcdContentType i_contentType, char i_content)
        {
            // todo: caof: 2015-02-26: 0~9, a~z, A~Z 범위 체크
            //bluetooth.Call("setLcd", ++i_x, ++i_y, ++i_z, (byte)i_contentType, System.Convert.ToByte(i_content));
        }*/

        public override void setMotor(byte i_x, byte i_y, byte i_z, KJBluetoothValues.MotorDirection i_direction, byte i_speed)
        {
            byte speed = System.Math.Min((byte)3, System.Math.Max((byte)0, i_speed));
            if (i_speed != speed)
            {
                KJ.Utilities.Logger.Print(KJ.Utilities.Logger.E_LOG_LEVEL.WARNING, "KJAndroidMethods.Bluetooth.setMotor: speed out of range, {0} -> {1}", i_speed, speed);
            }

            bluetooth.Call("setMotor", ++i_x, ++i_y, ++i_z, (byte)i_direction, speed);
        }

        public override void setThreed(byte i_x, byte i_y, byte i_z, KJBluetoothValues.BlockType i_blockType, byte i_led, int cardID)
        {
            Debug.Log("setThreed:" + i_x + "," + i_y + "," + i_z + ":" + (byte)i_blockType + "|" + i_led + "|" + cardID);
            bluetooth.Call("setThreed", ++i_x, ++i_y, ++i_z, (byte)i_blockType, i_led, cardID);
        }
        public override void setThreed(byte i_x, byte i_y, byte i_z, KJBluetoothValues.BlockType i_blockType, byte BlockState, byte i_data0, int cardID)
        {
            Debug.Log("setThreed2:" + i_x + "," + i_y + "," + i_z + ":" + (byte)i_blockType + "," + BlockState + "|" + i_data0 + "|" + cardID);
            bluetooth.Call("setThreed", ++i_x, ++i_y, ++i_z, (byte)i_blockType, BlockState, i_data0, cardID);
        }

        public override void setArt(byte i_x, byte i_y, byte i_z, KJBluetoothValues.BlockType i_blockType, byte i_led)
        {
            Debug.Log("setArt:" + i_x + "," + i_y + "," + i_z + ":" + (byte)i_blockType + "|" + i_led);
            bluetooth.Call("setArt", ++i_x, ++i_y, ++i_z, (byte)i_blockType, i_led);
        }
        public override void setRgb(byte i_x, byte i_y, byte i_z, Color32 i_color, byte i_bright)
        {
            bluetooth.Call("setRgb", ++i_x, ++i_y, ++i_z, i_color.r, i_color.g, i_color.b, i_bright);
        }

        public override void WriteDeviceName(string deviceName)
        {
            bluetooth.Call("WriteDeviceName", deviceName);
        }


        public Bluetooth(AndroidJavaObject i_mainActivity)
        {
            bluetooth = i_mainActivity.Get<AndroidJavaObject>("bluetooth");
        }
        ~Bluetooth()
        {
            KJ.Utilities.Common.SafeDispose(ref bluetooth);
        }
    }

    public class Nfc : KJAPluginMethods.ANfc
    {
        private AndroidJavaObject nfc = null;

        public override void writeText(string i_text)
        {
            nfc.Call("writeText", i_text);
        }

        public Nfc(AndroidJavaObject i_mainActivity)
        {
            nfc = i_mainActivity.Get<AndroidJavaObject>("nfc");
        }
        ~Nfc()
        {
            KJ.Utilities.Common.SafeDispose(ref nfc);
        }
    }


    protected override KJAPluginMethods.ABluetooth CreateBluetooth()
    {
        return new Bluetooth(mainActivity);
    }
    protected override KJAPluginMethods.ANfc CreateNfc()
    {
        return new Nfc(mainActivity);
    }

    public override void Initialize()
    {
        mainActivityClass = new AndroidJavaClass(ActivityClassName);
        mainActivity = mainActivityClass.CallStatic<AndroidJavaObject>(ActivityObjectName);
    }

    ~KJAndroidMethods()
    {
        KJ.Utilities.Common.SafeDispose(ref mainActivity);
        KJ.Utilities.Common.SafeDispose(ref mainActivityClass);
    }


}

﻿using UnityEngine;
using System.Collections;


#if UNITY_EDITOR
using TargetMethods = KJEmptyMethods;
#elif UNITY_ANDROID
using TargetMethods = KJAndroidMethods;
#else
using TargetMethods = KJEmptyMethods;
#endif

public static class KJPluginMethods
{
    [System.Obsolete("This is no longer in use. Using KJBluetoothValues", true)]
    public static class Bluetooth
    {
        [System.Obsolete("This is no longer in use. Using KJBluetoothValues", true)]
        public enum BluetoothCommands : int
        {
            Loop = 1,
            MemoryRead,
            MemoryReadAndLoop,
            Wait,
        }

        [System.Obsolete("This is no longer in use. Using KJBluetoothValues", true)]
        public enum BlockCategory : int
        {
            None = -1,

            GameNfc = 0,
            Dummy,
            Led,
            Special,
        }
        [System.Obsolete("This is no longer in use. Using KJBluetoothValues", true)]
        public enum BlockType : int
        {
            None = -1,

            GameNfc = 'N',

            DummyRed = 'R',
            DummyGreen = 'G',
            DummyBlue = 'B',
            DummyYellow = 'Y',
            DummyViolet = 'V',

            Led = 'D',

            DummyLongRed = 'H',
            DummyLongGreen = 'I',
            DummyLongBlue = 'J',
            DummyLongYellow = 'K',
            DummyLongViolet = 'Q',

            SpecialMotor = 'M',
	        SpecialLcd = 'L',
	        SpecialTouch = 'T',
        }
        [System.Obsolete("This is no longer in use. Using KJBluetoothValues", true)]
        public enum SubBlockType : int
        {
            None = -1,

            Player = 0,
            Item,
            Skin,
        }
        [System.Obsolete("This is no longer in use. Using KJBluetoothValues", true)]
        public enum SubItemBlockType : int
        {
            None = -1,

            Green = 0,
            Blue,
            Red,
            Yellow,
            Violet,

            RoofGreen,
            RoofBlue,
            RoofRed,
            RoofYellow,
            RoofViolet,
        }
        [System.Obsolete("This is no longer in use. Using KJBluetoothValues", true)]
        public enum SubItemBlockCategory : int
        {
            Default = 0,
            Roof,
        }

        [System.Obsolete("This is no longer in use. Using KJBluetoothValues", true)]
        public enum BlockState : int
        {
            On = 1,
            Off,
            Hold,
        }

        [System.Obsolete("This is no longer in use. Using KJBluetoothValues", true)]
        public enum BlinkState : byte
        {
            Default = 3,
            Stop,
        }

        [System.Obsolete("This is no longer in use. Using KJBluetoothValues", true)]
        public enum BlinkTime : byte
        {
            Seconds_1 = 1,
            Seconds_2,
            Seconds_3,
            Seconds_4,
            Seconds_5,
            Seconds_6,
            Seconds_7,
            Seconds_8,
            Seconds_Unlimited,
        }

        [System.Obsolete("This is no longer in use. Using KJBluetoothValues", true)]
        public enum BlinkInterval : byte
        {
            MilliSeconds_100 = 1,
            MilliSeconds_300,
            /// <summary>
            /// default
            /// </summary>
            MilliSeconds_500,
            KeepOn,
        }

        [System.Obsolete("This is no longer in use. Using KJBluetoothValues", true)]
        public enum BlinkTarget : byte
        {
            Board = 0,
            /// <summary>
            /// Act on Block Type
            /// </summary>
            TargetBlock,
            NfcBlocks,
            DummyBlocks,
            LedBlocks,
            NfcAndDummyBlocks,
            NfcAndLedBlocks,
            DummyAndLedBlocks,
            NfcDummyAndLedBlocks,
            All,
        }
    }

    private static TargetMethods _methods = null;
    public static TargetMethods methods
    {
        get
        {
            if (null == _methods)
            {
                _methods = new TargetMethods();
                _methods.Initialize();
            }

            return _methods;
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public static partial class KJBluetoothValues
{
    public enum BlockType : int
    {
        None = -1,

        GameNfc = 'N',


        SpecialMotor = 'M',
        SpecialTouch = 'T',

        ThreeRed = 'R',
        ThreeGreen = 'G',
	    ThreeBlue = 'B',
	    ThreeYellow = 'Y',
	    ThreeViolet = 'V',
	    ThreeOrange = 'O',
	    ThreeIndigo = 'I',
        
        TowerRed = 'Z',
        TowerGreen = 'X',
        TowerBlue = 'W',
        TowerYellow = 'P',
        TowerViolet = 'Q',
        TowerOrange = 'U',
        TowerIndigo = 'K',
	
	    ArtRed = 'r',
	    ArtGreen = 'g',
	    ArtBlue = 'b',
	    ArtYellow = 'y',
	    ArtViolet = 'v',
	    ArtOrange = 'o',
	    ArtIndigo = 'i',
        Rgb = 'C',
        Censor = 'S',
    }
}

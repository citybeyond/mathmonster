﻿using UnityEngine;
using System.Collections;

public static partial class KJBluetoothValues
{
    public enum BlinkInterval : byte
    {
        MilliSeconds_100 = 1,
        MilliSeconds_300,
        /// <summary>
        /// default
        /// </summary>
        MilliSeconds_500,
        KeepOn,
    }
}

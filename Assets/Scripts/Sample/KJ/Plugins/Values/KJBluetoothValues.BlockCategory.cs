﻿using UnityEngine;
using System.Collections;

public static partial class KJBluetoothValues
{
    public enum BlockCategory : int
    {
        None = -1,

        GameNfc = 0,
        //Dummy,
        //Led,
        
        ThreeD,
        Art,
        Rgb,

        Special,

    }
}

﻿using UnityEngine;
using System.Collections.Generic;

public class BTPopupBluetoothView : BTPopup, KJ.Handler.KJMessageHandler.IMessageReceiver
{
    public UIPanel ScrollviewPanel;
    [SerializeField]
    private UIGrid uiGridList = null;

    [SerializeField]
    private GameObject prefabListItem = null;

    [SerializeField]
    private UIButton uiButtonSearch = null;

    [SerializeField]
    private UIButton uiButtonCancel = null;

    List<BluetoothSerialize.FoundDevice> listFoundDevices = new List<BluetoothSerialize.FoundDevice>();

    public GameObject KJBluetoothReceiver; 


    void Start()
    {
        ScrollviewPanel.depth = BluetoothManager.Instance.NextDepth();
        Debug.Log("Start");
        
        //NGUITools.SetActiveSelf(goRootView, false);
        KJBluetoothReceiver = GameObject.Find("KJBluetoothReceiver");

        DoLoadData();

        DoOpen();
    }

    void DoLoadData()
    {
        Debug.Log("DoLoadData");
        listFoundDevices = new List<BluetoothSerialize.FoundDevice>(KJPluginMethods.methods.bluetooth.getBondedDevices());
        this.CreateListItem();
    }
    void DoSaveData()
    {
        
    }

    void DoOpen()
    {
        //gameObject.GetComponent<UIPanel>().alpha = 1f;
        Debug.Log("DoOpen");
        this._RegistMessage();

        if (uiGridList == null)
        {
            uiGridList = transform.FindChild("BluetoothList/Scroll View_list/Grid").GetComponent<UIGrid>();
            Debug.Log("DoOpen : " + uiGridList);

        }
        Debug.Log ("DoOpen : " + uiGridList.transform.childCount);
        if (uiGridList.transform.childCount > 0)
        {
            for (int i = 0; i < uiGridList.transform.childCount; i++)
            {
                GameObject go = uiGridList.transform.GetChild(i).gameObject;
                Destroy (go);
            }
        }

        //KJ.Utilities.GameObjectUtility.DestroyAllChildren(uiGridList.gameObject);

        this.AddFoundedDevices();
    }
    void DoClose()
    {
        Debug.Log("DoClose");
        this._UnregistMessage();
    }

    void AddFoundedDevices()
    {
        for (int i = 0; i < listFoundDevices.Count; ++i)
        {
            this.AddListItem(listFoundDevices[i]);
        }

        uiGridList.repositionNow = true;
    }
    void AddListItem(BluetoothSerialize.FoundDevice i_foundDevice)
    {

        if (uiGridList == null)
        {
            Debug.Log("AddListItem 1: uiGridList null");

            return;
        }

        GameObject item = NGUITools.AddChild(uiGridList.gameObject, prefabListItem);
        Debug.Log("AddListItem 1: " + item);
        item.name = i_foundDevice.address;
        item.GetComponentInChildren<UILabel>().text = i_foundDevice.name;

        EventDelegate.Add(item.GetComponent<UIButton>().onClick, this.OnClickButtonListItem);

        uiGridList.repositionNow = true;
    }

    #region On Message
    void OnMessageFoundDevice(KJ.Message.Bluetooth.FoundDevice i_message)
    {
        if (i_message == null)
        {
            Debug.Log("OnMessageFoundDevice : null");
            return;
        }
        Debug.Log("OnMessageFoundDevice : " + i_message.foundDevice);
        

        this.AddListItem(i_message.foundDevice);
    }
    void OnMessageDiscoveryFinished(KJ.Message.Bluetooth.DiscoveryFinished i_message)
    {
        NGUITools.SetActiveSelf(uiButtonSearch.gameObject, true);
        NGUITools.SetActiveSelf(uiButtonCancel.gameObject, false);
    }

    public void _RegistMessage()
    {
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.FoundDevice>(this.OnMessageFoundDevice);
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.DiscoveryFinished>(this.OnMessageDiscoveryFinished);
    }
    public void _UnregistMessage()
    {
        KJ.Handler.KJMessageHandler.UnregistMessage(this);
    }
    #endregion On Message

    int iiii = 0;
    #region On UI
    public void OnClickButtonSearch()
    {
        Debug.Log("OnClickButtonSearch");
        if (uiGridList.transform.childCount > 0)
        {
            for (int i = 0; i < uiGridList.transform.childCount; i++)
            {
                GameObject go = uiGridList.transform.GetChild(i).gameObject;
                Destroy(go);
            }
        }

        this.AddFoundedDevices();
        //uiButtonSearch.gameObject.SetActive(false);
        uiButtonCancel.gameObject.SetActive(true);



        KJPluginMethods.methods.bluetooth.doDiscovery();


        /*if (iiii % 2 == 0)
        {
            _UnregistMessage();
        }
        else
        {
            _RegistMessage();
        }
        iiii++;*/
    }
    public void OnClickButtonCancel()
    {
        Debug.Log("OnClickButtonCancel");
        //uiButtonSearch.gameObject.SetActive(true);
        uiButtonCancel.gameObject.SetActive(false);

        KJPluginMethods.methods.bluetooth.cancelDiscovery();
    }


    private void OnClickButtonListItem()
    {
        this._UnregistMessage();
        //ResultData resultData = new ResultData();
        //resultData.str = UIButton.current.name;
        //KJBluetoothReceiver.GetComponent<KJBluetoothReceiver>().deviceName = UIButton.current.GetComponentInChildren<UILabel>().text;

        PlayerPrefs.SetString("deviceName", UIButton.current.GetComponentInChildren<UILabel>().text);
        PlayerPrefs.SetString("address", UIButton.current.name);

        CloseBTPopup();
        GameObject o = BluetoothManager.Instance.CreateBTPopup("BTPopupBluetoothConnectingView");
        
    }

    public void onClickExit()
    {
        CloseBTPopup();
        //BluetoothManager.Instance.CreateBTPopup("BTPopupQuit");
    }

    #endregion On UI


    #region Condition Debug
    [System.Diagnostics.Conditional("UNITY_EDITOR")]
    void CreateListItem()
    {
        BluetoothSerialize.FoundDevice foundDevice = new BluetoothSerialize.FoundDevice();
        foundDevice.name = "TEST";
        foundDevice.address = "TEST_ADDRESS";
        listFoundDevices.Add(foundDevice);
    }
    #endregion Condition Debug
}

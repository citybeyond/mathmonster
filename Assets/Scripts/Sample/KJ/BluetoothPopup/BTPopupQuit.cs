﻿using UnityEngine;
using System.Collections;

public class BTPopupQuit : BTPopup {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void onClickExitOK()
    {
        Application.Quit();
    }
    public void onClickExitNo()
    {
        CloseBTPopup();
    }

}

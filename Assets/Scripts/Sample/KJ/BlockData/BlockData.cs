﻿using UnityEngine;
using System.Collections.Generic;


public class BlockCommonData
{
    /// <summary>
    /// signal id
    /// </summary>
    public int id = BlockDataManager.s_invalidID;

    public KJBluetoothValues.BlockType type = KJBluetoothValues.BlockType.None;
    public KJBluetoothValues.BlockCategory category = KJBluetoothValues.BlockCategory.None;
    public KJBluetoothValues.BlockState state = KJBluetoothValues.BlockState.On;

    public int x = BlockDataManager.s_invalidRow;
    public int y = BlockDataManager.s_invalidColumn;
    public int z = BlockDataManager.s_invalidLayer;

    public bool isAttached = false;

    // note: only test
    public bool isInfiniteBlink = false;


    /// <summary>
    /// block game object
    /// </summary>
    [LitJson.JsonMapper.Ignore]
    public GameObject goBlock = null;


    /// <summary>
    /// get sub block data
    /// </summary>
    /// <param name="i_type">sub block type</param>
    /// <param name="i_at">only using item block(4 direction)</param>
    /// <returns></returns>
    public virtual SubBlockCommonData FindSubBlockData(KJBluetoothValues.SubBlockType i_type, int i_at)
    {
        return null;
    }
    public SubBlockCommonData FindSubBlockData(KJBluetoothValues.SubBlockType i_type)
    {
        return this.FindSubBlockData(i_type, 0);
    }

    public virtual bool HasAnySubBlockData()
    {
        return false;
    }

    public virtual void Clear()
    {
        id = BlockDataManager.s_invalidID;

        x = BlockDataManager.s_invalidRow;
        y = BlockDataManager.s_invalidColumn;
        z = BlockDataManager.s_invalidLayer;

        isAttached = false;

        goBlock = null;
    }
}
public class SubBlockCommonData
{
    public BlockCommonData mainBlockData = null;

    /// <summary>
    /// signal id
    /// </summary>
    public int id = BlockDataManager.s_invalidID;

    public KJBluetoothValues.SubBlockType type = KJBluetoothValues.SubBlockType.None;

    /// <summary>
    /// only use by item block
    /// </summary>
    [LitJson.JsonMapper.Ignore]
    public int index = 0;

    public bool isAttached = false;

    /// <summary>
    /// block game object
    /// </summary>
    [LitJson.JsonMapper.Ignore]
    public GameObject goBlock = null;


    public virtual void Clear()
    {
        id = BlockDataManager.s_invalidID;

        type = KJBluetoothValues.SubBlockType.None;

        isAttached = false;

        goBlock = null;
    }

    public SubBlockCommonData()
    {
    }
    public SubBlockCommonData(int i_index)
    {
        index = i_index;
    }
}
/*
public sealed class DummyBlockData : BlockCommonData
{
    public SubBlockCommonData[] arrSubBlockCommonData = new SubBlockCommonData[] {
        new SubItemBlockData(0),
        new SubItemBlockData(1),
        new SubItemBlockData(2),
        new SubItemBlockData(3),
    };


    public override SubBlockCommonData FindSubBlockData(KJBluetoothValues.SubBlockType i_type, int i_at)
    {
        if (KJBluetoothValues.SubBlockType.Item == i_type)
        {
            if (arrSubBlockCommonData.Length <= i_at)
            {
                KJ.Utilities.Logger.Print(KJ.Utilities.Logger.E_LOG_LEVEL.ERROR, "out of range: {0}", i_at);
                return null;
            }

            return arrSubBlockCommonData[i_at];
        }

        KJ.Utilities.Logger.Print(KJ.Utilities.Logger.E_LOG_LEVEL.ERROR, "not supported sub block: {0}", i_type);
        return null;
    }

    public override bool HasAnySubBlockData()
    {
        foreach (SubBlockCommonData sub in arrSubBlockCommonData)
        {
            if (true == sub.isAttached)
            {
                return true;
            }
        }

        return false;
    }

    public override void Clear()
    {
        base.Clear();

        foreach (SubBlockCommonData data in arrSubBlockCommonData)
        {
            data.Clear();
        }
    }
}
*/

public sealed class GameBlockData : BlockCommonData
{
    /*public SubBlockCommonData playerBlockData = new SubBlockCommonData();
    
    public SubBlockCommonData[] arrSubBlockCommonData = new SubBlockCommonData[] {
        new SubItemBlockData(0),
        new SubItemBlockData(1),
        new SubItemBlockData(2),
        new SubItemBlockData(3),
    };*/

    public SubBlockCommonData skinBlockData = new SubBlockCommonData();


    public override SubBlockCommonData FindSubBlockData(KJBluetoothValues.SubBlockType i_type, int i_at)
    {
        /*if (KJBluetoothValues.SubBlockType.Player == i_type)
        {
            return playerBlockData;
        }
        else if (KJBluetoothValues.SubBlockType.Item == i_type)
        {
            if (arrSubBlockCommonData.Length <= i_at)
            {
                KJ.Utilities.Logger.Print(KJ.Utilities.Logger.E_LOG_LEVEL.ERROR, "out of range: {0}", i_at);
                return null;
            }

            return arrSubBlockCommonData[i_at];
        }
        else if (KJBluetoothValues.SubBlockType.Skin == i_type)
        {
            return skinBlockData;
        }*/
        if (KJBluetoothValues.SubBlockType.Skin == i_type)
        {
            return skinBlockData;
        }

        KJ.Utilities.Logger.Print(KJ.Utilities.Logger.E_LOG_LEVEL.ERROR, "not supported sub block: {0}", i_type);
        return null;
    }

    public override bool HasAnySubBlockData()
    {
        /*if (true == playerBlockData.isAttached || true == skinBlockData.isAttached)
        {
            return true;
        }
        foreach (SubBlockCommonData sub in arrSubBlockCommonData)
        {
            if (true == sub.isAttached)
            {
                return true;
            }
        }*/
        if ( true == skinBlockData.isAttached)
        {
            return true;
        }

        return false;
    }

    public override void Clear()
    {
        base.Clear();

        /*playerBlockData.Clear();

        foreach (SubBlockCommonData data in arrSubBlockCommonData)
        {
            data.Clear();
        }*/

        skinBlockData.Clear();
    }
}

/*
public sealed class LedBlockData : BlockCommonData
{
    public static readonly Color32 s_defaultColor = new Color32(0, 0, 0, 255);

    public Color32 color = s_defaultColor;

    /// <summary>
    /// 0~9, 0: off, default: 5
    /// </summary>
    public int bright = 0;

    public bool isOn
    {
        get
        {
            return (0 != bright);
        }
    }


    public override void Clear()
    {
        base.Clear();

        color = s_defaultColor;
        bright = 0;
    }
}
*/

public sealed class ThreeDBlockData : BlockCommonData
{
    public byte ledState = 2;
    public int cardID = 0;
}

public sealed class ArtBlockData : BlockCommonData
{
    public byte ledState = 2;
    
}

public sealed class CensorBlockData : BlockCommonData
{
    public byte centimeter = 0;

}

public sealed class RgbBlockData : BlockCommonData
{
    public static readonly Color32 s_defaultColor = new Color32(0, 0, 0, 255);

    public Color32 color = s_defaultColor;

    /// <summary>
    /// 0~9, 0: off, default: 5
    /// </summary>
    public int bright = 0;

    public bool isOn
    {
        get
        {
            return (0 != bright);
        }
    }


    public override void Clear()
    {
        base.Clear();

        color = s_defaultColor;
        bright = 0;
    }
}



public sealed class TouchBlockData : BlockCommonData
{
    /// <summary>
    /// 5 direction touch state
    /// </summary>
    public KJBluetoothValues.TouchState[] directionStates = new KJBluetoothValues.TouchState[0];
    public KJBluetoothValues.TouchState[] prevDirectionStates = new KJBluetoothValues.TouchState[0];

    public bool this[KJBluetoothValues.TouchDirection i_direction]
    {
        get
        {
            return (KJBluetoothValues.TouchState.On == directionStates[(int)i_direction - 1]);
        }
    }
}




public sealed class MotorBlockData : BlockCommonData
{
    public KJBluetoothValues.MotorDirection direction = KJBluetoothValues.MotorDirection.Stop;
    /// <summary>
    /// range in 1 ~ 3
    /// </summary>
    private byte _speed = 0;
    public byte speed
    {
        get
        {
            return _speed;
        }
        set
        {
            _speed = System.Math.Min((byte)3, System.Math.Max((byte)1, value));
        }
    }
}




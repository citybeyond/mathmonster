﻿using UnityEngine;
using System.Collections.Generic;


public interface IBlockDataReceiver
{
    void AddGameBlock(GameBlockData i_blockData);
    void AddTouchBlock(TouchBlockData i_blockData);
    void AddMotorBlock(MotorBlockData i_blockData);
    void AddThreeDBlock(ThreeDBlockData i_blockData);
    void AddArtBlock(ArtBlockData i_blockData);
    void AddRgbBlock(RgbBlockData i_blockData);
    void AddCensorBlock(CensorBlockData i_blockData);
    //void AddUndefineBlock(UndefineBlockData i_blockData);

    void RemoveGameBlock(BlockCommonData i_blockData);
    void RemoveTouchBlock(BlockCommonData i_blockData);
    void RemoveMotorBlock(BlockCommonData i_blockData);
    void RemoveThreeDBlock(BlockCommonData i_blockData);
    void RemoveArtBlock(BlockCommonData i_blockData);
    void RemoveRgbBlock(BlockCommonData i_blockData);
    void RemoveCensorBlock(BlockCommonData i_blockData);


    void AddSkinBlock(SubBlockCommonData i_subBlockData);
    void RemoveSkinBlock(SubBlockCommonData i_subBlockData);


    void ChangeTouchState(TouchBlockData i_blockData);
    void ChangeMotorState(MotorBlockData i_blockData);
    void ChangeRgbColor(RgbBlockData i_blockData);
    void ChangeCensorValue(CensorBlockData i_blockData);
}

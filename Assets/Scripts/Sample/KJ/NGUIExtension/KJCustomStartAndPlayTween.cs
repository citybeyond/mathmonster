﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("KJ/Custom-NGUI/Tween/Play At Start")]
public class KJCustomStartAndPlayTween : MonoBehaviour
{
    [SerializeField]
    private GameObject tweenTarget = null;

    [SerializeField]
    private int tweenGroup = 0;

    [SerializeField]
    private AnimationOrTween.Direction playDirection = AnimationOrTween.Direction.Forward;

    [SerializeField]
    private AnimationOrTween.EnableCondition ifDisabledOnPlay = AnimationOrTween.EnableCondition.DoNothing;

    [SerializeField]
    public AnimationOrTween.DisableCondition disableWhenFinished = AnimationOrTween.DisableCondition.DoNotDisable;

    [SerializeField]
    private bool includeChildren = false;

    [SerializeField]
    private bool resetOnPlay = false;

    [SerializeField]
    private bool resetIfDisabled = false;

    void Start()
    {
        this.Play(true);
    }

	public void Play(bool forward)
    {
        GameObject go = (tweenTarget == null) ? gameObject : tweenTarget;

        if (!NGUITools.GetActive(go))
        {
            // If the object is disabled, don't do anything
            if (ifDisabledOnPlay != AnimationOrTween.EnableCondition.EnableThenPlay) return;

            // Enable the game object before tweening it
            NGUITools.SetActive(go, true);
        }

        // Gather the tweening components
        UITweener[] tweens = includeChildren ? go.GetComponentsInChildren<UITweener>() : go.GetComponents<UITweener>();

        if (tweens.Length == 0)
        {
            // No tweeners found -- should we disable the object?
            if (disableWhenFinished != AnimationOrTween.DisableCondition.DoNotDisable)
                NGUITools.SetActive(tweenTarget, false);
        }
        else
        {
            bool activated = false;
            if (playDirection == AnimationOrTween.Direction.Reverse) forward = !forward;

            // Run through all located tween components
            for (int i = 0, imax = tweens.Length; i < imax; ++i)
            {
                UITweener tw = tweens[i];

                // If the tweener's group matches, we can work with it
                if (tw.tweenGroup == tweenGroup)
                {
                    // Ensure that the game objects are enabled
                    if (!activated && !NGUITools.GetActive(go))
                    {
                        activated = true;
                        NGUITools.SetActive(go, true);
                    }

                    // Toggle or activate the tween component
                    if (playDirection == AnimationOrTween.Direction.Toggle)
                    {
                        tw.Toggle();
                    }
                    else
                    {
                        if (resetOnPlay || (resetIfDisabled && !tw.enabled)) tw.ResetToBeginning();
                        tw.Play(forward);
                    }
                }
            }
        }
	}
}

﻿using UnityEngine;
using System.Collections;


[AddComponentMenu("KJ/Custom-NGUI/UI/Localize Button")]
[RequireComponent(typeof(UIButton))]
public class KJCustomLocalizeButton : MonoBehaviour
{
    /// <summary>
    /// Localization key.
    /// </summary>
    public string key;
    public string keyHover;
    public string keyPressed;
    public string keyDisabled;

    /// <summary>
    /// Manually change the value of whatever the localization component is attached to.
    /// </summary>
    public string value
    {
        set
        {
            if (!string.IsNullOrEmpty(value))
            {
                UIButton button = GetComponent<UIButton>();
                button.normalSprite = value;

#if UNITY_EDITOR
                if (!Application.isPlaying) NGUITools.SetDirty(button);
#endif
            }
        }
    }
    public string valueHover
    {
        set
        {
            if (!string.IsNullOrEmpty(value))
            {
                UIButton button = GetComponent<UIButton>();
                button.hoverSprite = value;

#if UNITY_EDITOR
                if (!Application.isPlaying) NGUITools.SetDirty(button);
#endif
            }
        }
    }
    public string valuePressed
    {
        set
        {
            if (!string.IsNullOrEmpty(value))
            {
                UIButton button = GetComponent<UIButton>();
                button.pressedSprite = value;

#if UNITY_EDITOR
                if (!Application.isPlaying) NGUITools.SetDirty(button);
#endif
            }
        }
    }
    public string valueDisabled
    {
        set
        {
            if (!string.IsNullOrEmpty(value))
            {
                UIButton button = GetComponent<UIButton>();
                button.disabledSprite = value;

#if UNITY_EDITOR
                if (!Application.isPlaying) NGUITools.SetDirty(button);
#endif
            }
        }
    }

    bool mStarted = false;

    /// <summary>
    /// Localize the widget on enable, but only if it has been started already.
    /// </summary>
    void OnEnable()
    {
#if UNITY_EDITOR
        if (!Application.isPlaying) return;
#endif
        if (mStarted) OnLocalize();
    }

    /// <summary>
    /// Localize the widget on start.
    /// </summary>
    void Start()
    {
#if UNITY_EDITOR
        if (!Application.isPlaying) return;
#endif
        mStarted = true;
        OnLocalize();
    }

    /// <summary>
    /// This function is called by the Localization manager via a broadcast SendMessage.
    /// </summary>
    void OnLocalize()
    {
        // If we still don't have a key, leave the value as blank
        if (!string.IsNullOrEmpty(key))
        {
            string temp = Localization.Get(key);
            if (key != temp)
            {
                value = temp;
            }
        }
        if (!string.IsNullOrEmpty(keyHover))
        {
            string temp = Localization.Get(keyHover);
            if (keyHover != temp)
            {
                valueHover = temp;
            }
        }
        if (!string.IsNullOrEmpty(keyPressed))
        {
            string temp = Localization.Get(keyPressed);
            if (keyPressed != temp)
            {
                valuePressed = temp;
            }
        }
        if (!string.IsNullOrEmpty(keyDisabled))
        {
            string temp = Localization.Get(keyDisabled);
            if (keyDisabled != temp)
            {
                valueDisabled = temp;
            }
        }
    }
}

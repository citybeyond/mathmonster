﻿using UnityEngine;
using System.Collections;

public class KJCustomRepeatButton : UIButton
{
    [SerializeField]
    private float delay = 1.0f;

    [SerializeField]
    private float interval = 0.25f;

    bool isPressed = false;
    float nextClick = 0.0f;


    protected override void OnPress(bool i_isPressed)
    {
        base.OnPress(i_isPressed);

        isPressed = i_isPressed;
        nextClick = Time.realtimeSinceStartup + delay;

        if (true == isPressed)
        {
            this.PerformAction();
        }
    }

    void Update()
    {
        // Adjusted condition slightly...
        if (isPressed && Time.realtimeSinceStartup >= nextClick)
        {
            nextClick = Time.realtimeSinceStartup + interval;
            this.PerformAction();
        }
    }

    void PerformAction()
    {
        this.OnClick();
    }
}

﻿using UnityEngine;
using System.Collections;


[AddComponentMenu("KJ/Custom-NGUI/UI/Localize Tween Count")]
[RequireComponent(typeof(KJCustomTweenCount))]
public class KJCustomLocalizeTweenCount : MonoBehaviour
{
    /// <summary>
    /// Localization key.
    /// </summary>
    public string key;

    /// <summary>
    /// Manually change the value of whatever the localization component is attached to.
    /// </summary>
    public string value
    {
        set
        {
            if (!string.IsNullOrEmpty(value))
            {
                KJCustomTweenCount ui = GetComponent<KJCustomTweenCount>();
                ui.Postfix = value;

#if UNITY_EDITOR
                if (!Application.isPlaying) NGUITools.SetDirty(ui);
#endif
            }
        }
    }

    bool mStarted = false;

    /// <summary>
    /// Localize the widget on enable, but only if it has been started already.
    /// </summary>
    void OnEnable()
    {
#if UNITY_EDITOR
        if (!Application.isPlaying) return;
#endif
        if (mStarted) OnLocalize();
    }

    /// <summary>
    /// Localize the widget on start.
    /// </summary>
    void Start()
    {
#if UNITY_EDITOR
        if (!Application.isPlaying) return;
#endif
        mStarted = true;
        OnLocalize();
    }

    /// <summary>
    /// This function is called by the Localization manager via a broadcast SendMessage.
    /// </summary>
    void OnLocalize()
    {
        // If we still don't have a key, leave the value as blank
        if (!string.IsNullOrEmpty(key))
        {
            string temp = Localization.Get(key);
            if (key != temp)
            {
                value = temp;
            }
        }
    }
}

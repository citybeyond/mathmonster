﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(UIPlayTween))]
[AddComponentMenu("KJ/Custom-NGUI/Tween/Play Tween Helper")]
public class KJCustomPlayTweenHelper : MonoBehaviour
{
    UIPlayTween uiPlayTween;

    void Awake()
    {
        uiPlayTween = this.GetComponent<UIPlayTween>();
    }

    public void OnChangeValueToggle()
    {
        uiPlayTween.Play(UIToggle.current.value);
    }
}

﻿using UnityEngine;
using System.Collections;

public class LoadingView : KJ.Model.KJAViewController, KJ.Handler.KJMessageHandler.IMessageReceiver
{
    UIProgressBar uiProgressBar = null;

    bool startLoading = false;

    protected override void Start()
    {
        base.Start();

        this._RegistMessage();
        
        NGUITools.SetActiveSelf(goRootView, false);
    }
    protected override void OnDestroy()
    {
        this._UnregistMessage();
        base.OnDestroy();
    }

    protected override void DoLoadData()
    {
        UIRoot uiRoot = GameObject.FindObjectOfType<UIRoot>();
        KJ.Utilities.GameObjectUtility.AttachChild(uiRoot.gameObject, transform);
    }
    protected override void DoSaveData()
    {
    }

    protected override void DoOpen()
    {
        NGUITools.SetActiveSelf(goRootView, true);

        if (null == uiProgressBar)
        {
            uiProgressBar = goRootView.GetComponentInChildren<UIProgressBar>();
        }
        uiProgressBar.value = 0;

        startLoading = false;
    }
    protected override void DoClose()
    {
        NGUITools.SetActiveSelf(goRootView, false);
    }

    #region On Message
    void OnMessageLoadSceneStart(KJ.Handler.Message.Scene.LoadSceneStart i_message)
    {
        KJ.Handler.Message.Scene.PushView message = new KJ.Handler.Message.Scene.PushView(this);
        KJ.Handler.KJMessageHandler.SendMessage(message);

        startLoading = true;
    }
    void OnMessageLoadSceneProgress(KJ.Handler.Message.Scene.LoadSceneProgress i_message)
    {
        if (false == startLoading || null == uiProgressBar)
        {
            return;
        }

        uiProgressBar.value = i_message.progress;
    }

    public void _RegistMessage()
    {
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Handler.Message.Scene.LoadSceneStart>(this.OnMessageLoadSceneStart);
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Handler.Message.Scene.LoadSceneProgress>(this.OnMessageLoadSceneProgress);
    }
    public void _UnregistMessage()
    {
        KJ.Handler.KJMessageHandler.UnregistMessage(this);
    }
    #endregion On Message
}

﻿using UnityEngine;
using System.Collections;


public class TestInputBlock : MonoBehaviour
{
    [System.Diagnostics.Conditional("UNITY_EDITOR")]
    class Inner : System.Attribute
    {
        const string s_showKey = "TestInputBlock.s_showKey";

        #region Static Values
        
        static readonly string[] s_blockCategoryNames = new string[] {
            KJBluetoothValues.BlockCategory.GameNfc.ToString(),
            KJBluetoothValues.BlockCategory.ThreeD.ToString(),
            KJBluetoothValues.BlockCategory.Art.ToString(),
            KJBluetoothValues.BlockCategory.Rgb.ToString(),
            KJBluetoothValues.BlockCategory.Special.ToString(),
        };

        static readonly KJBluetoothValues.BlockType[] s_threeBlockTypes = new KJBluetoothValues.BlockType[] {
            KJBluetoothValues.BlockType.ThreeRed,
            KJBluetoothValues.BlockType.ThreeGreen,
            KJBluetoothValues.BlockType.ThreeBlue,
            KJBluetoothValues.BlockType.ThreeYellow,
            KJBluetoothValues.BlockType.ThreeViolet,
            KJBluetoothValues.BlockType.ThreeOrange,
            KJBluetoothValues.BlockType.ThreeIndigo,

            KJBluetoothValues.BlockType.TowerRed,
            KJBluetoothValues.BlockType.TowerGreen,
            KJBluetoothValues.BlockType.TowerBlue,
            KJBluetoothValues.BlockType.TowerYellow,
            KJBluetoothValues.BlockType.TowerViolet,
            KJBluetoothValues.BlockType.TowerOrange,
            KJBluetoothValues.BlockType.TowerIndigo,

        };

        static readonly string[] s_threeBlockTypeNames = new string[] {
            KJBluetoothValues.BlockType.ThreeRed.ToString(),
            KJBluetoothValues.BlockType.ThreeGreen.ToString(),
            KJBluetoothValues.BlockType.ThreeBlue.ToString(),
            KJBluetoothValues.BlockType.ThreeYellow.ToString(),
            KJBluetoothValues.BlockType.ThreeViolet.ToString(),
            KJBluetoothValues.BlockType.ThreeOrange.ToString(),
            KJBluetoothValues.BlockType.ThreeIndigo.ToString(),

            KJBluetoothValues.BlockType.TowerRed.ToString(),
            KJBluetoothValues.BlockType.TowerGreen.ToString(),
            KJBluetoothValues.BlockType.TowerBlue.ToString(),
            KJBluetoothValues.BlockType.TowerYellow.ToString(),
            KJBluetoothValues.BlockType.TowerViolet.ToString(),
            KJBluetoothValues.BlockType.TowerOrange.ToString(),
            KJBluetoothValues.BlockType.TowerIndigo.ToString(),
        };

        static readonly KJBluetoothValues.BlockType[] s_artBlockTypes = new KJBluetoothValues.BlockType[] {
            KJBluetoothValues.BlockType.ArtRed,
            KJBluetoothValues.BlockType.ArtGreen,
            KJBluetoothValues.BlockType.ArtBlue,
            KJBluetoothValues.BlockType.ArtYellow,
            KJBluetoothValues.BlockType.ArtViolet,
            KJBluetoothValues.BlockType.ArtOrange,
            KJBluetoothValues.BlockType.ArtIndigo,

        };

        static readonly string[] s_artBlockTypeNames = new string[] {
            KJBluetoothValues.BlockType.ArtRed.ToString(),
            KJBluetoothValues.BlockType.ArtGreen.ToString(),
            KJBluetoothValues.BlockType.ArtBlue.ToString(),
            KJBluetoothValues.BlockType.ArtYellow.ToString(),
            KJBluetoothValues.BlockType.ArtViolet.ToString(),
            KJBluetoothValues.BlockType.ArtOrange.ToString(),
            KJBluetoothValues.BlockType.ArtIndigo.ToString(),
        };



        static readonly KJBluetoothValues.BlockType[] s_specialBlockTypes = new KJBluetoothValues.BlockType[] {
            KJBluetoothValues.BlockType.SpecialTouch,
            //KJBluetoothValues.BlockType.SpecialLcd,
            KJBluetoothValues.BlockType.SpecialMotor,
            KJBluetoothValues.BlockType.Censor,
        };

        static readonly string[] s_specialBlockTypeNames = new string[] {
            KJBluetoothValues.BlockType.SpecialTouch.ToString(),
            //KJBluetoothValues.BlockType.SpecialLcd.ToString(),
            KJBluetoothValues.BlockType.SpecialMotor.ToString(),
            KJBluetoothValues.BlockType.Censor.ToString(),
        };

        static readonly string[] s_touchBlockDirectionNames = new string[] {
            KJBluetoothValues.TouchDirection.Up.ToString(),
            KJBluetoothValues.TouchDirection.Right.ToString(),
            KJBluetoothValues.TouchDirection.Down.ToString(),
            KJBluetoothValues.TouchDirection.Left.ToString(),
            KJBluetoothValues.TouchDirection.Center.ToString(),
        };

        static readonly KJBluetoothValues.MotorDirection[] s_motorBlockDirections = new KJBluetoothValues.MotorDirection[] {
            KJBluetoothValues.MotorDirection.Stop,
            KJBluetoothValues.MotorDirection.CW,
            KJBluetoothValues.MotorDirection.CCW,
        };
        static readonly string[] s_motorBlockDirectionNames = new string[] {
            KJBluetoothValues.MotorDirection.Stop.ToString(),
            KJBluetoothValues.MotorDirection.CW.ToString(),
            KJBluetoothValues.MotorDirection.CCW.ToString(),
        };
        
        /*static readonly string[] s_atPositionNames = new string[]
        {
            "Pos0" , "Pos1" , "Pos2" , "Pos3"
        };*/
        #endregion Static Values


        string x = "0", y = "0", z = "0";
        int at = 0;
        bool add = true;

        Color32 color = new Color32(0, 0, 0, 255);
        int bright = 5;

        KJBluetoothValues.BlockCategory blockCategory = KJBluetoothValues.BlockCategory.GameNfc;
        int threeBlockType = 0;
        int artBlockType = 0;

        int specialBlockType = 0;
        int motorBlockDirection = 0;

        bool[] touchBlockDirections = new bool[5];

        string lcdContent = string.Empty;

        string player = "0", item = "0", skin = "0";

        bool show = false;


        public void Setup()
        {
#if UNITY_EDITOR
            show = UnityEditor.EditorPrefs.GetBool(s_showKey, true);
#endif // UNITY_EDITOR
        }


        void SendGameBlock()
        {
            BluetoothSerialize.GameBlockData data = new BluetoothSerialize.GameBlockData();
            data.SetPosition(int.Parse(x), int.Parse(y), int.Parse(z));

            data.type = KJBluetoothValues.BlockType.GameNfc;
            data.blockState = (true == add) ? KJBluetoothValues.BlockState.On : KJBluetoothValues.BlockState.Off;

            KJ.Handler.KJMessageHandler.SendMessage(new KJ.Message.Bluetooth.ReceiveDataGameBlock(data));
        }

        /*void SendDummyBlock()
        {
            BluetoothSerialize.DummyBlockData data = new BluetoothSerialize.DummyBlockData();
            data.SetPosition(int.Parse(x), int.Parse(y), int.Parse(z));

            data.type = s_dummyBlockTypes[dummyBlockType];
            data.blockState = (true == add) ? KJBluetoothValues.BlockState.On : KJBluetoothValues.BlockState.Off;

            KJ.Handler.KJMessageHandler.SendMessage(new KJ.Message.Bluetooth.ReceiveDataDummyBlock(data));
        }*/

        /*void SendLedBlock()
        {
            BluetoothSerialize.LedBlockData data = new BluetoothSerialize.LedBlockData();
            data.SetPosition(int.Parse(x), int.Parse(y), int.Parse(z));

            data.type = KJBluetoothValues.BlockType.Led;
            data.blockState = (true == add) ? KJBluetoothValues.BlockState.On : KJBluetoothValues.BlockState.Off;

            KJ.Handler.KJMessageHandler.SendMessage(new KJ.Message.Bluetooth.ReceiveDataLedBlock(data));
        }*/

        void SendThreeBlock()
        {
            BluetoothSerialize.ThreeDBlockData data = new BluetoothSerialize.ThreeDBlockData();
            data.SetPosition(int.Parse(x), int.Parse(y), int.Parse(z));

            data.type = s_threeBlockTypes[threeBlockType];
            data.blockState = (true == add) ? KJBluetoothValues.BlockState.On : KJBluetoothValues.BlockState.Off;

            data.cardID = 99;

            KJ.Handler.KJMessageHandler.SendMessage(new KJ.Message.Bluetooth.ReceiveDataThreeDBlock(data));
        }
        void SendArtBlock()
        {
            BluetoothSerialize.ArtBlockData data = new BluetoothSerialize.ArtBlockData();
            data.SetPosition(int.Parse(x), int.Parse(y), int.Parse(z));

            data.type = s_artBlockTypes[artBlockType];
            data.blockState = (true == add) ? KJBluetoothValues.BlockState.On : KJBluetoothValues.BlockState.Off;

            KJ.Handler.KJMessageHandler.SendMessage(new KJ.Message.Bluetooth.ReceiveDataArtBlock(data));
        }
        void SendRgbBlock()
        {
            BluetoothSerialize.RgbBlockData data = new BluetoothSerialize.RgbBlockData();
            data.SetPosition(int.Parse(x), int.Parse(y), int.Parse(z));

            data.type = KJBluetoothValues.BlockType.Rgb;
            data.blockState = (true == add) ? KJBluetoothValues.BlockState.On : KJBluetoothValues.BlockState.Off;

            KJ.Handler.KJMessageHandler.SendMessage(new KJ.Message.Bluetooth.ReceiveDataRgbBlock(data));
        }
        void SendTouchBlock()
        {
            BluetoothSerialize.TouchBlockData data = new BluetoothSerialize.TouchBlockData();
            data.SetPosition(int.Parse(x), int.Parse(y), int.Parse(z));
            data.directionStates = new byte[5];
            for (int i = 0; i < data.directionStates.Length; ++i)
            {
                data.directionStates[i] = (byte)KJBluetoothValues.TouchState.Off;
            }

            data.type = s_specialBlockTypes[specialBlockType];
            data.blockState = (true == add) ? KJBluetoothValues.BlockState.On : KJBluetoothValues.BlockState.Off;

            KJ.Handler.KJMessageHandler.SendMessage(new KJ.Message.Bluetooth.ReceiveDataTouchBlock(data));
        }

        /*void SendLcdBlock()
        {
            BluetoothSerialize.LcdBlockData data = new BluetoothSerialize.LcdBlockData();
            data.SetPosition(int.Parse(x), int.Parse(y), int.Parse(z));

            data.type = s_specialBlockTypes[specialBlockType];
            data.blockState = (true == add) ? KJBluetoothValues.BlockState.On : KJBluetoothValues.BlockState.Off;

            KJ.Handler.KJMessageHandler.SendMessage(new KJ.Message.Bluetooth.ReceiveDataLcdBlock(data));
        }*/

        void SendMotorBlock()
        {
            BluetoothSerialize.MotorBlockData data = new BluetoothSerialize.MotorBlockData();
            data.SetPosition(int.Parse(x), int.Parse(y), int.Parse(z));

            data.type = s_specialBlockTypes[specialBlockType];
            data.blockState = (true == add) ? KJBluetoothValues.BlockState.On : KJBluetoothValues.BlockState.Off;

            KJ.Handler.KJMessageHandler.SendMessage(new KJ.Message.Bluetooth.ReceiveDataMotorBlock(data));
        }

        void SendCensorBlock()
        {
            BluetoothSerialize.CensorBlockData data = new BluetoothSerialize.CensorBlockData();
            data.SetPosition(int.Parse(x), int.Parse(y), int.Parse(z));

            data.type = s_specialBlockTypes[specialBlockType];
            data.blockState = (true == add) ? KJBluetoothValues.BlockState.On : KJBluetoothValues.BlockState.Off;
            data.centimeter = 0;

            KJ.Handler.KJMessageHandler.SendMessage(new KJ.Message.Bluetooth.ReceiveDataCensorBlock(data));
        }

        void SendSpecialBlock()
        {
            switch (s_specialBlockTypes[specialBlockType])
            {
                case KJBluetoothValues.BlockType.SpecialTouch:
                    this.SendTouchBlock();
                    break;

                case KJBluetoothValues.BlockType.SpecialMotor:
                    this.SendMotorBlock();
                    break;
                case KJBluetoothValues.BlockType.Censor:
                    this.SendCensorBlock();
                    break;

                default:
                    KJ.Utilities.Logger.Print(KJ.Utilities.Logger.E_LOG_LEVEL.WARNING, "is not special block");
                    break;
            }
        }

        void SendBlockType()
        {
            switch (blockCategory)
            {
                case KJBluetoothValues.BlockCategory.GameNfc:
                    this.SendGameBlock();
                    break;

                /*case KJBluetoothValues.BlockCategory.Dummy:
                    this.SendDummyBlock();
                    break;

                case KJBluetoothValues.BlockCategory.Led:
                    this.SendLedBlock();
                    break;*/

                case KJBluetoothValues.BlockCategory.ThreeD:
                    this.SendThreeBlock();
                    break;

                case KJBluetoothValues.BlockCategory.Art:
                    this.SendArtBlock();
                    break;
                case KJBluetoothValues.BlockCategory.Rgb:
                    this.SendRgbBlock();
                    break;

                case KJBluetoothValues.BlockCategory.Special:
                    this.SendSpecialBlock();
                    break;
            }
        }


        void SendColor()
        {
            KJ.Message.Bluetooth.ReceiveDataRgbColor message = new KJ.Message.Bluetooth.ReceiveDataRgbColor(int.Parse(x), int.Parse(y), int.Parse(z), (int)KJBluetoothValues.BlockCategory.Rgb, bright, color);
            KJ.Handler.KJMessageHandler.SendMessage(message);
        }

        /*void SendPlayer(bool i_add)
        {
            KJ.Message.Bluetooth.ReceiveDataPlayer message = new KJ.Message.Bluetooth.ReceiveDataPlayer();
            message.SetData(int.Parse(x), int.Parse(y), int.Parse(z), (int)blockCategory, int.Parse(player), (true == i_add) ? KJBluetoothValues.BlockState.On : KJBluetoothValues.BlockState.Off, 0);

            KJ.Handler.KJMessageHandler.SendMessage(message);
        }

        void SendItem(bool i_add)
        {
            KJBluetoothValues.BlockType type = KJBluetoothValues.BlockType.GameNfc;
            if (KJBluetoothValues.BlockCategory.Dummy == blockCategory)
            {
                type = s_dummyBlockTypes[dummyBlockType];
            }

            KJ.Message.Bluetooth.ReceiveDataItem message = new KJ.Message.Bluetooth.ReceiveDataItem();
            message.SetData(int.Parse(x), int.Parse(y), int.Parse(z), (int)type, int.Parse(item), (true == i_add) ? KJBluetoothValues.BlockState.On : KJBluetoothValues.BlockState.Off, at);

            KJ.Handler.KJMessageHandler.SendMessage(message);
        }*/

        void SendSkin(bool i_add)
        {
            KJ.Message.Bluetooth.ReceiveDataSkin message = new KJ.Message.Bluetooth.ReceiveDataSkin();
            message.SetData(int.Parse(x), int.Parse(y), int.Parse(z), (int)blockCategory, int.Parse(skin), (true == i_add) ? KJBluetoothValues.BlockState.On : KJBluetoothValues.BlockState.Off, 0);

            KJ.Handler.KJMessageHandler.SendMessage(message);
        }


        #region Draw Block Type
        void DrawGameBlock()
        {
            //this.DrawPlayerBlock();
            //this.DrawItemBlock();
            this.DrawSkinBlock();
        }

        /*void DrawDummyBlock()
        {
            GUILayout.BeginHorizontal();
            {
                dummyBlockType = GUILayout.SelectionGrid(dummyBlockType, s_dummyBlockTypeNames, 5);
            }
            GUILayout.EndHorizontal();

            this.DrawItemBlock();
        }*/
        void DrawThreeBlock()
        {
            GUILayout.BeginHorizontal();
            {
                threeBlockType = GUILayout.SelectionGrid(threeBlockType, s_threeBlockTypeNames, 7);
            }
            GUILayout.EndHorizontal();

            //this.DrawItemBlock();
        }
        void DrawArtBlock()
        {
            GUILayout.BeginHorizontal();
            {
                artBlockType = GUILayout.SelectionGrid(artBlockType, s_artBlockTypeNames, 7);
            }
            GUILayout.EndHorizontal();

            //this.DrawItemBlock();
        }

        void DrawRgbBlock()
        {
            GUILayout.BeginHorizontal();
            {
                GUILayout.Label(string.Format("R: {0}", color.r), GUILayout.Width(50));
                color.r = (byte)GUILayout.HorizontalSlider((float)color.r, 0, 255);
                GUILayout.Space(50);

                GUILayout.Label(string.Format("G: {0}", color.g), GUILayout.Width(50));
                color.g = (byte)GUILayout.HorizontalSlider((float)color.g, 0, 255);
                GUILayout.Space(50);

                GUILayout.Label(string.Format("B: {0}", color.b), GUILayout.Width(50));
                color.b = (byte)GUILayout.HorizontalSlider((float)color.b, 0, 255);
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            {
                GUILayout.Label(string.Format("Bright: {0}", bright), GUILayout.Width(100));
                bright = (int)GUILayout.HorizontalSlider((float)bright, 0, 9);

                GUILayout.Space(50);

                if (true == GUILayout.Button("Set Color"))
                {
                    this.SendColor();
                }
            }
            GUILayout.EndHorizontal();
        }

        void DrawTouchBlock()
        {
            TouchBlockData touchBlockData = BlockDataManager.shared[int.Parse(x), int.Parse(y), int.Parse(z)] as TouchBlockData;

            GUILayout.BeginHorizontal();
            {
                if (null == touchBlockData)
                {
                    GUI.enabled = false;
                }
                else
                {
                    for (int i = 0; i < touchBlockData.directionStates.Length; ++i)
                    {
                        touchBlockDirections[i] = touchBlockData.directionStates[i] == KJBluetoothValues.TouchState.On;
                    }
                }

                bool[] oldTouchDirections = new bool[touchBlockDirections.Length];
                touchBlockDirections.CopyTo(oldTouchDirections, 0);

                for (int i = 0; i < touchBlockDirections.Length; ++i)
                {
                    touchBlockDirections[i] = GUILayout.Toggle(touchBlockDirections[i], s_touchBlockDirectionNames[i]);
                }

                if (true == GUI.enabled)
                {
                    for (int i = 0; i < touchBlockDirections.Length; ++i)
                    {
                        if (oldTouchDirections[i] != touchBlockDirections[i])
                        {
                            byte[] state = new byte[touchBlockDirections.Length];
                            for (int j = 0; j < touchBlockDirections.Length; ++j)
                            {
                                state[j] = (true == touchBlockDirections[j]) ? (byte)KJBluetoothValues.TouchState.On : (byte)KJBluetoothValues.TouchState.Off;
                            }

                            BluetoothSerialize.TouchBlockData data = new BluetoothSerialize.TouchBlockData();
                            data.SetPosition(int.Parse(x), int.Parse(y), int.Parse(z));
                            data.directionStates = state;

                            data.type = s_specialBlockTypes[specialBlockType];
                            data.blockState = KJBluetoothValues.BlockState.Hold;

                            KJ.Handler.KJMessageHandler.SendMessage(new KJ.Message.Bluetooth.ReceiveDataTouchState(data));
                            break;
                        }
                    }
                }
            }
            GUILayout.EndHorizontal();

            GUI.enabled = true;
        }



        void DrawMotorBlock()
        {
            GUILayout.BeginHorizontal();
            {
                int index = GUILayout.SelectionGrid(motorBlockDirection, s_motorBlockDirectionNames, s_motorBlockDirectionNames.Length);
                if (index != motorBlockDirection)
                {
                    motorBlockDirection = index;

                    BluetoothSerialize.MotorBlockData data = new BluetoothSerialize.MotorBlockData();
                    data.SetPosition(int.Parse(x), int.Parse(y), int.Parse(z));

                    data.type = s_specialBlockTypes[specialBlockType];
                    data.blockState = KJBluetoothValues.BlockState.Hold;

                    data.direction = (byte)s_motorBlockDirections[motorBlockDirection];

                    KJ.Handler.KJMessageHandler.SendMessage(new KJ.Message.Bluetooth.ReceiveDataMotorState(data));
                }
            }
            GUILayout.EndHorizontal();
        }

        void DrawCensorBlock()
        {

        }

        void DrawSpecial()
        {
            GUILayout.BeginHorizontal();
            {
                specialBlockType = GUILayout.SelectionGrid(specialBlockType, s_specialBlockTypeNames, s_specialBlockTypeNames.Length);
            }
            GUILayout.EndHorizontal();

            switch (s_specialBlockTypes[specialBlockType])
            {
                case KJBluetoothValues.BlockType.SpecialTouch:
                    this.DrawTouchBlock();
                    break;

                case KJBluetoothValues.BlockType.SpecialMotor:
                    this.DrawMotorBlock();
                    break;

                case KJBluetoothValues.BlockType.Censor:
                    this.DrawCensorBlock();
                    break;

                default:
                    KJ.Utilities.Logger.Print(KJ.Utilities.Logger.E_LOG_LEVEL.WARNING, "is not special block");
                    break;
            }
        }

        /*void DrawPlayerBlock()
        {
            GUILayout.BeginHorizontal();
            {
                GUILayout.Label("Player", GUILayout.Width(50));

                player = GUILayout.TextField(player);

                GUILayout.Space(50);

                if (true == GUILayout.Button("Add"))
                {
                    this.SendPlayer(true);
                }
                if (true == GUILayout.Button("Remove"))
                {
                    this.SendPlayer(false);
                }
            }
            GUILayout.EndHorizontal();
        }*/

        /*void DrawItemBlock()
        {
            GUILayout.BeginHorizontal();
            {
                GUILayout.Label("Item", GUILayout.Width(50));

                item = GUILayout.TextField(item);

                GUILayout.Space(50);

                at = GUILayout.SelectionGrid(at, s_atPositionNames, s_atPositionNames.Length);

                if (true == GUILayout.Button("Add"))
                {
                    this.SendItem(true);
                }
                if (true == GUILayout.Button("Remove"))
                {
                    this.SendItem(false);
                }
            }
            GUILayout.EndHorizontal();
        }*/

        void DrawSkinBlock()
        {
            GUILayout.BeginHorizontal();
            {
                GUILayout.Label("Skin", GUILayout.Width(50));

                skin = GUILayout.TextField(skin);

                GUILayout.Space(50);

                if (true == GUILayout.Button("Add"))
                {
                    this.SendSkin(true);
                }
                if (true == GUILayout.Button("Remove"))
                {
                    this.SendSkin(false);
                }
            }
            GUILayout.EndHorizontal();
        }
        #endregion Draw Block Type

        void DrawCommon()
        {
            GUILayout.BeginHorizontal(GUILayout.Width(Screen.width));
            {
                blockCategory = (KJBluetoothValues.BlockCategory)GUILayout.SelectionGrid((int)blockCategory, s_blockCategoryNames, s_blockCategoryNames.Length);
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal(GUILayout.Width(Screen.width));
            {
                GUILayout.Label("X", GUILayout.Width(20));
                x = GUILayout.TextField(x);
                GUILayout.Space(30);

                GUILayout.Label("Y", GUILayout.Width(20));
                y = GUILayout.TextField(y);
                GUILayout.Space(30);

                GUILayout.Label("Z", GUILayout.Width(20));
                z = GUILayout.TextField(z);

                GUILayout.Space(50);
                add = GUILayout.Toggle(add, "Add", GUILayout.Width(50));

                if (true == GUILayout.Button("OK", GUILayout.Width(100)))
                {
                    this.SendBlockType();
                }
            }
            GUILayout.EndHorizontal();
        }

        public void OnGUI()
        {
            GUILayout.BeginVertical(GUILayout.Width(Screen.width), GUILayout.Height(Screen.height));
            {
                if (true == GUILayout.Button((true == show) ? "Hide" : "Show"))
                {
                    show = !show;

#if UNITY_EDITOR
                    UnityEditor.EditorPrefs.SetBool(s_showKey, show);
#endif // UNITY_EDITOR
                }

                if (true == show)
                {
                    this.DrawCommon();

                    switch (blockCategory)
                    {
                        case KJBluetoothValues.BlockCategory.GameNfc:
                            this.DrawGameBlock();
                            break;

                        case KJBluetoothValues.BlockCategory.ThreeD:
                            this.DrawThreeBlock();
                            break;
                        case KJBluetoothValues.BlockCategory.Art:
                            this.DrawArtBlock();
                            break;

                        case KJBluetoothValues.BlockCategory.Rgb:
                            this.DrawRgbBlock();
                            break;
                        /*case KJBluetoothValues.BlockCategory.Dummy:
                            this.DrawDummyBlock();
                            break;

                        case KJBluetoothValues.BlockCategory.Led:
                            this.DrawLedBlock();
                            break;*/


                        case KJBluetoothValues.BlockCategory.Special:
                            this.DrawSpecial();
                            break;
                    }
                }
            }
            GUILayout.EndVertical();
        }
    }

    Inner inner = new Inner();

    [System.Diagnostics.Conditional("UNITY_EDITOR")]
    void Start()
    {
        inner.Setup();
    }

    [System.Diagnostics.Conditional("UNITY_EDITOR")]
    void OnGUI()
    {
        inner.OnGUI();
    }
}

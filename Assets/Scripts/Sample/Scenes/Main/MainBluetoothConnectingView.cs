﻿using UnityEngine;
using System.Collections;

public class MainBluetoothConnectingView : KJ.Model.KJAViewController, KJ.Handler.KJMessageHandler.IMessageReceiver
{
    string address = string.Empty;
    public GameObject m_panelSuccess;
    public GameObject m_panelFail;
    float m_timerForConnect = 0f;
    protected override void Start()
    {
        base.Start();

        NGUITools.SetActiveSelf(goRootView, false);
        
    }

    void Update()
    {
        if (m_timerForConnect > 0f)
        {
            m_timerForConnect -= Time.deltaTime;
            if (m_timerForConnect <= 0f)
            {
                this._UnregistMessage();
                //time out
                KJ.Handler.Message.Scene.PushView message = new KJ.Handler.Message.Scene.PushView(typeof(MainBluetoothConnectionFailView));
                KJ.Handler.KJMessageHandler.SendMessage(message);
            }
        }
    }

    protected override void ProcessResultData(KJ.Model.KJAViewController.ResultData i_resultData)
    {
    //    Debug.Log("ProcessResultData : " + i_resultData.str);
        address = i_resultData.str;
    }

    protected override void DoLoadData()
    {
   //     Debug.Log("MainBluetoothConnectingView : " + "DoLoadData");
    }
    protected override void DoSaveData()
    {
        KJPluginMethods.methods.bluetooth.disconnect();
    }

    protected override void DoOpen()
    {
  //      Debug.Log("MainBluetoothConnectingView : " + "DoOpen");
        this._RegistMessage();

        NGUITools.SetActiveSelf(goRootView, true);

        KJPluginMethods.methods.bluetooth.connectDeviceByAddress(address);

        this.CheckTestAddress();

        m_timerForConnect = 20f;
        PanelViewHide();
    }
    protected override void DoClose()
    {
        m_timerForConnect = 0f;
 //       Debug.Log("MainBluetoothConnectingView : " + "DoClose");
        NGUITools.SetActiveSelf(goRootView, false);

        this._UnregistMessage();
    }
    /*
     * KJ.Handler.Message.Scene.PushView message = new KJ.Handler.Message.Scene.PushView(typeof(MainBluetoothConnectingView), resultData);
        KJ.Handler.KJMessageHandler.SendMessage(message);
     * */

    void PanelViewHide()
    {
        m_panelSuccess.SetActive(false);
        m_panelFail.SetActive(false);
    }

	public void InitBoardType()
	{
		PlayerPrefs.SetInt ("BoardType",0);
	}

    #region On Message
    void OnMessageConnectionSuccess(KJ.Message.Bluetooth.ConnectionSuccess i_message)
    {
 //       Debug.Log("OnMessageConnectionSuccess : " + i_message.deviceName);
        if (m_timerForConnect <= 0f)
        {
            return;
        }
        this._UnregistMessage();
        m_timerForConnect = 0f;
		InitBoardType();
		KJPluginMethods.methods.bluetooth.requestBoardType(); //#boardmodelid

        KJPluginMethods.methods.bluetooth.requestCommand(KJBluetoothValues.BluetoothCommands.Loop);
        ResultData resultData = new ResultData();
        resultData.str = i_message.deviceName;
        KJ.Handler.Message.Scene.PushView message = new KJ.Handler.Message.Scene.PushView(typeof(MainBluetoothConnectionSuccessView), resultData);
        KJ.Handler.KJMessageHandler.SendMessage(message);
    }

    void OnMessageConnectionFail(KJ.Message.Bluetooth.ConnectionFail i_message)
    {
        if (m_timerForConnect <= 0f)
        {
            return;
        }
        this._UnregistMessage();
        m_timerForConnect = 0f;
        KJ.Handler.Message.Scene.PushView message = new KJ.Handler.Message.Scene.PushView(typeof(MainBluetoothConnectionFailView));
        KJ.Handler.KJMessageHandler.SendMessage(message);
    }
    void OnMessageConnectionLost(KJ.Message.Bluetooth.ConnectionLost i_message)
    {

    }

    public void _RegistMessage()
    {
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ConnectionSuccess>(this.OnMessageConnectionSuccess);
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ConnectionFail>(this.OnMessageConnectionFail);
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ConnectionLost>(this.OnMessageConnectionLost);
    }
    public void _UnregistMessage()
    {
        KJ.Handler.KJMessageHandler.UnregistMessage(this);
    }
    #endregion On Message

    #region On UI
    public void OnClickButtonCancel()
    {
        KJ.Handler.KJMessageHandler.SendMessage(new KJ.Handler.Message.Scene.PopView());
    }
    #endregion On UI


    #region Condition Debug
    [System.Diagnostics.Conditional("UNITY_EDITOR")]
    void SendMessageConnectionSuccess()
    {
        KJ.Message.Bluetooth.ConnectionSuccess message = new KJ.Message.Bluetooth.ConnectionSuccess("TEST");
        KJ.Handler.KJMessageHandler.SendMessage(message);
    }

    [System.Diagnostics.Conditional("UNITY_EDITOR")]
    void CheckTestAddress()
    {
        if (false == address.Equals("TEST_ADDRESS"))
        {
            return;
        }

        this.Invoke("SendMessageConnectionSuccess", 0.1f);
    }


    #endregion Condition Debug
}

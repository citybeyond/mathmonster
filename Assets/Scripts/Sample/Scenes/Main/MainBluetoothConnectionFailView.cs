﻿using UnityEngine;
using System.Collections;

public class MainBluetoothConnectionFailView : KJ.Model.KJAViewController
{
    protected override void Start()
    {
        base.Start();

        NGUITools.SetActiveSelf(goRootView, false);
    }

    protected override void DoLoadData()
    {
    }
    protected override void DoSaveData()
    {
    }

    protected override void DoOpen()
    {
        NGUITools.SetActiveSelf(goRootView, true);
    }
    protected override void DoClose()
    {
        NGUITools.SetActiveSelf(goRootView, false);
    }


    #region On UI
    public void OnClickButtonRetry()
    {
        KJ.Handler.KJMessageHandler.SendMessage(new KJ.Handler.Message.Scene.PopView());
    }
    public void OnClickButtonCancel()
    {
        KJ.Handler.KJMessageHandler.SendMessage(new KJ.Handler.Message.Scene.PopViewForType(typeof(MainBluetoothView))); 
    }
    #endregion On UI
}

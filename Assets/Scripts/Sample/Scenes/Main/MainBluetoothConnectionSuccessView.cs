﻿using UnityEngine;
using System.Collections;



public class DisableSystemUI
{
#if UNITY_ANDROID
    static AndroidJavaObject activityInstance;
    static AndroidJavaObject windowInstance;
    static AndroidJavaObject viewInstance;

    const int SYSTEM_UI_FLAG_HIDE_NAVIGATION = 2;
    const int SYSTEM_UI_FLAG_LAYOUT_STABLE = 256;
    const int SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION = 512;
    const int SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN = 1024;
    const int SYSTEM_UI_FLAG_IMMERSIVE = 2048;
    const int SYSTEM_UI_FLAG_IMMERSIVE_STICKY = 4096;
    const int SYSTEM_UI_FLAG_FULLSCREEN = 4;

    public delegate void RunPtr();

    public static void Run()
    {
        if (viewInstance != null)
        {
            viewInstance.Call("setSystemUiVisibility",
                              SYSTEM_UI_FLAG_LAYOUT_STABLE
                              | SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                              | SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                              | SYSTEM_UI_FLAG_HIDE_NAVIGATION
                              | SYSTEM_UI_FLAG_FULLSCREEN
                              | SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }

    }


    public static void DisableNavUI()
    {
        if (Application.platform != RuntimePlatform.Android)
            return;

        AndroidJavaClass unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

        {
            activityInstance = unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity");
            windowInstance = activityInstance.Call<AndroidJavaObject>("getWindow");
            viewInstance = windowInstance.Call<AndroidJavaObject>("getDecorView");

            AndroidJavaRunnable RunThis;
            RunThis = new AndroidJavaRunnable(new RunPtr(Run));
            activityInstance.Call("runOnUiThread", RunThis);
        }

    }
#else

    public static void DisableNavUI() {

    }

#endif

}



public class MainBluetoothConnectionSuccessView : KJ.Model.KJAViewController, KJ.Handler.KJMessageHandler.IMessageReceiver
{

    public GameObject m_panelChangeDeviceName;
    public UIInput m_input;
    public UILabel m_deviceName;

    string m_name = "";

    public GameObject KJBluetoothReceiver; 
    string address = string.Empty;
	public int testBoardType = 0;

	[SerializeField]
	private GameObject	goReceivingBoardType;

	[SerializeField]
	private GameObject	goFirmwareInfo;
	
	[SerializeField]
	private GameObject	goFrame;

	[SerializeField]
	private GameObject goBtnOK;

	[SerializeField]
	private UILabel labelRename;
	private int boardType = 0;
	private float fillAmount = 0.0f;
	private bool firmwareInfo = false;
	private readonly int OLD_BOARD_TYPE = 9;

    protected override void Start()
    {
        base.Start();

        NGUITools.SetActiveSelf(goRootView, false);
        m_deviceName.text = KJBluetoothReceiver.GetComponent<KJBluetoothReceiver>().deviceName;
    }

    protected override void DoLoadData()
    {
        Debug.Log("MainBluetoothConnectionSuccessView : DoLoadData");

#if UNITY_EDITOR
		PassInUnity();
#else
		StartCoroutine("WaitResponse");
#endif
    }
    protected override void DoSaveData()
    {
    }

    protected override void DoOpen()
    {
        Debug.Log("MainBluetoothConnectionSuccessView : DoOpen");
        NGUITools.SetActiveSelf(goRootView, true);
        _RegistMessage();
    }
    protected override void DoClose()
    {
        Debug.Log("MainBluetoothConnectionSuccessView : DoClose");
        NGUITools.SetActiveSelf(goRootView, false);
        _UnregistMessage();
    }
    protected override void ProcessResultData(KJ.Model.KJAViewController.ResultData i_resultData)
    {
        address = i_resultData.str;

        string test = "MOO-123456789";
        if ( test.StartsWith("MOBLO-")) {
			//m_name = test.Substring("MOBLO-".Length);
			m_name = test.Substring( ("MOBLO" + boardType.ToString() + "-").Length);
        }
        else{
            m_name = test;
        }
        Debug.Log("m_name :" + m_name);
    }
	
    #region On Message

    void OnMessageConnectionFail(KJ.Message.Bluetooth.ConnectionFail i_message)
    {
    }
    void OnMessageConnectionLost(KJ.Message.Bluetooth.ConnectionLost i_message)
    {
    }


	void PassInUnity()
	{
		goReceivingBoardType.SetActive(false);
		goFrame.SetActive(true);
		goBtnOK.SetActive(true);
		boardType = testBoardType;
		PlayerPrefs.SetInt ("BoardType",boardType);

		m_deviceName.text = KJBluetoothReceiver.GetComponent<KJBluetoothReceiver>().deviceName;
	}

	void OnMessageBoardType(KJ.Message.Bluetooth.ReceiveBoardType type)
	{
		boardType = type.boardType;
		goReceivingBoardType.SetActive(false);
		goFrame.SetActive(true);
		goBtnOK.SetActive(true);

		m_deviceName.text = KJBluetoothReceiver.GetComponent<KJBluetoothReceiver>().deviceName;
		string moblo = m_deviceName.text;

		if(moblo.StartsWith("MOBLO" + boardType.ToString()) == false)
		{

			moblo = moblo.Insert(5,boardType.ToString());
			m_deviceName.text = moblo;
		}

		StopCoroutine("WaitResponse");
	}

    public void _RegistMessage()
    {
        //KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ConnectionFail>(this.OnMessageConnectionFail);
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ConnectionLost>(this.OnMessageConnectionLost);
		KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ReceiveBoardType>(this.OnMessageBoardType);
    }

    public void _UnregistMessage()
    {
        KJ.Handler.KJMessageHandler.UnregistMessage(this);
    }
    #endregion On Message

    #region On UI
    public void OnClickButtonOk()
    {
		ToNextStepOnUnityEditor();

		if(boardType > 0)
		{
	        _UnregistMessage();
	        KJ.Handler.KJMessageHandler.SendMessage(new KJ.Handler.Message.Scene.LoadNextScene());
		}
    }

	[System.Diagnostics.Conditional("UNITY_EDITOR")]
	public void ToNextStepOnUnityEditor()
	{
		_UnregistMessage();
		KJ.Handler.KJMessageHandler.SendMessage(new KJ.Handler.Message.Scene.LoadNextScene());
	}


    public void OnClickButtonChangeDeviceView()
    {
        m_panelChangeDeviceName.SetActive(true);
		labelRename.text = "MOBLO" + boardType.ToString() + " -";

        if (m_deviceName.text != null)
        {
            if (m_deviceName.text.StartsWith("MOBLO" + boardType.ToString() + "-"))
            {
                m_input.value = m_deviceName.text.Substring(("MOBLO" + boardType.ToString() +"-").Length);
            }
            else
            {
                m_input.value = m_deviceName.text;
            }
        }
        //m_input.value = m_deviceName.text.Substring("MOBLO-".Length);
    }

    public void OnClickButtonDeviceNameConfirm()
    {
        m_panelChangeDeviceName.SetActive(false);
    }


    public void OnSubmitDeviceName()
    {
        string text = "MOBLO" + boardType.ToString() + "-" + UIInput.current.value;
        m_deviceName.text = text;
   //     Debug.Log("OnSubmitDeviceName : " + text);
        byte[] bytes = System.Text.Encoding.ASCII.GetBytes(text);

        KJPluginMethods.methods.bluetooth.WriteDeviceName(text);

        DisableSystemUI.DisableNavUI();
    }

    #endregion On UI

	public void OnSetOldBoardType()
	{
		boardType = OLD_BOARD_TYPE;
		goReceivingBoardType.SetActive(false);
		goFrame.SetActive(true);
		goBtnOK.SetActive(true);
		PlayerPrefs.SetInt("BoardType",boardType);
		m_deviceName.text = KJBluetoothReceiver.GetComponent<KJBluetoothReceiver>().deviceName;
		
		StopCoroutine("WaitResponse");
	}

	IEnumerator WaitResponse()
	{
		while(boardType <= 0)
		{
			yield return new WaitForSeconds(4.0f);
		//	Debug.Log ("WaitResponse");
			KJPluginMethods.methods.bluetooth.requestBoardType();

			if(firmwareInfo == false)
			{
				goReceivingBoardType.SetActive(false);
				goFirmwareInfo.SetActive(true);
				firmwareInfo = true;
			}
		}
	}
}

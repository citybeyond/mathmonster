﻿using UnityEngine;
using System.Collections.Generic;

public class TestConnectView : KJ.Model.KJAViewController, KJ.Handler.KJMessageHandler.IMessageReceiver
{
    Vector2 scroll = Vector2.zero;

    List<BluetoothSerialize.FoundDevice> listDevice = new List<BluetoothSerialize.FoundDevice>();

    List<BluetoothSerialize.FoundDevice> listBondedDevice = new List<BluetoothSerialize.FoundDevice>();

    protected override void Start()
    {
        base.Start();
    }
    protected override void OnDestroy()
    {
        base.OnDestroy();
    }

    protected override void DoLoadData()
    {
        this._RegistMessage();
    }
    protected override void DoSaveData()
    {
        this._UnregistMessage();
    }

    protected override void DoOpen()
    {
        goRootView.SetActive(true);

        listBondedDevice = new List<BluetoothSerialize.FoundDevice>(KJPluginMethods.methods.bluetooth.getBondedDevices());
        listDevice.Clear();

        listDevice.AddRange(listBondedDevice);
    }
    protected override void DoClose()
    {
        goRootView.SetActive(false);
    }


    void OnMessageFoundDevice(KJ.Message.Bluetooth.FoundDevice i_message)
    {
        listDevice.Add(i_message.foundDevice);
    }

    void OnMessageConnectionSuccess(KJ.Message.Bluetooth.ConnectionSuccess i_message)
    {
        KJ.Handler.KJMessageHandler.SendMessage(new KJ.Message.TestLog.Log(string.Format("connected: {0}", i_message.deviceName)));

        KJ.Handler.KJMessageHandler.SendMessage(new KJ.Handler.Message.Scene.PushView(typeof(TestLedView)));
    }
    void OnMessageConnectionFail(KJ.Message.Bluetooth.ConnectionFail i_message)
    {
        KJ.Handler.KJMessageHandler.SendMessage(new KJ.Message.TestLog.Log(string.Format("connect failed")));
    }

    public void _RegistMessage()
    {
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.FoundDevice>(this.OnMessageFoundDevice);

        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ConnectionSuccess>(this.OnMessageConnectionSuccess);
        KJ.Handler.KJMessageHandler.RegistMessage<KJ.Message.Bluetooth.ConnectionFail>(this.OnMessageConnectionFail);
    }
    public void _UnregistMessage()
    {
        KJ.Handler.KJMessageHandler.UnregistMessage(this);
    }

    void OnGUI()
    {
        if (false == isOpened || true == TestLogView.fullScreen)
        {
            return;
        }

        float height = Screen.height / 20;

        Rect area = new Rect(0, Screen.height / 3, Screen.width, Screen.height - Screen.height / 3);

        GUILayout.BeginArea(area);
        {
            GUILayout.BeginVertical();
            {
                scroll = GUILayout.BeginScrollView(scroll);
                {
                    foreach (BluetoothSerialize.FoundDevice device in listDevice)
                    {
                        if (true == GUILayout.Button(device.name, GUILayout.Height(height)))
                        {
                            KJPluginMethods.methods.bluetooth.connectDeviceByAddress(device.address);
                            KJ.Handler.KJMessageHandler.SendMessage(new KJ.Message.TestLog.Log(string.Format("connecting...: {0}", device.name)));
                        }
                    }
                }
                GUILayout.EndScrollView();

                if (true == GUILayout.Button("Search", GUILayout.Height(height)))
                {
                    listDevice.Clear();
                    listDevice.AddRange(listBondedDevice);

                    KJPluginMethods.methods.bluetooth.cancelDiscovery();
                    KJPluginMethods.methods.bluetooth.doDiscovery();
                }
            }
            GUILayout.EndVertical();
        }
        GUILayout.EndArea();
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PopupSelectMultiNumCon : MonoBehaviour {
	public GameObject objButtonOK;

	public Image[] buttonImageArr = new Image[10];
	public Sprite[] defaultButtonSpriteArr = new Sprite[10];
	public Sprite[] selectedButtonSpriteArr = new Sprite[10];

	public Transform[] button7x7FormArr;
	public Transform[] button9x9FormArr;

	bool[] isSelectedArr = new bool[10];

	private int boardType = 0;
	// Use this for initialization
	void Start () {
		for(int i = 0 ; i < isSelectedArr.Length ; i++){
			isSelectedArr[i] = false; 
		}

		boardType = 0;

		if(CtrLearnMultiplicationTables02.instance != null){
			boardType = CtrLearnMultiplicationTables02.instance.boardType;
		}else if(CtrSpeedQuiz.instance != null){
			boardType = CtrSpeedQuiz.instance.boardType;
		}

		if (boardType == 7) {
			for(int i = 0 ; i < button7x7FormArr.Length ; i++){
				buttonImageArr[i].transform.position = button7x7FormArr[i].position;
				buttonImageArr[i].gameObject.SetActive(true);
			}
		}else if(boardType == 9){
			for(int i = 0 ; i < button9x9FormArr.Length ; i++){
				buttonImageArr[i].transform.position = button9x9FormArr[i].position;
				buttonImageArr[i].gameObject.SetActive(true);
			}
		}
	}

	public void ClickNum(int index){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_basic");

		if(CtrLearnMultiplicationTables02.instance != null){
			if(isSelectedArr[index] == false){
				isSelectedArr[index] = true;
				buttonImageArr[index].sprite = selectedButtonSpriteArr[index];

				if(index != 0){
					CtrLearnMultiplicationTables02.instance.listSelectedMultiNum.Add(index);
					SoundManager.Instance.PlayEffect(string.Format("nar_mc_square_{0}", index.ToString("000")));
				}

				if(index == 0){
					CtrLearnMultiplicationTables02.instance.listSelectedMultiNum.Clear();
					
					if (boardType == 7) {
						for(int i = 1 ; i < button7x7FormArr.Length ; i++){
							isSelectedArr[i] = true;
							CtrLearnMultiplicationTables02.instance.listSelectedMultiNum.Add(i);
							buttonImageArr[i].sprite = selectedButtonSpriteArr[i];

						}
					}else if(boardType == 9){
						for(int i = 1 ; i < button9x9FormArr.Length ; i++){
							isSelectedArr[i] = true;
							CtrLearnMultiplicationTables02.instance.listSelectedMultiNum.Add(i);
							buttonImageArr[i].sprite = selectedButtonSpriteArr[i];
						}
					}
//					for(int i = 1 ; i < buttonImageArr.Length ; i++){
//						isSelectedArr[i] = true;
//						CtrLearnMultiplicationTables02.instance.listSelectedMultiNum.Add(i);
//						buttonImageArr[i].sprite = selectedButtonSpriteArr[i];
//					}
				}

			}else{
				isSelectedArr[index] = false;
				buttonImageArr[index].sprite = defaultButtonSpriteArr[index];
				
				if(index != 0){
					CtrLearnMultiplicationTables02.instance.listSelectedMultiNum.Remove(index);
				}
				
				if(index == 0){
				
					for(int i = 1 ; i < buttonImageArr.Length ; i++){
						isSelectedArr[i] = false;
						CtrLearnMultiplicationTables02.instance.listSelectedMultiNum.Remove(i);
						buttonImageArr[i].sprite = defaultButtonSpriteArr[i];
					}
				}
			}

			if(CtrLearnMultiplicationTables02.instance.listSelectedMultiNum.Count > 0)
				objButtonOK.SetActive(true);
			else
				objButtonOK.SetActive(false);

//			string str = null;
//
//			for(int i = 0 ; i < CtrLearnMultiplicationTables02.instance.listSelectedMultiNum.Count ; i++){
//				str += CtrLearnMultiplicationTables02.instance.listSelectedMultiNum[i].ToString();
//			}
//			Debug.Log(str);

		}else if(CtrSpeedQuiz.instance != null){
			if(isSelectedArr[index] == false){
				isSelectedArr[index] = true;
				buttonImageArr[index].sprite = selectedButtonSpriteArr[index];
				
				if(index != 0){
					CtrSpeedQuiz.instance.listSelectedMultiNum.Add(index);
					SoundManager.Instance.PlayEffect(string.Format("nar_mc_triangle_{0}", index.ToString("000")));
				}
				
				if(index == 0){
					CtrSpeedQuiz.instance.listSelectedMultiNum.Clear();
					
					if (boardType == 7) {
						for(int i = 1 ; i < button7x7FormArr.Length ; i++){
							isSelectedArr[i] = true;
							CtrSpeedQuiz.instance.listSelectedMultiNum.Add(i);
							buttonImageArr[i].sprite = selectedButtonSpriteArr[i];
							
						}
					}else if(boardType == 9){
						for(int i = 1 ; i < button9x9FormArr.Length ; i++){
							isSelectedArr[i] = true;
							CtrSpeedQuiz.instance.listSelectedMultiNum.Add(i);
							buttonImageArr[i].sprite = selectedButtonSpriteArr[i];
						}
					}
//					for(int i = 1 ; i < buttonImageArr.Length ; i++){
//						isSelectedArr[i] = true;
//						CtrSpeedQuiz.instance.listSelectedMultiNum.Add(i);
//						buttonImageArr[i].sprite = selectedButtonSpriteArr[i];
//					}
				}
				
			}else{
				isSelectedArr[index] = false;
				buttonImageArr[index].sprite = defaultButtonSpriteArr[index];
				
				if(index != 0){
					CtrSpeedQuiz.instance.listSelectedMultiNum.Remove(index);
				}
				
				if(index == 0){
					
					for(int i = 1 ; i < buttonImageArr.Length ; i++){
						isSelectedArr[i] = false;
						CtrSpeedQuiz.instance.listSelectedMultiNum.Remove(i);
						buttonImageArr[i].sprite = defaultButtonSpriteArr[i];
					}
				}
			}
			
			if(CtrSpeedQuiz.instance.listSelectedMultiNum.Count > 0)
				objButtonOK.SetActive(true);
			else
				objButtonOK.SetActive(false);
			
			//			string str = null;
			//
			//			for(int i = 0 ; i < CtrLearnMultiplicationTables02.instance.listSelectedMultiNum.Count ; i++){
			//				str += CtrLearnMultiplicationTables02.instance.listSelectedMultiNum[i].ToString();
			//			}
			//			Debug.Log(str);
		}
	}

	void OnDisable(){
		if (CtrLearnMultiplicationTables02.instance != null)
			CtrLearnMultiplicationTables02.instance.OpenDoor ();
		else if (CtrSpeedQuiz.instance != null)
			CtrSpeedQuiz.instance.OpenDoor ();
	}

	public void ClickOK(){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_basic");
		this.gameObject.SetActive(false);
	}
}

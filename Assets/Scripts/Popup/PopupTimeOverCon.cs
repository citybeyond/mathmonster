using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class PopupTimeOverCon : MonoBehaviour {
	public Image handsOfClock;

	public GameObject[] tearArr;

	bool isGamePlaying = false;

	void OnEnable(){
		SoundManager.Instance.PlayEffect ("eff_mc_end_sesult_fail");

		//Time.timeScale = 0f;
		DOTween.PauseAll ();
		
		if (CtrLearnMultiplicationTables01.instance != null) {
			isGamePlaying = CtrLearnMultiplicationTables01.instance.isPlaying;
			CtrLearnMultiplicationTables01.instance.isPlaying = false;
			
		} else if (CtrLearnMultiplicationTables02.instance != null) {
			isGamePlaying = CtrLearnMultiplicationTables02.instance.isPlaying;
			CtrLearnMultiplicationTables02.instance.isPlaying = false;
			
		} else if (CtrFindEqualNums.instance != null) {
			isGamePlaying = CtrFindEqualNums.instance.isPlaying;
			CtrFindEqualNums.instance.isPlaying = false;
			
		} else if (CtrSpeedQuiz.instance != null) {
			isGamePlaying = CtrSpeedQuiz.instance.isPlaying;
			CtrSpeedQuiz.instance.isPlaying = false;
			
		}
		handsOfClock.transform.localEulerAngles = Vector3.zero;
		handsOfClock.transform.DOLocalRotate (Vector3.forward * 60f, 2f).SetEase (Ease.InOutQuad).SetLoops (-1, LoopType.Yoyo);

		for(int i = 0 ; i < tearArr.Length ; i++){
			StartCoroutine(CryMonster(tearArr[i]));
		}
	}
	
	void OnDisable(){
		//Time.timeScale = 1.0f;
		DOTween.PlayAll ();
		
		if(CtrLearnMultiplicationTables01.instance != null)
			CtrLearnMultiplicationTables01.instance.isPlaying = isGamePlaying;
		
		else if (CtrLearnMultiplicationTables02.instance != null)
			CtrLearnMultiplicationTables02.instance.isPlaying = isGamePlaying;
		
		else if (CtrFindEqualNums.instance != null)
			CtrFindEqualNums.instance.isPlaying = isGamePlaying;
		
		else if (CtrSpeedQuiz.instance != null)
			CtrSpeedQuiz.instance.isPlaying = isGamePlaying;

		handsOfClock.DOKill ();

		Transform parentForm = tearArr [0].transform.parent;
		Transform form;

		for(int i = 0 ; i < parentForm.childCount ; i++){
			form = parentForm.GetChild(i);
			if(form.name.Equals("Tear")){
				Destroy(form.gameObject);
			}
		}
	}

	public void ClickTryIncorrectQuiz(){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_basic");

		if(CtrLearnMultiplicationTables02.instance != null)
			CtrLearnMultiplicationTables02.instance.TryIncorrectQuiz ();

		else if(CtrFindEqualNums.instance != null)
			CtrFindEqualNums.instance.TryIncorrectQuiz ();

		else if(CtrSpeedQuiz.instance != null)
			CtrSpeedQuiz.instance.TryIncorrectQuiz ();

		this.gameObject.SetActive (false);
	}

	public void ClickReplayQuiz(){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_basic");

		if(CtrLearnMultiplicationTables02.instance != null)
			CtrLearnMultiplicationTables02.instance.ReplayQuiz ();

		else if(CtrFindEqualNums.instance != null)
			CtrFindEqualNums.instance.ReplayQuiz ();

		else if(CtrSpeedQuiz.instance != null)
			CtrSpeedQuiz.instance.ReplayQuiz ();

		this.gameObject.SetActive (false);
	}

	public void ClickSelectMultiLevel(){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_basic");
		Application.LoadLevel (Application.loadedLevelName);
		this.gameObject.SetActive (false);
	}

	public void ClickGoToMain(){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_basic");
		Application.LoadLevel ("3.SelectScene");
		this.gameObject.SetActive (false);
	}

	IEnumerator CryMonster(GameObject tear){
		while(true){
			float time = Random.Range(0.5f, 2.0f);
			float delay = Random.Range(1f, 2f);
			
			GameObject tearClone = Instantiate(tear) as GameObject;
			tearClone.name = "Tear";
			tearClone.transform.parent = tear.transform.parent;
			tearClone.transform.localScale = Vector3.one;
			tearClone.transform.position = tear.transform.position;
			tearClone.SetActive(true);
			
			tearClone.transform.DOLocalMoveY(-130f, time).SetEase(Ease.InCubic);
			Destroy(tearClone, time);
			yield return new WaitForSeconds(delay);
			
		}
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PopupFormulaCon : MonoBehaviour {
	public Sprite[] numSpriteArr;

	public Image leftNum;
	public Image rightNum;
	public Image tenDigit;
	public Image oneDigit;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetNumbers(int i_leftNum, int i_rightNum){
		leftNum.sprite = numSpriteArr [i_leftNum];
		rightNum.sprite = numSpriteArr [i_rightNum];
		tenDigit.sprite = numSpriteArr [(i_leftNum * i_rightNum) / 10];
		oneDigit.sprite = numSpriteArr [(i_leftNum * i_rightNum) % 10];
	}
}

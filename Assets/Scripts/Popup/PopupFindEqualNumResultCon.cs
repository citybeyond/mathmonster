﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class PopupFindEqualNumResultCon : MonoBehaviour {
	public int verticalNumber = 9;
	public int horizontalNumber = 9;

	public GameObject objBoard7x7;
	public GameObject objBoard9x9;
	public GameObject objBoardItem;

	public Text textTop;
	public Text[] textNumArr;
	public Text[] textFormulaArr;
	public Text[] textCorrectAnswerArr;

	public Sprite[] spriteBlockArr;
	public Sprite spriteCorrectBtn;
	public Sprite spriteIncorrectBtn;

	public Button[] buttonAnswer;
	public Button buttonRetryIncorrect;

	public Image[] imageHighlightArr;

	public BlockListItem[] blocks;

	private GameObject objBoard;

	bool isGamePlaying = false;

	QuizHistoryFindEqual[] quizHistoryArr;

	float ratio = 1f;

	void Start(){

	}

	void OnEnable(){
		SoundManager.Instance.PlayEffect ("eff_mc_end_sesult_success");

		Time.timeScale = 0f;
		DOTween.PauseAll ();
		
		if (CtrFindEqualNums.instance != null) {
			isGamePlaying = CtrFindEqualNums.instance.isPlaying;
			CtrFindEqualNums.instance.isPlaying = false;
			
		}

		if(blocks.Length == 0){
			Init ();
		}else if (blocks.Length == horizontalNumber) {
			ClearBlockImage ();
			ClearHighlight ();
		}
		ShowAnswer (0);
	}
	
	void OnDisable(){
		Time.timeScale = 1.0f;
		DOTween.PlayAll ();
		
		if (CtrFindEqualNums.instance != null)
			CtrFindEqualNums.instance.isPlaying = isGamePlaying;
	}

	void Init(){

		if (CtrFindEqualNums.instance.boardType == 7) {
			objBoard = objBoard7x7;
		}else if(CtrFindEqualNums.instance.boardType == 9){
			objBoard = objBoard9x9;
		}

		objBoard.SetActive (true);

		verticalNumber = CtrFindEqualNums.instance.verticalNumber;
		horizontalNumber = CtrFindEqualNums.instance.horizontalNumber;
		
		blocks = new BlockListItem[horizontalNumber];
		
		ratio = 280f / 660f;
		
		for( int i = 0; i < horizontalNumber; ++i )
		{
			blocks[i] = new BlockListItem();
			blocks[i].block = new BlockItem[verticalNumber];
			for( int j = 0; j < verticalNumber; ++j )
			{
				
				GameObject _block = Instantiate(objBoardItem) as GameObject;
				BlockItem _blockItem = _block.GetComponent<BlockItem>();
				
				_blockItem.transform.name = "Block_"+i.ToString()+"_"+j.ToString();
				_blockItem.transform.parent = objBoard.transform;
				_blockItem.transform.localPosition = new Vector3(((i * 72f * CtrFindEqualNums.instance.boardRatio) - 324f) * ratio, ((j * 72f * CtrFindEqualNums.instance.boardRatio) - 324f) * ratio);
				_blockItem.transform.localScale = ratio * Vector3.one * CtrFindEqualNums.instance.boardRatio;
				_blockItem.x = i;
				_blockItem.y = j;
				_blockItem.imgBlock.gameObject.SetActive(false);
				
				blocks[i].block[j] = _blockItem;
			}
		}
	}

	public void SetResult(QuizHistoryFindEqual[] quizHistoryArr){
		this.quizHistoryArr = quizHistoryArr;

		for (int i = 0; i < textNumArr.Length; i++) {
			textNumArr [i].gameObject.SetActive (false);
			textFormulaArr [i].gameObject.SetActive (false);
			textCorrectAnswerArr [i].gameObject.SetActive (false);
			buttonAnswer[i].gameObject.SetActive(false);
		}

		int quizCnt = quizHistoryArr.Length;
		int correctCnt = 0;
		int incorrectCnt = 0;

		for (int i = 0; i < quizCnt; i++) {
			textNumArr [i].gameObject.SetActive (true);

			int ranIndex = Random.Range(0, quizHistoryArr [i].fomulaList.Count);

			string[] strArr;
			strArr = quizHistoryArr [i].fomulaList[ranIndex].Split('X');

			textFormulaArr [i].text = string.Format ("{0}X{1}", strArr[0], strArr[1]);
			textFormulaArr [i].gameObject.SetActive (true);

			textCorrectAnswerArr [i].text = quizHistoryArr [i].multiNum.ToString ();
			textCorrectAnswerArr [i].gameObject.SetActive (true);
			buttonAnswer[i].gameObject.SetActive(true);

			if (quizHistoryArr [i].isBeenIncorrect) {
				incorrectCnt++;
				textFormulaArr [i].color= new Color(244f / 255f, 120f / 255f, 115f / 255f);
				textCorrectAnswerArr[i].GetComponent<Outline>().effectColor = new Color(1f, 0f, 0f);
				buttonAnswer[i].image.sprite = spriteIncorrectBtn;
			} else {
				correctCnt++;
				textFormulaArr [i].color = new Color(1f, 1f, 1f);
				textCorrectAnswerArr[i].GetComponent<Outline>().effectColor = new Color(130f / 255f, 64f / 255f, 30f / 255f);
				buttonAnswer[i].image.sprite = spriteCorrectBtn;
			}
		}

		float score = (float)correctCnt / (float)quizCnt * 100f;
		Debug.Log ("score is " + score);

		if (score == 100f) {
			textTop.text = "정말 대단해요!";
		} else if (score >= 80f) {
			textTop.text = "참 잘했어요!";
		} else if (score >= 60) {
			textTop.text = "잘했어요!";
		}else {
			textTop.text = "아쉬워요";
		}

		if (incorrectCnt == 0)
			buttonRetryIncorrect.interactable = false;
		else
			buttonRetryIncorrect.interactable = true;
	}

	public void ClickAnswerBtn(int index){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_basic");
		ShowAnswer (index);
	}

	public void ClickTryIncorrectQuiz(){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_basic");
		CtrFindEqualNums.instance.TryIncorrectQuiz ();
		this.gameObject.SetActive (false);
	}

	public void ClickReplayQuiz(){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_basic");
		CtrFindEqualNums.instance.ReplayQuiz ();
		this.gameObject.SetActive (false);
	}

	public void ClickSelectMultiLevel(){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_basic");
		Application.LoadLevel (Application.loadedLevelName);
		this.gameObject.SetActive (false);
	}

	public void ClickGoToMain(){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_basic");
		Application.LoadLevel ("3.SelectScene");
		this.gameObject.SetActive (false);
	}

	void ClearBlockImage()
	{
		for( int i = 0; i < horizontalNumber; ++i )
		{
			for( int j = 0; j < verticalNumber; ++j )
			{
				blocks[i].block[j].imgBlock.gameObject.SetActive(false);
			}
		}
	}
	
	void ClearHighlight(){
		for(int i = 0 ; i < imageHighlightArr.Length ; i++){
			imageHighlightArr[i].gameObject.SetActive(false);
		}
	}

	void ShowAnswer(int index){
		ClearBlockImage ();
		ClearHighlight ();
		
		imageHighlightArr [index].gameObject.SetActive (true);
		
		QuizHistoryFindEqual currentQuizHistory = quizHistoryArr [index];
		
		for(int i = 0 ; i < currentQuizHistory.posList.Count ; i++){
			string[] strArr = currentQuizHistory.posList[i].Split(',');
			int x = int.Parse(strArr[0]) - 1;
			int y = int.Parse(strArr[1]) - 1;
			
			blocks[x].block[y].imgBlock.sprite = spriteBlockArr[currentQuizHistory.colorIndexList[i]];
			blocks[x].block[y].imgBlock.gameObject.SetActive(true);
		}
	}
}

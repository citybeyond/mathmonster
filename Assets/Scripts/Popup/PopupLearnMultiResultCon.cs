using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class PopupLearnMultiResultCon : MonoBehaviour {
	public Image imageCorrectNumTenDigit;
	public Image imageCorrectNumOneDigit;
	public Image imageIncorrectNumTenDigit;
	public Image imageIncorrectNumOneDigit;

	public Text textTop;
	public Text[] textNumArr;
	public Text[] textFormulaArr;
	public Text[] textCorrectAnswerArr;

	public Sprite[] spriteCorrectNumArr;
	public Sprite[] spriteIncorrectNumArr;

	public Button buttonRetryIncorrect;

	bool isGamePlaying = false;

	void OnEnable(){
		SoundManager.Instance.PlayEffect ("eff_mc_end_sesult_success");

		Time.timeScale = 0f;
		DOTween.PauseAll ();
		
		if (CtrLearnMultiplicationTables01.instance != null) {
			isGamePlaying = CtrLearnMultiplicationTables01.instance.isPlaying;
			CtrLearnMultiplicationTables01.instance.isPlaying = false;
			
		} else if (CtrLearnMultiplicationTables02.instance != null) {
			isGamePlaying = CtrLearnMultiplicationTables02.instance.isPlaying;
			CtrLearnMultiplicationTables02.instance.isPlaying = false;
			
		} else if (CtrFindEqualNums.instance != null) {
			isGamePlaying = CtrFindEqualNums.instance.isPlaying;
			CtrFindEqualNums.instance.isPlaying = false;
			
		} else if (CtrSpeedQuiz.instance != null) {
			isGamePlaying = CtrSpeedQuiz.instance.isPlaying;
			CtrSpeedQuiz.instance.isPlaying = false;
			
		}
	}
	
	void OnDisable(){
		Time.timeScale = 1.0f;
		DOTween.PlayAll ();
		
		if(CtrLearnMultiplicationTables01.instance != null)
			CtrLearnMultiplicationTables01.instance.isPlaying = isGamePlaying;
		
		else if (CtrLearnMultiplicationTables02.instance != null)
			CtrLearnMultiplicationTables02.instance.isPlaying = isGamePlaying;
		
		else if (CtrFindEqualNums.instance != null)
			CtrFindEqualNums.instance.isPlaying = isGamePlaying;
		
		else if (CtrSpeedQuiz.instance != null)
			CtrSpeedQuiz.instance.isPlaying = isGamePlaying;
	}

	public void SetResult(QuizHistory[] quizHistoryArr){
		for (int i = 0; i < textNumArr.Length; i++) {
			textNumArr [i].gameObject.SetActive (false);
			textFormulaArr [i].gameObject.SetActive (false);
			textCorrectAnswerArr [i].gameObject.SetActive (false);
		}

		int quizCnt = quizHistoryArr.Length;
		int correctCnt = 0;
		int incorrectCnt = 0;

		for (int i = 0; i < quizCnt; i++) {
			textNumArr [i].gameObject.SetActive (true);

			textFormulaArr [i].text = string.Format ("{0}X{1}", quizHistoryArr [i].x, quizHistoryArr [i].y);
			textFormulaArr [i].gameObject.SetActive (true);

			textCorrectAnswerArr [i].text = quizHistoryArr [i].multiNum.ToString ();
			textCorrectAnswerArr [i].gameObject.SetActive (true);

			if (quizHistoryArr [i].isCorrect) {
				correctCnt++;
				textCorrectAnswerArr[i].color = new Color(240f / 255f, 190f / 255f, 15f / 255f);
			} else {
				incorrectCnt++;
				textCorrectAnswerArr[i].color = new Color(244f / 255f, 120f / 255f, 115f / 255f);
			}
		}

		imageCorrectNumTenDigit.sprite = spriteCorrectNumArr [correctCnt / 10];
		imageCorrectNumTenDigit.SetNativeSize ();
		imageCorrectNumOneDigit.sprite = spriteCorrectNumArr [correctCnt % 10];
		imageCorrectNumOneDigit.SetNativeSize ();
		imageIncorrectNumTenDigit.sprite = spriteIncorrectNumArr [incorrectCnt / 10];
		imageIncorrectNumTenDigit.SetNativeSize ();
		imageIncorrectNumOneDigit.sprite = spriteIncorrectNumArr [incorrectCnt % 10];
		imageIncorrectNumOneDigit.SetNativeSize ();

		float score = (float)correctCnt / (float)quizCnt * 100f;
		Debug.Log ("score is " + score);

		if (score == 100f) {
			textTop.text = "정말 대단해요!";
		} else if (score >= 80f) {
			textTop.text = "참 잘했어요!";
		} else if (score >= 60) {
			textTop.text = "잘했어요!";
		}else {
			textTop.text = "아쉬워요";
		}

		if (incorrectCnt == 0)
			buttonRetryIncorrect.interactable = false;
		else
			buttonRetryIncorrect.interactable = true;
	}

	public void ClickTryIncorrectQuiz(){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_basic");
		CtrLearnMultiplicationTables02.instance.TryIncorrectQuiz ();
		this.gameObject.SetActive (false);
	}

	public void ClickReplayQuiz(){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_basic");
		CtrLearnMultiplicationTables02.instance.ReplayQuiz ();
		this.gameObject.SetActive (false);
	}

	public void ClickSelectMultiLevel(){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_basic");
		Application.LoadLevel (Application.loadedLevelName);
		this.gameObject.SetActive (false);
	}

	public void ClickGoToMain(){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_basic");
		Application.LoadLevel ("3.SelectScene");
		this.gameObject.SetActive (false);
	}
}

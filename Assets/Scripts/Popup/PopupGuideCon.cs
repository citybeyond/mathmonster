using UnityEngine;
using System.Collections;

public class PopupGuideCon : MonoBehaviour {
	public GameObject checkPic;
	public string playerPrefsKey;
	bool isDontOpenInWeek = false;

	void Start(){
		System.DateTime currentTime = System.DateTime.Now;

		string str = PlayerPrefs.GetString (playerPrefsKey, "None");

		if (!str.Equals ("None")) {
			System.DateTime dateTime = System.DateTime.Parse (str);
			
			if(dateTime <= currentTime && currentTime <= dateTime.AddDays(7.0)){
				this.gameObject.SetActive(false);
			}
		}
	}

	public void ClickCheckBox(){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_basic");
		isDontOpenInWeek = !isDontOpenInWeek;
		checkPic.SetActive (isDontOpenInWeek);
	}
	                        
	public void ClickOK(){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_basic");
		if (isDontOpenInWeek) {
			PlayerPrefs.SetString(playerPrefsKey, System.DateTime.Now.ToString());
		}

		this.gameObject.SetActive (false);
	}

	public void OnDisable(){
		if (CtrLearnMultiplicationTables01.instance != null)
			CtrLearnMultiplicationTables01.instance.OpenDoor ();
		else if (CtrFindEqualNums.instance != null)
			CtrFindEqualNums.instance.OpenDoor ();
	}

}

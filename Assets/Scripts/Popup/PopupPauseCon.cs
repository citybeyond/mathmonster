using UnityEngine;
using System.Collections;
using DG.Tweening;

public class PopupPauseCon : MonoBehaviour {
	bool isGamePlaying = false;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnEnable(){
		Time.timeScale = 0f;
		DOTween.PauseAll ();

		if (CtrLearnMultiplicationTables01.instance != null) {
			isGamePlaying = CtrLearnMultiplicationTables01.instance.isPlaying;
			CtrLearnMultiplicationTables01.instance.isPlaying = false;

		} else if (CtrLearnMultiplicationTables02.instance != null) {
			isGamePlaying = CtrLearnMultiplicationTables02.instance.isPlaying;
			CtrLearnMultiplicationTables02.instance.isPlaying = false;

		} else if (CtrFindEqualNums.instance != null) {
			isGamePlaying = CtrFindEqualNums.instance.isPlaying;
			CtrFindEqualNums.instance.isPlaying = false;

		} else if (CtrSpeedQuiz.instance != null) {
			isGamePlaying = CtrSpeedQuiz.instance.isPlaying;
			CtrSpeedQuiz.instance.isPlaying = false;

		}
	}

	void OnDisable(){
		Time.timeScale = 1.0f;
		DOTween.PlayAll ();
		
		if(CtrLearnMultiplicationTables01.instance != null)
			CtrLearnMultiplicationTables01.instance.isPlaying = isGamePlaying;

		else if (CtrLearnMultiplicationTables02.instance != null)
			CtrLearnMultiplicationTables02.instance.isPlaying = isGamePlaying;
		
		else if (CtrFindEqualNums.instance != null)
			CtrFindEqualNums.instance.isPlaying = isGamePlaying;
		
		else if (CtrSpeedQuiz.instance != null)
			CtrSpeedQuiz.instance.isPlaying = isGamePlaying;
	}

	public void ClickContinue(){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_basic");
		gameObject.SetActive (false);
	}

	public void ClickGoToMain(){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_basic");
		Application.LoadLevel ("3.SelectScene");
		gameObject.SetActive (false);
	}

	public void ClickReplay(){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_basic");

		if (CtrLearnMultiplicationTables02.instance != null) {
			CtrLearnMultiplicationTables02.instance.ReplayQuiz ();
			isGamePlaying = false;
		} else if (CtrFindEqualNums.instance != null) {
			CtrFindEqualNums.instance.ReplayQuiz ();
			isGamePlaying = false;
		} else if (CtrSpeedQuiz.instance != null) {
			CtrSpeedQuiz.instance.ReplayQuiz ();
			isGamePlaying = false;
		}

		gameObject.SetActive (false);
	}
}

﻿using UnityEngine;
using System.Collections;

public class PopupQuitCon : MonoBehaviour {

	public void ClickOK(){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_basic");
		if(Application.platform == RuntimePlatform.Android){
			Application.Quit();
		}
	}

	public void ClickCancel(){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_basic");
		gameObject.SetActive (false);
	}
}

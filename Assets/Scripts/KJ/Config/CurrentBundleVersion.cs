public class CurrentBundleVersion
{
	public static readonly string bundleIdentifier = "com.motionblue.MathMonster2";

	public string version = "1.0.0";

    public static bool operator ==(CurrentBundleVersion info1, string info2)
    {
        return System.String.Compare(info1.version, info2) == 0;
    }

    public static bool operator ==(string info1, CurrentBundleVersion info2)
    {
        return System.String.Compare(info1, info2.version) == 0;
    }

    public static bool operator !=(CurrentBundleVersion info1, string info2)
    {
        return System.String.Compare(info1.version, info2) != 0;
    }

    public static bool operator !=(string info1, CurrentBundleVersion info2)
    {
        return System.String.Compare(info1, info2.version) != 0;
    }

    public static bool operator >(CurrentBundleVersion info1, string info2)
    {
        return System.String.Compare(info1.version, info2) > 0;
    }

    public static bool operator >(string info1, CurrentBundleVersion info2)
    {
        return System.String.Compare(info1, info2.version) > 0;
    }

    public static bool operator <(CurrentBundleVersion info1, string info2)
    {
        return System.String.Compare(info1.version, info2) < 0;
    }

    public static bool operator <(string info1, CurrentBundleVersion info2)
    {
        return System.String.Compare(info1, info2.version) < 0;
    }

    public static bool operator >=(CurrentBundleVersion info1, string info2)
    {
        return System.String.Compare(info1.version, info2) >= 0;
    }

    public static bool operator >=(string info1, CurrentBundleVersion info2)
    {
        return System.String.Compare(info1, info2.version) >= 0;
    }

    public static bool operator <=(CurrentBundleVersion info1, string info2)
    {
        return System.String.Compare(info1.version, info2) <= 0;
    }

    public static bool operator <=(string info1, CurrentBundleVersion info2)
    {
        return System.String.Compare(info1, info2.version) <= 0;
    }

    public override bool Equals (object obj2) {
		if (obj2 == null || !(obj2 is CurrentBundleVersion)) {
			return false;
		}
		return (this == (CurrentBundleVersion)obj2);
	}
	
	public override int GetHashCode () {
		return ToString ().GetHashCode ();
	}

	public override string ToString () {
		return string.Format ("{0}", version);
	}
}


﻿using UnityEngine;
using System.Collections;

public class Controller_Start : MonoBehaviour {

	void Awake()
	{
		DontDestroyOnLoad(this);
//		ApplicationChrome.navigationBarState = ApplicationChrome.States.Hidden;
//		StartCoroutine("CheckSoftBar");
		Screen.sleepTimeout = SleepTimeout.NeverSleep; 
	}
	
	IEnumerator CheckSoftBar () {
		yield return new WaitForSeconds(1.0f);

		if( ApplicationChrome.navigationBarState != ApplicationChrome.States.Hidden )
			ApplicationChrome.navigationBarState = ApplicationChrome.States.Hidden;

		StartCoroutine("CheckSoftBar");
	}

	// Use this for initialization
	IEnumerator Start () {
		
		yield return new WaitForSeconds(2f);
		Application.LoadLevel(1);

	}
	
	// Update is called once per frame
	void Update()
	{
		if(Application.platform == RuntimePlatform.Android)
		{
			if(Input.GetKey(KeyCode.Escape))
			{
				Application.Quit();
			}
		}
	}

			
	public void OnApplicationPause( bool pauseStatus) {
		if (pauseStatus == false) {
			ApplicationChrome.navigationBarState = ApplicationChrome.States.Hidden;
		}
	}

	

}

using UnityEngine;
using System.Collections;



public class Controller_Moblo : MonoBehaviour
{
	public static Controller_Moblo Instance;

	void Awake()
	{
		Instance = this;
	}

	public void AddMessage(string i_message)
	{
		Debug.Log ("Log "+ i_message);
//		char[] _data = new char[17];
//		_data = i_message.ToCharArray();

//		int _x = int.Parse(_data[2].ToString())-1;
//		int _y = int.Parse(_data[3].ToString())-1;
//		int _state = int.Parse(_data[6].ToString());
		
//		int _idTag = int.Parse(_data[13].ToString())*100 
//			+ int.Parse(_data[14].ToString())*10 +  int.Parse(_data[15].ToString());
//		int _tagstate = int.Parse(_data[16].ToString());

		//Custom Process
		if (CtrLearnMultiplicationTables01.instance != null) {
			CtrLearnMultiplicationTables01.instance.ReceiveData (i_message);

		} else if (CtrLearnMultiplicationTables02.instance != null) {
			CtrLearnMultiplicationTables02.instance.ReceiveData (i_message);

		} else if(CtrFindEqualNums.instance != null){
			CtrFindEqualNums.instance.ReceiveData(i_message);

		}else if(CtrSpeedQuiz.instance != null){
			CtrSpeedQuiz.instance.ReceiveData(i_message);

		}

	}



}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundManager : Singleton<SoundManager>
{
	AudioSource activeBGM;
	AudioClip preloadEffect; 
	
	void Awake()
	{
		activeBGM = null;
		preloadEffect = Resources.Load (Constants.PATH_SOUND + "button_hit") as AudioClip;
	}

	// 효과음 - 재생 (File)
	public void PlayEffect(string effect)
	{
		PlaySound(effect, Constants.DEFAULT_VOLUME);
	}

	// 효과음 - 재생 (File, Volume)
	public void PlayEffect(string effect, float volume)
	{
		PlaySound(effect, volume);
	}

	// 배경음악 - 재생 (File)
	public void PlayBGM(string music)
	{
		PlayBGM(music, Constants.DEFAULT_VOLUME);
	}

	// 배경음악 - 재생 (File, Volume)
	public void PlayBGM(string music, float volume)
	{
		if ( activeBGM != null )
		{
			// 이미 같은 배경 음악이 재생 중일 경우, 다시 재생하지 않음
			if ( activeBGM.GetComponent<AudioSource>().clip.name.Equals(music) )
				return;

			activeBGM.Stop();
			Destroy(activeBGM.gameObject);
		}

		PlaySound(music, volume, true);
	}

	// 배경음악 - 일시정지
	public void PauseBGM()
	{
		if ( activeBGM != null )
		{
			activeBGM.Pause();
		}
	}

	// 배경음악 - 이어듣기
	public void ResumeBGM()
	{
		if ( activeBGM != null )
		{
			activeBGM.Play();
		}
	}

	// 배경음악 - 종료
	public void StopBGM()
	{
		if ( activeBGM != null )
		{
			activeBGM.Stop();
			Destroy(activeBGM.gameObject);
		}
	}

	public void StopSound(string soundName){//SH
		Transform soundForm = transform.FindChild ("(Audio) " + soundName);
		if (soundForm != null)
			Destroy (soundForm.gameObject);
	}

	public AudioSource GetSound(string soundName){
		Transform soundForm = transform.FindChild ("(Audio) " + soundName);
		if (soundForm == null) {
			return null;
		} else {
			return soundForm.GetComponent<AudioSource>();
		}
	}

	#region Private Methods
	void PlaySound(string soundName, float volume)
	{
		PlaySound(soundName, volume, false);
	}

	void PlaySound(string soundName, float volume, bool isBGM)
	{
		AudioClip clip = null;
		if( preloadEffect.name == soundName )
			clip = preloadEffect;
		else 
			clip = Resources.Load (Constants.PATH_SOUND + soundName) as AudioClip;

		GameObject soundObject = new GameObject("(Audio) " + clip.name);
		soundObject.transform.parent = this.transform;
		AudioSource source = soundObject.AddComponent<AudioSource>();

		SetSource(ref source, clip, volume);

		source.loop = isBGM;
		source.Play();

		if (isBGM)
		{
			activeBGM = source;
		}
		else
		{
			Destroy(soundObject, clip.length);
		}
	}

	void SetSource(ref AudioSource source, AudioClip clip, float volume)
	{
		source.clip = clip;
//		source.volume = 0.0f;
		source.volume = volume;
	}
	
	#endregion
	
}

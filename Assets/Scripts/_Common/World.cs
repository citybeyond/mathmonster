﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BlockHistory  
{  
	public int verticalNumber = 0;
	public int horizontalNumber = 0;
	public int layerNumber = 0;
	public int indexColor = 0;
	public RectTransform rectBlockLine;    
	public string errorCode = null;
}  

public class QuizHistory{
	public bool isCorrect = false;

	public int x = 0;
	public int y = 0;
	public int multiNum = 0;
	
	public QuizHistory(int x, int y, bool isCorrect){
		this.x = x;
		this.y = y;
		this.multiNum = x * y;
		this.isCorrect = isCorrect;
	}
}

public class QuizHistoryFindEqual{
	public bool isCorrect = false;
	public bool isBeenIncorrect = false;

	public int multiNum = 0;
	public int findCorrectFomulaCnt = 0;

	public List<string> fomulaList = new List<string>();
	public List<string> posList = new List<string>();
	public List<int> colorIndexList = new List<int> ();
}

public class QuizHistorySpeedQuiz{
	public bool isCorrect = false;

	public int x = 0;
	public int y = 0;
	public int multiNum = 0;
	public int sum = 0;

	public List<string> posList = new List<string>();
	public List<int> colorIndexList = new List<int> ();

	public QuizHistorySpeedQuiz(int x, int y, bool isCorrect){
		this.x = x;
		this.y = y;
		this.multiNum = x * y;
		this.isCorrect = isCorrect;
	}

}
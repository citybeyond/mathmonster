﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public class BlockListItem  
{  
	public BlockItem[] block;    
}  

public class BlockItem : MonoBehaviour {
	
	public int x;
	public int y;

	public Image imgBlock;

	void Start()
	{
		imgBlock = this.transform.GetChild(0).GetComponent<Image>();
	}
}

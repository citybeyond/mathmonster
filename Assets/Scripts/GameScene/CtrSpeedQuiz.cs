﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;

public class CtrSpeedQuiz : MonoBehaviour {
	public static CtrSpeedQuiz instance;
	
	public int verticalNumber = 9;
	public int horizontalNumber = 9;
	public int cenVerticalNumber = 5;
	
	public GameObject monsterChar;
	
	public GameObject objLeftDoor;
	public GameObject objRightDoor;
	public GameObject objCenterDoor;
	public GameObject objReady;
	public GameObject objGo;
	public GameObject objBoard7x7;
	public GameObject objBoard9x9;
	public GameObject objBoardItem;
	public GameObject objBlockLine;
	public GameObject objIncorrectLayerIndicator;
	public GameObject objIncorrectBlockIndicator;
	
	public GameObject popupPause;
	public GameObject popupTimeOver;

	//public Image imageMonster;
	public Animator animatorMonster;
	
	public Sprite[] spriteblocks;
	public Sprite correctBlockLineSprite;
	
	public Sprite[] spriteCurrentNumberArr;
	public Sprite[] spriteMaxQuizNumberArr;
	public Sprite[] spriteCorrectNumberArr;
	public Sprite[] spriteIncorrectNumberArr;
	public Sprite[] spriteQuizNumberArr;

//	public Sprite spriteMonsterHappy;
//	public Sprite spriteMonsterCry;
//	public Sprite spriteMonsterNormal;
	
	public Image imageCurrentNumberTenDigit;
	public Image imageCurrentNumberOneDigit;
	public Image imageMaxQuizNumberTenDigit;
	public Image imageMaxQuizNumberOneDigit;
	public Image imageCorrectNumberTenDigit;
	public Image imageCorrectNumberOneDigit;
	public Image imageIncorrectNumberTenDigit;
	public Image imageIncorrectNumberOneDigit;
	public Image imageQuizNumber1;
	public Image imageQuizNumber2;
	public Image imageCorrect;
	public Image imageIncorrect;
	public Image timerGauge;
	
	public BlockListItem[] blocks;
	
	public PopupSpeedQuizResultCon popupSpeedQuizResultCon;
	
	public string narationSoundPrefix;
	
	public List<int> listSelectedMultiNum = new List<int> ();
	
	public bool isPlaying = false;
	
	public int maxQuestNum = 5;
	public int boardType = 0;

	public float bonusTimePerSelectLevel = 10.0f;
	public float boardRatio = 1f;

	private GameObject objBoard;
	
	private bool isCountDownStart = false;
	private bool isShowingLayerErr = false;
	private bool isShowingBlockErr = false;
	
	private int currentNum;
	private int correctNum;
	private int incorrectNum;
	
	private float originMaxTime = 60.0f;

	private IEnumerator checkTimeCoroutine;
	
	private List<BlockHistory> listBlockHistory = new List<BlockHistory>();
	private List<QuizHistorySpeedQuiz> listQuizHistory = new List<QuizHistorySpeedQuiz>();

	private QuizHistorySpeedQuiz currentQuizHistory = null;
	
	const string LayerNumberErrorCode = "LayerNumberError";
	const string BlockErrorCode = "BlockError";
	
	void Awake(){
		instance = this;

		boardType = PlayerPrefs.GetInt ("BoardType", 9);
		
		Debug.Log ("board type is " + boardType);
		
		if (boardType == 7) {
			objBoard = objBoard7x7;
		}else if(boardType == 9){
			objBoard = objBoard9x9;
		}
		
		objBoard.SetActive (true);
		
		horizontalNumber = boardType;
		verticalNumber = boardType;
		
		boardRatio = 9f / (float)boardType;// Default boardType is 9!
	}
	
	// Use this for initialization
	void Start () {
		SoundManager.Instance.PlayBGM ("bgm_gugudan");
		SoundManager.Instance.GetSound ("bgm_gugudan").volume = 0.5f;

		cenVerticalNumber = verticalNumber / 2 + 1;

		blocks = new BlockListItem[horizontalNumber];
		
		for( int i = 0; i < horizontalNumber; ++i )
		{
			blocks[i] = new BlockListItem();
			blocks[i].block = new BlockItem[verticalNumber];
			for( int j = 0; j < verticalNumber; ++j )
			{
				
				GameObject _block = Instantiate(objBoardItem) as GameObject;
				BlockItem _blockItem = _block.GetComponent<BlockItem>();
				
				_blockItem.transform.name = "Block_"+i.ToString()+"_"+j.ToString();
				_blockItem.transform.parent = objBoard.transform;
				_blockItem.transform.localPosition = new Vector3((i * 72f * boardRatio)-324f, ( j * 72f * boardRatio)-324f, 0f);
				_blockItem.transform.localScale = Vector3.one * boardRatio;
				_blockItem.x = i;
				_blockItem.y = j;
				_blockItem.imgBlock.gameObject.SetActive(false);
				
				blocks[i].block[j] = _blockItem;
			}
		}
		ResetBoardState ();
	}
	
	void Update()
	{
		#if UNITY_EDITOR
		if (!isPlaying)
			return;
		
		if (Input.GetMouseButtonDown(0))
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit2D hit = Physics2D.Raycast (ray.origin, ray.direction, Mathf.Infinity);
			if (hit) 
			{
				BlockItem _blockItem = hit.transform.GetComponent<BlockItem>();
				if( _blockItem != null )
				{
					if( !RemoveBlock(_blockItem.x, _blockItem.y, 0) ){
						SetPosition( _blockItem.x, _blockItem.y, 0, 0);
						CheckAddBlock(_blockItem.x, _blockItem.y, 0, 0);
						StartCoroutine (CheckBlock());
					}
					else{
						CheckRemoveBlock(_blockItem.x, _blockItem.y, 0, 0);
						RefreshBlockHistory();
					}
				}
			}
		}
		#endif
	}
	#region public functions
	public void ClearBlockImage()
	{
		for( int i = 0; i < horizontalNumber; ++i )
		{
			for( int j = 0; j < verticalNumber; ++j )
			{
				blocks[i].block[j].imgBlock.gameObject.SetActive(false);;
			}
		}
	}
	
	public void ClickPause(){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_basic");
		popupPause.SetActive (true);
	}

	public void ClickSubmit(){
		if (!isPlaying) return;
		SoundManager.Instance.PlayEffect ("eff_mc_btn_basic");
		StartCoroutine (CheckQuizAnswer());
	}
	
	public void OpenDoor(){
		MakeQuizHistory ();
		StartCoroutine (OpenDoorCoroutine());
	}
	
	public void ReceiveData(string i_message){//if in Android, this function called
		if (!isPlaying)
			return;
		
		int x = int.Parse (i_message [2].ToString()) - 1;
		int y = int.Parse (i_message [3].ToString()) - 1;
		int z = int.Parse (i_message [4].ToString()) - 1;
		
		int colorIndex = 0;
		
		switch(i_message[5]){
		case 'R':
			colorIndex = 0;
			break;
			
		case 'O':
			colorIndex = 1;
			break;
			
		case 'Y':
			colorIndex = 2;
			break;
			
		case 'G':
			colorIndex = 3;
			break;
			
		case 'B':
			colorIndex = 4;
			break;
			
		case 'I':
			colorIndex = 5;
			break;
			
		case 'V':
			colorIndex = 6;
			break;

		case 'r':
			colorIndex = 7;
			break;

		case 'o':
			colorIndex = 8;
			break;

		case 'y':
			colorIndex = 9;
			break;

		case 'g':
			colorIndex = 10;
			break;

		case 'b':
			colorIndex = 11;
			break;

		case 'i':
			colorIndex = 12;
			break;

		case 'v':
			colorIndex = 13;
			break;
			
		default:
			colorIndex = -1;
			break;
			
		}
		Debug.Log ("message : " + i_message);
		Debug.Log ("Block x is " + i_message [2].ToString() +" y is " + i_message[3].ToString() + " z is " + i_message[4].ToString() + "color is " + i_message[5].ToString());
		
		if (i_message [6] == '1') {//Put Block
			Debug.Log ("Block put");
			SetPosition(x, y, z, colorIndex);
			CheckAddBlock(x, y, z, colorIndex);
			StartCoroutine (CheckBlock());
		}else if(i_message[6] == '2'){// Remove Block
			Debug.Log ("Block Remove");

			for( int i = 0; i < listBlockHistory.Count; ++i )//Find colorIndex, Because when block removed always color string value is '0'
			{
				if( listBlockHistory[i].horizontalNumber == x && listBlockHistory[i].verticalNumber == y && listBlockHistory[i].layerNumber == z)
				{
					colorIndex = listBlockHistory[i].indexColor;
					break;
				}
			}

			if(RemoveBlock(x, y, z)){
				CheckRemoveBlock(x, y, z, colorIndex);
				RefreshBlockHistory();
			}
		}
		
		Debug.Log ("Block Data Proccess Done");
	}
	
	public void RefreshBlockHistory()
	{
		ClearBlockImage();
		
		BlockItem _block;
		for( int i = 0; i < listBlockHistory.Count; ++i )
		{
			BlockHistory _history = listBlockHistory[i];
			
			_block = blocks[_history.horizontalNumber].block[_history.verticalNumber];
			if(_history.errorCode == null){
				_block.imgBlock.sprite = spriteblocks[_history.indexColor];
				_block.imgBlock.gameObject.SetActive(true);
			}
		}
	}
	
	public bool RemoveBlock(int _x, int _y, int _z)
	{
		for( int i = 0; i < listBlockHistory.Count; ++i )
		{
			if( listBlockHistory[i].horizontalNumber == _x && listBlockHistory[i].verticalNumber == _y && listBlockHistory[i].layerNumber == _z)
			{
				GameObject _objBlockLine = null;
				if(listBlockHistory[i].rectBlockLine != null){
					_objBlockLine = listBlockHistory[i].rectBlockLine.gameObject;
				}
				listBlockHistory.Remove(listBlockHistory[i]);
				
				Destroy(_objBlockLine);
				return true;
				
			}
		}
		return false;
		
	}
	
	public void ReplayQuiz(){
		currentNum = 0;
		for(int i = 0 ; i < listQuizHistory.Count ; i++){
			listQuizHistory[i].isCorrect = false;
			listQuizHistory[i].sum = 0;
			listQuizHistory[i].posList.Clear();
			listQuizHistory[i].colorIndexList.Clear();
		}
		ResetBoardState ();
		StartCoroutine (OpenDoorCoroutine());
	}
	
	public void SetPosition(int _horizontalNum, int _verticalNum, int _layerNum, int _indexColor)
	{	
		BlockItem _block;
		
		BlockHistory _history = new BlockHistory();
		_history.horizontalNumber = _horizontalNum;
		_history.verticalNumber = _verticalNum;
		_history.indexColor = _indexColor;
		_history.layerNumber = _layerNum;
		
		
		if (_layerNum > 0) {//layerNumber error
			_history.errorCode = LayerNumberErrorCode;
			listBlockHistory.Add( _history );
			
		} else if (_indexColor == -1) {//Block error
			_history.errorCode = BlockErrorCode;
			listBlockHistory.Add( _history );
			
		} else {//None Error
			_history.errorCode = null;
			
			//monsterChar ani
			//put on block sound
			
			_block = blocks[_horizontalNum].block[_verticalNum];
			_block.imgBlock.sprite = spriteblocks[_indexColor];
			_block.imgBlock.gameObject.SetActive(true);
			
			listBlockHistory.Add( _history );
		}
	}
	
	public void TryIncorrectQuiz(){
		currentNum = 0;
		
		List<QuizHistorySpeedQuiz> tempList = new List<QuizHistorySpeedQuiz> ();
		
		for(int i = 0 ; i < listQuizHistory.Count ; i++){
			if(!listQuizHistory[i].isCorrect){
				listQuizHistory[i].isCorrect = false;
				listQuizHistory[i].sum = 0;
				listQuizHistory[i].posList.Clear();
				listQuizHistory[i].colorIndexList.Clear();
				tempList.Add (listQuizHistory[i]);
			}
		}
		
		listQuizHistory = tempList;
		
		maxQuestNum = listQuizHistory.Count;
		
		ResetBoardState ();
		
		StartCoroutine (OpenDoorCoroutine());
	}
	#endregion
	
	#region private functions

	void CheckAddBlock(int verticalNum, int horizontalNum, int layerNum, int colorIndex){
		if(layerNum == 0 && colorIndex != -1){

			bool isFind = false;
			
			isFind = currentQuizHistory.posList.Exists (x => x == string.Format("{0},{1}", verticalNum + 1, horizontalNum + 1));

			if(!isFind){
				currentQuizHistory.posList.Add(string.Format("{0},{1}", verticalNum + 1, horizontalNum + 1));
				currentQuizHistory.colorIndexList.Add (colorIndex);
				
				if(verticalNum < cenVerticalNumber - 1)
					currentQuizHistory.sum += 10;
				
				else if(verticalNum > cenVerticalNumber - 1)
					currentQuizHistory.sum += 1;

				Debug.Log ("Sum is " + currentQuizHistory.sum);
			}
		}
	}

	void CheckRemoveBlock(int verticalNum, int horizontalNum, int layerNum, int colorIndex){
		if(layerNum == 0 && colorIndex != -1){
			bool isFind = false;
			
			isFind = currentQuizHistory.posList.Exists (x => x == string.Format("{0},{1}", verticalNum + 1, horizontalNum + 1));
			
			if(isFind){
				int index = currentQuizHistory.posList.FindIndex(x => x == string.Format("{0},{1}", verticalNum + 1, horizontalNum + 1));

				currentQuizHistory.posList.RemoveAt(index);
				currentQuizHistory.colorIndexList.RemoveAt(index);

				if(verticalNum < cenVerticalNumber - 1)
					currentQuizHistory.sum -= 10;
				
				else if(verticalNum > cenVerticalNumber - 1)
					currentQuizHistory.sum -= 1;

				Debug.Log ("Sum is " + currentQuizHistory.sum);
			}
		}
	}
	
	void MakeQuizHistory(){
		for(int i = 0 ; i < maxQuestNum ; ){
			
			int x = listSelectedMultiNum[Random.Range (0, listSelectedMultiNum.Count)];
			int y = Random.Range (1, horizontalNumber + 1);
			
			if(i >= 1 && x * y == listQuizHistory[i - 1].multiNum){
				continue;
			}
			
			listQuizHistory.Add (new QuizHistorySpeedQuiz(x, y, false));
			
			i++;
		}
	}
	
	void RemoveAllBlocks(){
		BlockHistory[] historyArr = listBlockHistory.ToArray();
		
		for(int i = 0 ; i < historyArr.Length ; i++){
			RemoveBlock(historyArr[i].horizontalNumber, historyArr[i].verticalNumber, historyArr[i].layerNumber);
		}
		
		RefreshBlockHistory();
	}
	
	void ResetBoardState(){
		StopAllCoroutines ();
		DOTween.KillAll ();
		
		timerGauge.fillAmount = 1f;
		
		correctNum = 0;
		incorrectNum = 0;
		
		imageCurrentNumberTenDigit.sprite = spriteCurrentNumberArr [0];
		imageCurrentNumberTenDigit.SetNativeSize ();
		imageCurrentNumberOneDigit.sprite = spriteCurrentNumberArr [0];
		imageCurrentNumberOneDigit.SetNativeSize ();
		
		imageCorrectNumberTenDigit.sprite = spriteCorrectNumberArr [0];
		imageCorrectNumberTenDigit.SetNativeSize ();
		imageCorrectNumberOneDigit.sprite = spriteCorrectNumberArr [0];
		imageCorrectNumberOneDigit.SetNativeSize ();
		
		imageIncorrectNumberTenDigit.sprite = spriteIncorrectNumberArr [0];
		imageIncorrectNumberTenDigit.SetNativeSize ();
		imageIncorrectNumberOneDigit.sprite = spriteIncorrectNumberArr [0];
		imageIncorrectNumberOneDigit.SetNativeSize ();
		
		imageQuizNumber1.sprite = spriteQuizNumberArr [0];
		imageQuizNumber1.SetNativeSize ();
		imageQuizNumber2.sprite = spriteQuizNumberArr [0];
		imageQuizNumber2.SetNativeSize ();
		
		imageMaxQuizNumberTenDigit.sprite = spriteMaxQuizNumberArr [maxQuestNum / 10];
		imageMaxQuizNumberTenDigit.SetNativeSize ();
		imageMaxQuizNumberOneDigit.sprite = spriteMaxQuizNumberArr [maxQuestNum % 10];
		imageMaxQuizNumberOneDigit.SetNativeSize ();
		
		RemoveAllBlocks ();
		
		imageCorrect.GetComponent<CanvasGroup> ().alpha = 0f;
		imageCorrect.transform.localScale = Vector3.one;
		
		imageIncorrect.GetComponent<CanvasGroup> ().alpha = 0f;
		imageIncorrect.transform.localScale = Vector3.one;
		
		objIncorrectBlockIndicator.transform.localPosition = new Vector3(0f, -535f, 0f);
		objIncorrectBlockIndicator.transform.localPosition = new Vector3(0f, -535f, 0f);

		//imageMonster.sprite = spriteMonsterNormal;
		
		isCountDownStart = false;
		isPlaying = false;
	}
	
	void ShowResult(){
		isPlaying = false;
		popupSpeedQuizResultCon.SetResult (listQuizHistory.ToArray());
		popupSpeedQuizResultCon.gameObject.SetActive (true);
	}

	#endregion
	
	#region coroutines
	
	IEnumerator OpenDoorCoroutine(){
		objLeftDoor.transform.localPosition = new Vector3 (-164f, 0f, 0f);
		objRightDoor.transform.localPosition = new Vector3 (164f, 0f, 0f);
		objCenterDoor.SetActive (true);
		
		objReady.SetActive (true);
		string soundName = string.Format ("{0}_ready_{1}", narationSoundPrefix, Random.Range(1, 3));
		SoundManager.Instance.PlayEffect (soundName);
		float length = SoundManager.Instance.GetSound (soundName).clip.length;
		yield return new WaitForSeconds (length);
		
		objReady.SetActive (false);
		objGo.SetActive (true);
		soundName = string.Format ("{0}_go_{1}", narationSoundPrefix, Random.Range(1, 3));
		SoundManager.Instance.PlayEffect (soundName);
		length = SoundManager.Instance.GetSound (soundName).clip.length;
		yield return new WaitForSeconds (1.0f);
		
		objGo.SetActive (false);
		objCenterDoor.SetActive (false);
		SoundManager.Instance.PlayEffect("eff_mc_door");
		objLeftDoor.transform.DOLocalMoveX (-500f, 1.0f).SetEase(Ease.InCubic);
		objRightDoor.transform.DOLocalMoveX (500f, 1.0f).SetEase(Ease.InCubic);
		
		yield return new WaitForSeconds (1.0f);
		
		StartCoroutine (QuestMultiplication ());
		
	}
	
	IEnumerator QuestMultiplication(){
		if (currentNum == 0) {
			checkTimeCoroutine = CheckTime();
			StartCoroutine(checkTimeCoroutine);
		}
		
		currentNum++;
		
		if (currentNum > maxQuestNum) {
			ShowResult();
			yield break;
		}
		
		imageCurrentNumberTenDigit.sprite = spriteCurrentNumberArr[currentNum / 10];
		imageCurrentNumberTenDigit.SetNativeSize ();
		imageCurrentNumberOneDigit.sprite = spriteCurrentNumberArr[currentNum % 10];
		imageCurrentNumberOneDigit.SetNativeSize ();
		
		currentQuizHistory = listQuizHistory[currentNum - 1];
		int quizNum = currentQuizHistory.multiNum;
		
		imageQuizNumber1.sprite = spriteQuizNumberArr [currentQuizHistory.x];
		imageQuizNumber1.SetNativeSize ();
		imageQuizNumber2.sprite = spriteQuizNumberArr [currentQuizHistory.y];
		imageQuizNumber2.SetNativeSize ();
		
		animatorMonster.SetBool("Talk",true);
		string soundName = string.Format ("{0}_{1}_{2}", narationSoundPrefix, currentQuizHistory.x, currentQuizHistory.y);
		SoundManager.Instance.PlayEffect (soundName);
		float waitTime = SoundManager.Instance.GetSound(soundName).clip.length;
		yield return new WaitForSeconds(waitTime);
		animatorMonster.SetBool("Talk",false);
		
		yield return null;
		
		isPlaying = true;
	}
	
	IEnumerator CheckTime(){
		float maxTime = originMaxTime + (bonusTimePerSelectLevel * listSelectedMultiNum.Count);
		float remainTime = maxTime;
		
		Debug.Log ("MaxTime is " + maxTime);
		
		while(true){
			remainTime -= Time.deltaTime;
			timerGauge.fillAmount = remainTime / maxTime;
			
			if(!isCountDownStart && remainTime <= 5.0f){
				StartCoroutine(CountDownCoroutine());
			}
			
			if(remainTime <= 0.0f){
				isPlaying = false;
				popupTimeOver.SetActive(true);
				yield break;
			}
			
			yield return null;
		}
	}
	
	IEnumerator CheckQuizAnswer(){
		isPlaying = false;
		
		//AnswerCheck
		if(currentQuizHistory.sum == currentQuizHistory.multiNum){
			correctNum++;
			
			if(currentNum == maxQuestNum){
				StopCoroutine(checkTimeCoroutine);
			}
			
			listQuizHistory[currentNum - 1].isCorrect = true;

			SoundManager.Instance.PlayEffect("eff_mc_dish_correct");
			SoundManager.Instance.PlayEffect(string.Format("{0}_feedback_{1}", narationSoundPrefix, Random.Range(1, 4)));

			//imageMonster.sprite = spriteMonsterHappy;
			animatorMonster.SetBool("Happy",true);
			
			imageCorrectNumberTenDigit.sprite = spriteCorrectNumberArr[correctNum / 10];
			imageCorrectNumberTenDigit.SetNativeSize();
			imageCorrectNumberOneDigit.sprite = spriteCorrectNumberArr[correctNum % 10];
			imageCorrectNumberOneDigit.SetNativeSize();
			
			imageCorrect.transform.localScale = 1.5f * Vector3.one;
			
			imageCorrect.GetComponent<CanvasGroup>().DOFade(1.0f, 0.5f).SetEase(Ease.OutCubic);
			imageCorrect.transform.DOScale(1.0f, 0.5f);
			yield return new WaitForSeconds(0.5f);
			
			yield return new WaitForSeconds(1.0f);
			
			imageCorrect.GetComponent<CanvasGroup>().DOFade(0.0f, 0.5f).SetEase(Ease.OutCubic);
			yield return new WaitForSeconds(0.5f);
			animatorMonster.SetBool("Happy",false);
			
			if(currentNum < maxQuestNum){
				yield return new WaitForSeconds(0.5f);
				
				animatorMonster.SetBool("Talk",true);
				string soundName = string.Format("{0}_next", narationSoundPrefix);
				SoundManager.Instance.PlayEffect(soundName);
				float waitTime = SoundManager.Instance.GetSound(soundName).clip.length;
				yield return new WaitForSeconds(waitTime);
				animatorMonster.SetBool("Talk",false);
			}
			
		}else{
			incorrectNum++;
			
			if(currentNum == maxQuestNum){
				StopCoroutine(checkTimeCoroutine);
			}
			
			listQuizHistory[currentNum - 1].isCorrect = false;

			SoundManager.Instance.PlayEffect("eff_mc_dish_wrong");
			SoundManager.Instance.PlayEffect(string.Format("{0}_feedback_{1}", narationSoundPrefix, Random.Range(4, 6)));

			//imageMonster.sprite = spriteMonsterCry;
			animatorMonster.SetBool("Cry",true);
			
			imageIncorrectNumberTenDigit.sprite = spriteIncorrectNumberArr[incorrectNum / 10];
			imageIncorrectNumberTenDigit.SetNativeSize();
			imageIncorrectNumberOneDigit.sprite = spriteIncorrectNumberArr[incorrectNum % 10];
			imageIncorrectNumberOneDigit.SetNativeSize();
			
			imageIncorrect.transform.localScale = 1.5f * Vector3.one;
			
			imageIncorrect.GetComponent<CanvasGroup>().DOFade(1.0f, 0.5f).SetEase(Ease.OutCubic);
			imageIncorrect.transform.DOScale(1.0f, 0.5f);
			yield return new WaitForSeconds(0.5f);
			
			yield return new WaitForSeconds(1.0f);
			
			imageIncorrect.GetComponent<CanvasGroup>().DOFade(0.0f, 0.5f).SetEase(Ease.OutCubic);
			yield return new WaitForSeconds(0.5f);
			animatorMonster.SetBool("Cry",false);
			
			if(currentNum < maxQuestNum){
				yield return new WaitForSeconds(0.5f);
				
				animatorMonster.SetBool("Talk",true);
				string soundName = string.Format("{0}_next", narationSoundPrefix);
				SoundManager.Instance.PlayEffect(soundName);
				float waitTime = SoundManager.Instance.GetSound(soundName).clip.length;
				yield return new WaitForSeconds(waitTime);
				animatorMonster.SetBool("Talk",false);
			}

		}

		//imageMonster.sprite = spriteMonsterNormal;

		RemoveAllBlocks();
		
		StartCoroutine(QuestMultiplication());
		if(currentNum <= maxQuestNum)
			isPlaying = true;
	}

	IEnumerator CheckBlock(){
		if(listBlockHistory.Count > 0){
			BlockHistory tailBlockHistory = listBlockHistory[listBlockHistory.Count - 1];
			if(tailBlockHistory.errorCode == null){
				yield break;
			}
			else if(tailBlockHistory.errorCode.Equals(LayerNumberErrorCode)){
				if(!isShowingLayerErr){
					string soundName = "nar_mc_circle_direct_04";
					objIncorrectLayerIndicator.GetComponent<RectTransform>().SetAsLastSibling();
					isShowingLayerErr = true;
					SoundManager.Instance.PlayEffect ("eff_mc_noti");
					yield return StartCoroutine(ShowAndHideFormByMoveCoroutine(objIncorrectLayerIndicator.transform, -276f * Vector3.up, objIncorrectLayerIndicator.transform.localPosition, soundName));
					isShowingLayerErr = false;
				}
				
			}else if(tailBlockHistory.errorCode.Equals(BlockErrorCode)){
				if(!isShowingBlockErr){
					string soundName = string.Format("{0}_direct_01", narationSoundPrefix);
					objIncorrectBlockIndicator.GetComponent<RectTransform>().SetAsLastSibling();
					isShowingBlockErr = true;
					SoundManager.Instance.PlayEffect ("eff_mc_noti");
					yield return StartCoroutine(ShowAndHideFormByMoveCoroutine(objIncorrectBlockIndicator.transform, -276f * Vector3.up, objIncorrectBlockIndicator.transform.localPosition, soundName));
					isShowingBlockErr = false;
				}
			}
		}
	}
	
	IEnumerator CountDownCoroutine(){
		isCountDownStart = true;
		
		for(int i = 5 ; i > 0 ; i--){
			SoundManager.Instance.PlayEffect(string.Format("{0}_count_{1}" , narationSoundPrefix, i));
			yield return new WaitForSeconds(1.0f);
		}
	}
	
	IEnumerator ShowAndHideFormByMoveCoroutine(Transform form, Vector3 showPos, Vector3 hidePos, string explainSound, float moveTime = 0.5f, float showTime = 1.0f, bool isLocal = true){
		if (isLocal) {
			form.DOLocalMove(showPos, moveTime);
			yield return new WaitForSeconds(moveTime);
			
			if(explainSound == null){
				yield return new WaitForSeconds(showTime);
			}
			else{
				animatorMonster.SetBool("Talk",true);
				SoundManager.Instance.PlayEffect(explainSound);
				float length = SoundManager.Instance.GetSound(explainSound).clip.length;
				yield return new WaitForSeconds(length);
				animatorMonster.SetBool("Talk",false);
			}
			
			form.DOLocalMove(hidePos, moveTime);
			yield return new WaitForSeconds(moveTime);
			
		} else {
			form.DOMove(showPos, moveTime);
			yield return new WaitForSeconds(moveTime);
			
			if(explainSound == null){
				yield return new WaitForSeconds(showTime);
			}
			else{
				animatorMonster.SetBool("Talk",true);
				SoundManager.Instance.PlayEffect(explainSound);
				float length = SoundManager.Instance.GetSound(explainSound).clip.length;
				yield return new WaitForSeconds(length);
				animatorMonster.SetBool("Talk",false);
			}
			
			form.DOMove(hidePos, moveTime);
			yield return new WaitForSeconds(moveTime);
		}
	}
	
	#endregion
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CtrLogoScene : MonoBehaviour {
	public GameObject objGuideScreen;

	public Image imageLoadingBar;

	// Use this for initialization
	IEnumerator Start () {

		SoundManager.Instance.PlayBGM("bgm_gugudan");

		while(imageLoadingBar.fillAmount < 1.0f){
			imageLoadingBar.fillAmount += (Time.deltaTime * 0.5f);
			yield return null;
		}
		objGuideScreen.SetActive (true);
		SoundManager.Instance.PlayEffect ("nar_mc_circle_direct_01");
		yield return new WaitForSeconds (10.0f);

		Application.LoadLevel ("3.SelectScene");
	}
	
	public void ClickOK(){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_basic");
		StopCoroutine ("Start");
		Application.LoadLevel ("3.SelectScene");
	}
}

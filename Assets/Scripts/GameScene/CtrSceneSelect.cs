﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class CtrSceneSelect : MonoBehaviour {
	public GameObject blockGuidePopup;
	public GameObject quitPopup;
	public GameObject bubblePrefab;

	public Image blueMonster;
	public Image redMonster;
	public Image yellowMonster;

	public Sprite spriteBlueMonsterWakeup;
	public Sprite spriteRedMonsterWakeup;
	public Sprite spriteYellowMonsterWakeup;

	public Transform[] waterFormArr;

	// Use this for initialization
	void Start () {
		SoundManager.Instance.PlayBGM ("bgm_gugudan");
		SoundManager.Instance.GetSound ("bgm_gugudan").volume = 1.0f;

		for(int i = 0 ; i < waterFormArr.Length ; i++){
			StartCoroutine(MakeBubbleCoroutine(waterFormArr[i]));
		}

		blueMonster.transform.DOLocalMoveY (140f, Random.Range(1f, 2.0f)).SetEase (Ease.InOutQuad).SetLoops (-1, LoopType.Yoyo);
		redMonster.transform.DOLocalMoveY (140f, Random.Range(1f, 2.0f)).SetEase (Ease.InOutQuad).SetLoops (-1, LoopType.Yoyo);
		yellowMonster.transform.DOLocalMoveY (140f, Random.Range(1f, 2.0f)).SetEase (Ease.InOutQuad).SetLoops (-1, LoopType.Yoyo);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ClickLearnMultiplicationTables(){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_main");
		blueMonster.sprite = spriteBlueMonsterWakeup;

		Application.LoadLevel("4_1.LearnMulti01");
	}

	public void ClickFindSameEqualNums(){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_main");
		redMonster.sprite = spriteRedMonsterWakeup;

		Application.LoadLevel("4_2.FindEqualNums");
	}

	public void ClickSpeedQuiz(){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_main");
		yellowMonster.sprite = spriteYellowMonsterWakeup;

		Application.LoadLevel("4_3.SpeedQuiz");
	}

	public void ClickBlockGuide(){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_basic");
		blockGuidePopup.SetActive (true);
	}
	public void ClickClose(){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_basic");
		quitPopup.SetActive (true);
	}

	IEnumerator MakeBubbleCoroutine(Transform waterForm){
		while(true){
			float time = Random.Range(0.5f, 2.0f);
			float delay = Random.Range(0.25f, 1.0f);
			float xLocalPosOffset = Random.Range(-90f, 90f);

			GameObject bubble = Instantiate(bubblePrefab) as GameObject;
			bubble.transform.position = waterForm.parent.position;
			bubble.transform.SetParent(waterForm);
			bubble.transform.localScale = Vector3.one * Random.Range(0.2f, 1.0f);
			bubble.transform.localPosition += Vector3.right * xLocalPosOffset;

			bubble.transform.DOLocalMoveY(70f, time).SetEase(Ease.InOutCubic);
			Destroy(bubble, time);
			yield return new WaitForSeconds(delay);

		}
	}
}

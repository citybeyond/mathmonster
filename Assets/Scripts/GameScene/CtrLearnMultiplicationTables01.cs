﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;

public class CtrLearnMultiplicationTables01 : MonoBehaviour {
	public static CtrLearnMultiplicationTables01 instance;
	
	public int verticalNumber = 9;
	public int horizontalNumber = 9;

	public GameObject objLeftDoor;
	public GameObject objRightDoor;
	public GameObject objCenterDoor;
	public GameObject objReady;
	public GameObject objGo;
	public GameObject objBoard7x7;
	public GameObject objBoard9x9;
	public GameObject objBoardItem;
	public GameObject objBlockLine;
	public GameObject objPlayStartIndicator;
	public GameObject objCorrectIndicator;
	public GameObject objIncorrectLayerIndicator;
	public GameObject objIncorrectBlockIndicator;
	public GameObject popupPause;

	public PopupFormulaCon popupFormula;

	public Animator animatorMonster;

	public Sprite[] spriteblocks;
	public Sprite correctBlockLineSprite;

	public BlockListItem[] blocks;

	public string narationSoundPrefix;

	public bool isPlaying = false;

	private List<BlockHistory> listBlockHistory = new List<BlockHistory>();
	private List<BlockHistory> listFormulaHistory = new List<BlockHistory>();

	private GameObject objBoard;

	private int boardType = 0;
	private float boardRatio = 1f;

	const string LayerNumberErrorCode = "LayerNumberError";
	const string BlockErrorCode = "BlockError";

	void Awake(){
		instance = this;

		boardType = PlayerPrefs.GetInt ("BoardType", 9);

		Debug.Log ("board type is " + boardType);

		if (boardType == 7) {
			objBoard = objBoard7x7;
		}else if(boardType == 9){
			objBoard = objBoard9x9;
		}
		else
			objBoard = objBoard9x9;


		objBoard.SetActive (true);

		horizontalNumber = boardType;
		verticalNumber = boardType;

		boardRatio = 9f / (float)boardType;// Default boardType is 9!

	}

	// Use this for initialization
	void Start () {
		SoundManager.Instance.PlayBGM ("bgm_gugudan");
		SoundManager.Instance.GetSound ("bgm_gugudan").volume = 0.5f;

		blocks = new BlockListItem[horizontalNumber];

		for( int i = 0; i < horizontalNumber; ++i )
		{
			blocks[i] = new BlockListItem();
			blocks[i].block = new BlockItem[verticalNumber];
			for( int j = 0; j < verticalNumber; ++j )
			{

				GameObject _block = Instantiate(objBoardItem) as GameObject;
				BlockItem _blockItem = _block.GetComponent<BlockItem>();
				
				_blockItem.transform.name = "Block_"+i.ToString()+"_"+j.ToString();
				_blockItem.transform.parent = objBoard.transform;
				_blockItem.transform.localPosition = new Vector3((i * 72f * boardRatio) - 324f, (j * 72f * boardRatio) - 324f, 0f);
				_blockItem.transform.localScale = Vector3.one * boardRatio;
				_blockItem.x = i;
				_blockItem.y = j;
				_blockItem.imgBlock.gameObject.SetActive(false);

				blocks[i].block[j] = _blockItem;
			}
		}

		StartCoroutine (ShowAndHideFomulaCoroutine());
	}

	void Update()
	{
#if UNITY_EDITOR
		if (!isPlaying)
			return;

		if (Input.GetMouseButtonDown(0))
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit2D hit = Physics2D.Raycast (ray.origin, ray.direction, Mathf.Infinity);
			if (hit) 
			{
				BlockItem _blockItem = hit.transform.GetComponent<BlockItem>();
				if( _blockItem != null )
				{
					if( !RemoveBlock(_blockItem.x, _blockItem.y, 0) )
						SetPosition( _blockItem.x, _blockItem.y, 0, Random.Range(0,7)); 
					else
						RefreshBlockHistory();
				}
			}
		}
#endif
	}
	#region public functions
	public void ClearBlockImage()
	{
		for( int i = 0; i < horizontalNumber; ++i )
		{
			for( int j = 0; j < verticalNumber; ++j )
			{
				blocks[i].block[j].imgBlock.gameObject.SetActive(false);;
			}
		}
	}

	public void ClickPause(){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_basic");
		popupPause.SetActive (true);
	}
	
	public void ClickStartGame(){
		SoundManager.Instance.PlayEffect ("eff_mc_btn_basic");
		Application.LoadLevel ("4_1.LearnMulti02");
	}

	public void OpenDoor(){
		StartCoroutine (OpenDoorCoroutine());
	}

	public void ReceiveData(string i_message){//if in Android, this function called
		if (!isPlaying)
			return;
		
		int x = int.Parse (i_message [2].ToString()) - 1;
		int y = int.Parse (i_message [3].ToString()) - 1;
		int z = int.Parse (i_message [4].ToString()) - 1;
		
		int colorIndex = 0;
		
		switch(i_message[5]){
		case 'R':
		case 'r':
			colorIndex = 0;
			break;
			
		case 'O':
		case 'o':
			colorIndex = 1;
			break;
			
		case 'Y':
		case 'y':
			colorIndex = 2;
			break;
			
		case 'G':
		case 'g':
			colorIndex = 3;
			break;
			
		case 'B':
		case 'b':
			colorIndex = 4;
			break;
			
		case 'I':
		case 'i':
			colorIndex = 5;
			break;
			
		case 'V':
		case 'v':
			colorIndex = 6;
			break;
			
		default:
			colorIndex = -1;
			break;
			
		}

		Debug.Log ("Block x is " + i_message [2].ToString() +" y is " + i_message[3].ToString() + " z is " + i_message[4].ToString() + "color is " + i_message[5].ToString());

		if (i_message [6] == '1') {//Put Block
			Debug.Log ("Block put");
			SetPosition(x, y, z, colorIndex);
		}else if(i_message[6] == '2'){// Remove Block
			Debug.Log ("Block Remove");
			if(RemoveBlock(x, y, z))
				RefreshBlockHistory();
		}

		Debug.Log ("Block Data Proccess Done");
	}

	public void RefreshBlockHistory()
	{
		ClearBlockImage();

		BlockItem _block;
		for( int i = 0; i < listBlockHistory.Count; ++i )
		{
			BlockHistory _history = listBlockHistory[i];
			for( int _x = 0; _x < _history.horizontalNumber+1; ++_x )
			{
				for( int _y = 0; _y < _history.verticalNumber+1; ++_y )
				{
					_block = blocks[_x].block[_y];
					if(_history.errorCode == null){
						_block.imgBlock.sprite = spriteblocks[_history.indexColor];
						_block.imgBlock.gameObject.SetActive(true);
					}
				}
			}
		}
	}

	public bool RemoveBlock(int _x, int _y, int _z)
	{
		for( int i = 0; i < listBlockHistory.Count; ++i )
		{
			if( listBlockHistory[i].horizontalNumber == _x && listBlockHistory[i].verticalNumber == _y && listBlockHistory[i].layerNumber == _z)
			{
				GameObject _objBlockLine = null;
				if(listBlockHistory[i].rectBlockLine != null){
					_objBlockLine = listBlockHistory[i].rectBlockLine.gameObject;
				}
				listFormulaHistory.Remove(listBlockHistory[i]);
				listBlockHistory.Remove(listBlockHistory[i]);

				Destroy(_objBlockLine);
				return true;

			}
		}
		return false;

	}

	public void SetPosition(int _horizontalNum, int _verticalNum, int _layerNum, int _indexColor)
	{
		HidePlayStartIndicator ();
		
		BlockItem _block;
		
		BlockHistory _history = new BlockHistory();
		_history.horizontalNumber = _horizontalNum;
		_history.verticalNumber = _verticalNum;
		_history.indexColor = _indexColor;
		_history.layerNumber = _layerNum;
		
		
		if (_layerNum > 0) {//layerNumber error
			_history.errorCode = LayerNumberErrorCode;
			listBlockHistory.Add( _history );
			listFormulaHistory.Add ( _history );
			
		} else if (_indexColor == -1) {//Block error
			_history.errorCode = BlockErrorCode;
			listBlockHistory.Add( _history );
			listFormulaHistory.Add ( _history );
			
		} else {//None Error
			_history.errorCode = null;
			
			//monsterChar ani
			//put on block sound
			
			for( int i = 0; i < _horizontalNum+1; ++i )
			{
				for( int j = 0; j < _verticalNum+1; ++j )
				{
					_block = blocks[i].block[j];
					_block.imgBlock.sprite = spriteblocks[_indexColor];
					_block.imgBlock.gameObject.SetActive(true);
				}
			}

			GameObject _blockLine = Instantiate(objBlockLine) as GameObject;
			_blockLine.name = "_BlockLine_"+_horizontalNum.ToString()+"_"+_verticalNum.ToString();
			_blockLine.transform.parent = objBoard.transform;
			_blockLine.transform.localPosition = new Vector3(-324f,-324f,0f);
			_blockLine.transform.localScale = Vector3.one;

			RectTransform _rectBlockLine = _blockLine.GetComponent<RectTransform>();
			_rectBlockLine.sizeDelta = new Vector2( (_horizontalNum+1) * 72f * boardRatio, (_verticalNum+1) * 72f * boardRatio);
			_history.rectBlockLine = _rectBlockLine;
			
			listBlockHistory.Add( _history );
			listFormulaHistory.Add ( _history );

			//Find same value and Change correctBlockLine
			for(int i = 0 ; i < listBlockHistory.Count; i++){
				for(int j = 0 ; j < listBlockHistory.Count ; j++){
					if(i != j && listBlockHistory[i].layerNumber == 0 && listBlockHistory[j].layerNumber == 0){
						
						if((listBlockHistory[i].horizontalNumber + 1) * (listBlockHistory[i].verticalNumber + 1) == (listBlockHistory[j].horizontalNumber + 1) * (listBlockHistory[j].verticalNumber + 1)){
							Image tempImage = listBlockHistory[i].rectBlockLine.GetComponent<Image>();
							tempImage.sprite = correctBlockLineSprite;

						}
					}
				}
			}
		}
	}
	#endregion

	#region private functions
	void FadeAlpha(Image image, float endValue, float time, bool isIncludeChildren = true){
		if (isIncludeChildren) {
			Image[] imgArr = image.GetComponentsInChildren<Image>(true);
			for(int i = 0 ; i < imgArr.Length ; i++){
				imgArr[i].DOFade(endValue, time);
			}
		}
		
		image.DOFade(endValue, time);
	}

	void HidePlayStartIndicator(){
		StopCoroutine (UpDownPlayStartIndicatorCoroutine());
		objPlayStartIndicator.transform.DOKill ();
		objPlayStartIndicator.transform.DOLocalMoveY (-535, 1).SetEase (Ease.OutCubic);
	}

	void ShowPlayStartIndicator(){
		SoundManager.Instance.PlayEffect ("eff_mc_noti");
		objPlayStartIndicator.transform.DOLocalMoveY (-276f, 1).SetEase (Ease.OutCubic);
	}
	#endregion

	#region coroutines
	IEnumerator OpenDoorCoroutine(){

		objReady.SetActive (true);
		string soundName = string.Format ("{0}_ready_{1}", narationSoundPrefix, Random.Range(1, 3));
		SoundManager.Instance.PlayEffect (soundName);
		float length = SoundManager.Instance.GetSound (soundName).clip.length;
		yield return new WaitForSeconds (length);
		
		objReady.SetActive (false);
		objGo.SetActive (true);
		soundName = string.Format ("{0}_go_{1}", narationSoundPrefix, Random.Range(1, 3));
		SoundManager.Instance.PlayEffect (soundName);
		length = SoundManager.Instance.GetSound (soundName).clip.length;
		yield return new WaitForSeconds (1.0f);
		
		objGo.SetActive (false);
		objCenterDoor.SetActive (false);
		SoundManager.Instance.PlayEffect("eff_mc_door");
		objLeftDoor.transform.DOLocalMoveX (-500f, 1.0f).SetEase(Ease.InCubic);
		objRightDoor.transform.DOLocalMoveX (500f, 1.0f).SetEase(Ease.InCubic);
		
		yield return new WaitForSeconds (1.0f);
		
		isPlaying = true;
		
		StartCoroutine (UpDownPlayStartIndicatorCoroutine());
	}

	IEnumerator ShowAndHideFomulaCoroutine(){
		while(true){
			if(listFormulaHistory.Count > 0){

				BlockHistory headFormulaHistory = listFormulaHistory[0];
				if(headFormulaHistory.errorCode == null){
					popupFormula.SetNumbers(headFormulaHistory.horizontalNumber + 1, headFormulaHistory.verticalNumber + 1);

					string soundName = string.Format("{0}_{1}_{2}", narationSoundPrefix, headFormulaHistory.horizontalNumber + 1, headFormulaHistory.verticalNumber + 1);

					SoundManager.Instance.PlayEffect ("eff_mc_popup_number");
					yield return StartCoroutine(ShowAndHideFormByMoveCoroutine(popupFormula.transform, 205 * Vector3.up, popupFormula.transform.localPosition, soundName));

					List<BlockHistory> sameMulValList = new List<BlockHistory>();
					
					for(int i = 0 ; i < listBlockHistory.Count ; i++){
						if((headFormulaHistory.horizontalNumber + 1) * (headFormulaHistory.verticalNumber + 1) == (listBlockHistory[i].horizontalNumber + 1) * (listBlockHistory[i].verticalNumber + 1)){
							sameMulValList.Add(listBlockHistory[i]);
						}
					}
					
					if(sameMulValList.Count > 0 && headFormulaHistory != sameMulValList[0]){
						SoundManager.Instance.PlayEffect ("eff_mc_dish_correct");
						animatorMonster.SetBool("Happy",true);


						for(int i = 0 ; i < listBlockHistory.Count ; i++){
							if((headFormulaHistory.horizontalNumber + 1) * (headFormulaHistory.verticalNumber + 1) == (listBlockHistory[i].horizontalNumber + 1) * (listBlockHistory[i].verticalNumber + 1)){
								Image tempImage = listBlockHistory[i].rectBlockLine.GetComponent<Image>();
								tempImage.DOKill();
								tempImage.color = new Color(1f, 1f, 1f, 1f);
								tempImage.DOFade(0f, 0.5f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);

							}
						}

						yield return StartCoroutine(ShowAndHideFormByMoveCoroutine(objCorrectIndicator.transform, 200f * Vector3.up, objCorrectIndicator.transform.localPosition, null));

						animatorMonster.SetBool("Happy",false);

						for(int i = 0 ; i < listBlockHistory.Count ; i++){
							if((headFormulaHistory.horizontalNumber + 1) * (headFormulaHistory.verticalNumber + 1) == (listBlockHistory[i].horizontalNumber + 1) * (listBlockHistory[i].verticalNumber + 1)){
								Image tempImage = listBlockHistory[i].rectBlockLine.GetComponent<Image>();
								tempImage.DOKill();
								tempImage.color = new Color(1f, 1f, 1f, 1f);
								
							}
						}
					}
				}

				else if(headFormulaHistory.errorCode.Equals(LayerNumberErrorCode)){
					SoundManager.Instance.PlayEffect ("eff_mc_noti");
					string soundName = "nar_mc_circle_direct_04";
					yield return StartCoroutine(ShowAndHideFormByMoveCoroutine(objIncorrectLayerIndicator.transform, -276f * Vector3.up, objIncorrectLayerIndicator.transform.localPosition, soundName));

				}else if(headFormulaHistory.errorCode.Equals(BlockErrorCode)){
					SoundManager.Instance.PlayEffect ("eff_mc_noti");
					string soundName = string.Format("{0}_direct_01", narationSoundPrefix);
					yield return StartCoroutine(ShowAndHideFormByMoveCoroutine(objIncorrectBlockIndicator.transform, -276f * Vector3.up, objIncorrectBlockIndicator.transform.localPosition, soundName));

				}

				listFormulaHistory.Remove(headFormulaHistory);
			}else{
				yield return null;
			}
		}
	}

	IEnumerator ShowAndHideFormByMoveCoroutine(Transform form, Vector3 showPos, Vector3 hidePos, string explainSound, float moveTime = 0.5f, float showTime = 1.0f, bool isLocal = true){
		if (isLocal) {
			form.DOLocalMove(showPos, moveTime);
			yield return new WaitForSeconds(moveTime);

			if(explainSound == null){
				yield return new WaitForSeconds(showTime);
			}
			else{
				
				animatorMonster.SetBool("Talk",true);
				SoundManager.Instance.PlayEffect(explainSound);
				float length = SoundManager.Instance.GetSound(explainSound).clip.length;
				yield return new WaitForSeconds(length);
				animatorMonster.SetBool("Talk",false);
			}

			form.DOLocalMove(hidePos, moveTime);
			yield return new WaitForSeconds(moveTime);

		} else {
			form.DOMove(showPos, moveTime);
			yield return new WaitForSeconds(moveTime);
			
			if(explainSound == null){
				yield return new WaitForSeconds(showTime);
			}
			else{
				animatorMonster.SetBool("Talk",true);
				SoundManager.Instance.PlayEffect(explainSound);
				float length = SoundManager.Instance.GetSound(explainSound).clip.length;
				yield return new WaitForSeconds(length);
				animatorMonster.SetBool("Talk",false);
			}
			
			form.DOMove(hidePos, moveTime);
			yield return new WaitForSeconds(moveTime);
		}
	}

	IEnumerator UpDownPlayStartIndicatorCoroutine(){
		ShowPlayStartIndicator ();
		yield return new WaitForSeconds (1.0f);
		
		animatorMonster.SetBool("Talk",true);
		SoundManager.Instance.PlayEffect ("nar_mc_circle_direct_02");
		float length = SoundManager.Instance.GetSound ("nar_mc_circle_direct_02").clip.length;
		yield return new WaitForSeconds (length);
		animatorMonster.SetBool("Talk",false);

		HidePlayStartIndicator ();
	}
	#endregion
}

﻿#pragma warning disable 414
using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;

public class UIDepthManager : EditorWindow
{
	//[MenuItem("GameObject/Show Depth")]
	//static void ValidateLogSelectedTransformName()
	//{
	//    // Return false if no transform is selected.
	//    //return Selection.activeTransform != null;
	//}

	[MenuItem("GameObject/Depth Manager 2")]
	static void Init()
	{
		UIDepthManager window = EditorWindow.GetWindow<UIDepthManager>();
		window.GetWidgetsInChildren();
	}

	GameObject target;

	UIPanel[] panels;
	
	List<GameObject> children;

	private void GetWidgetsInChildren()
	{
		target = Selection.activeGameObject;

		for (int i = 0; i < target.transform.childCount; i++)
		{
			if (target.transform.GetChild(i))
			{

			}
		}

		if (target != null)
		{

			panels = target.GetComponentsInChildren<UIPanel>(true);
			widgets = target.GetComponentsInChildren<UIWidget>(true);
		}
		else
		{
			panels = null;
			widgets = null;
		}
	}

	UIWidget[] widgets;
	Vector2 scrollPosition = Vector2.zero;
	void OnGUI()
	{

		EditorGUILayout.BeginVertical();
		{
			DrawRefreshButton();

			if (target != null)
			{
				NGUIEditorTools.DrawHeader(target.name);

				DrawTarget();

				NGUIEditorTools.DrawSeparator();

				scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);
				{
					DrawContents();
				}
				EditorGUILayout.EndScrollView();
			}
		}
		EditorGUILayout.EndVertical();

		if (GUI.changed)
		{
			if (target != null)
			{
				EditorUtility.SetDirty(target);
			}
		}
	}
	const string Root = "Root";
	const string IncrementSign = "+";
	const string DecrementSign = "-";
	const string DepthChange = "Depth Change";
	const string Refresh = "Refresh";

	const float ButtonHeight = 30;
	const int ButtonWidth = 20;
	const int DepthIntFieldWidth = 25;

	private void DrawTarget()
	{
		if (widgets == null)
		{
			return;
		}

		NGUIEditorTools.BeginContents();
		{
			EditorGUILayout.BeginHorizontal();
			{
				EditorGUILayout.ObjectField(Root, target, typeof(GameObject), true);

				if (GUILayout.Button(DecrementSign, GUILayout.Width(20)))
				{
					Undo.RecordObjects(widgets, DepthChange);
					foreach (UIWidget widget in widgets)
					{
						widget.depth--;
					}
				}

				if (GUILayout.Button(IncrementSign, GUILayout.Width(20)))
				{
					Undo.RecordObjects(widgets, DepthChange);
					foreach (UIWidget widget in widgets)
					{
						widget.depth++;
					}
				}
			}
			EditorGUILayout.EndHorizontal();
		}
		NGUIEditorTools.EndContents();
	}


	private void DrawContents()
	{
		if (widgets == null)
		{
			return;
		}

		NGUIEditorTools.BeginContents();
		{
			
			foreach (UIWidget widget in widgets)
			{
				if (widget == null)
				{
					continue;
				}
				EditorGUILayout.BeginHorizontal();
				{

					EditorGUILayout.ObjectField(
						widget.GetType().Name, 
						widget, 
						typeof(UIWidget), 
						true);

					if (GUILayout.Button(DecrementSign, GUILayout.Width(ButtonWidth)))
					{
						Undo.RecordObject(widget, DepthChange);
						widget.depth--;
					}

					widget.depth = EditorGUILayout.IntField(widget.depth, GUILayout.Width(DepthIntFieldWidth));

					if (GUILayout.Button(IncrementSign, GUILayout.Width(ButtonWidth)))
					{
						Undo.RecordObject(widget, DepthChange);
						widget.depth++;
					}
				}
				EditorGUILayout.EndHorizontal();
			}
		}
		NGUIEditorTools.EndContents();

	}

	private void DrawRefreshButton()
	{
		EditorGUILayout.BeginHorizontal();
		{
			if (GUILayout.Button(Refresh, GUILayout.Height(ButtonHeight)))
			{
				GetWidgetsInChildren();
			}
		}
		EditorGUILayout.EndHorizontal();
	}


}

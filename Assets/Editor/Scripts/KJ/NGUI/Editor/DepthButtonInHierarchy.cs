﻿#pragma warning disable 414
using UnityEngine;
using System.Collections;
using UnityEditor;

[InitializeOnLoad]
public class DepthButtonInHierarchy
{
    static DepthButtonInHierarchy()
    {
        EditorApplication.hierarchyWindowItemOnGUI += hierarchyWindowItemOnGUI;
    }

    [MenuItem("GameObject/Show Depth In Hierarchy", true)]
    static bool ShowDepthInHierarchy()
    {
        return EditorPrefs.GetBool("ShowDepthInHierarchy", true);
    }

    // Have we loaded the prefs yet
    private static bool prefsLoaded = false;

    // The Preferences
    public static bool sBoolPreference = false;

    [PreferenceItem("NGUI")]
    static void PreferencesGUIForNGUI()
    {
        // LoadMap the preferences
        if (!prefsLoaded)
        {
            sBoolPreference = EditorPrefs.GetBool("ShowDepthInHierarchy", true);

            prefsLoaded = true;
        }

        // Preferences GUI
        sBoolPreference = EditorGUILayout.Toggle("Show Depth In Hierarchy", sBoolPreference);

        //sFontActiveColor = EditorGUILayout.ColorField("Font Active Color", sFontActiveColor);

        // Save the preferences
        if (GUI.changed)
        {
            EditorPrefs.SetBool("ShowDepthInHierarchy", sBoolPreference);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="instanceID"></param>
    /// <param name="selectionRect"></param>
    static void hierarchyWindowItemOnGUI(int instanceID, Rect selectionRect)
    {
        if (EditorPrefs.GetBool("ShowDepthInHierarchy", true) == false)
        {
            return;
        }

        IfUIWidgetThenShowDepth(instanceID, selectionRect);
    }

    const float ButtonWidth = 20;
    const float IntFieldWidth = 20;
    const float FullWidth = ButtonWidth * 2/*two buttons*/ + IntFieldWidth;
    const string IncrementSign = "+";
    const string DecrementSign = "-";
    const string DepthChange = "Depth Change";

    static Rect rectRow = new Rect();
    static UIWidget sWidget;
    static UIPanel sPanel;
    static object obj;
    static float first;
    static int depth = 0;
    static GameObject gameObject;
    static UIToggle sToggle;
    private static void IfUIWidgetThenShowDepth(int instanceID, Rect selectionRect)
    {
        obj = EditorUtility.InstanceIDToObject(instanceID);

        if (obj is GameObject)
        {
            gameObject = obj as GameObject;

            sWidget = gameObject.GetComponent<UIWidget>();
            sPanel = gameObject.GetComponent<UIPanel>();

            if (sWidget != null)
            {
                DrawButtons(gameObject, sWidget.depth, selectionRect, BlueColor);

                if (depth != sWidget.depth)
                {
                    Undo.RecordObject(sWidget, DepthChange);
                    sWidget.depth = depth;
                }
            }

            if (sPanel != null)
            {
                //if (gameObject.activeInHierarchy)
                //{
                //    DrawRow(ref selectionRect, sBackgroundActiveColor, sFontActiveColor);
                //}
                //else
                //{
                //    DrawRow(ref selectionRect, sBackgroundInactiveColor, sFontInactiveColor);
                //}

                DrawButtonsForPanel(gameObject, sPanel.depth, selectionRect, Color.yellow);

                if (depth != sPanel.depth)
                {
                    Undo.RecordObject(sPanel, DepthChange);
                    sPanel.depth = depth;
                }

            }

            sToggle = gameObject.GetComponent<UIToggle>();
            if (null != sToggle)
            {
                int changeGroup;
                DrawToggle(gameObject, sToggle.group, selectionRect, Color.cyan, out changeGroup);

                if (changeGroup != sToggle.group)
                {
                    Undo.RecordObject(sToggle, string.Format("{0}: Toggle change group", gameObject.name));
                    sToggle.group = changeGroup;
                }
            }
        }
    }

    static Color sFontActiveColor = new Color(180.0f / 255, 180.0f / 255, 180.0f / 255);
    static Color sFontInactiveColor = new Color(128.0f / 255, 128.0f / 255, 128.0f / 255);
    static Color sBackgroundActiveColor = new Color(56.0f / 255, 56.0f / 255, 56.0f / 255);
    static Color sBackgroundInactiveColor = new Color(0, 0, 0);
    static Color BlueColor = new Color(0, 0, 1);
    private static void DrawRow(ref Rect selectionRect, Color rectColor, Color fontColorParam)
    {
        Color fontColor = GUI.color;
        GUI.color = fontColorParam;

        EditorGUI.DrawRect(selectionRect, rectColor);

        EditorGUI.LabelField(selectionRect, gameObject.name);

        GUI.color = fontColor;
    }

    private static void DrawButtons(GameObject gameObject, int depthParam, Rect selectionRect, Color backgroundColor)
    {
        // from selectionRect

        // "+" button
        rectRow.x = selectionRect.x + selectionRect.width - FullWidth;
        rectRow.y = selectionRect.y;
        rectRow.width = selectionRect.width;
        rectRow.height = selectionRect.height;

        rectRow.width = ButtonWidth;

        depth = depthParam;

        if (GUI.Button(rectRow, DecrementSign))
        {
            depth--;
        }

        // depth field
        rectRow.x = rectRow.x + ButtonWidth;
        rectRow.width = IntFieldWidth;

        tempColor = GUI.backgroundColor;
        GUI.backgroundColor = backgroundColor;
        depth = EditorGUI.IntField(rectRow, depth);

        GUI.backgroundColor = tempColor;
        // "-" button
        rectRow.x = rectRow.x + IntFieldWidth;
        rectRow.width = ButtonWidth;

        if (GUI.Button(rectRow, IncrementSign))
        {
            depth++;
        }
    }
    static Color tempColor;

    private static void DrawButtonsForPanel(GameObject gameObject, int depthParam, Rect selectionRect, Color backgroundColor)
    {

        // from selectionRect

        // "+" button
        rectRow.x = selectionRect.x + selectionRect.width - FullWidth;
        rectRow.y = selectionRect.y;
        rectRow.width = selectionRect.width;
        rectRow.height = selectionRect.height;

        rectRow.width = ButtonWidth;

        depth = depthParam;

        if (GUI.Button(rectRow, DecrementSign))
        {
            UIPanel[] panels = gameObject.GetComponentsInChildren<UIPanel>(true);

            foreach (UIPanel panel in panels)
            {
                if (panel.gameObject != gameObject)
                {
                    panel.depth--;
                }
            }

            depth--;
        }

        // depth field
        rectRow.x = rectRow.x + ButtonWidth;
        rectRow.width = IntFieldWidth;

        tempColor = GUI.backgroundColor;
        GUI.backgroundColor = backgroundColor;
        depth = EditorGUI.IntField(rectRow, depth);

        GUI.backgroundColor = tempColor;
        // "-" button
        rectRow.x = rectRow.x + IntFieldWidth;
        rectRow.width = ButtonWidth;

        if (GUI.Button(rectRow, IncrementSign))
        {
            UIPanel[] panels = gameObject.GetComponentsInChildren<UIPanel>(true);

            foreach (UIPanel panel in panels)
            {
                if (panel.gameObject != gameObject)
                {
                    panel.depth++;
                }
            }

            depth++;
        }
    }

    private static void DrawToggle(GameObject gameObject, int groupParam, Rect selectionRect, Color backgroundColor, out int changeGroupParam)
    {
        rectRow.x = selectionRect.x + selectionRect.width - FullWidth;
        rectRow.y = selectionRect.y;
        rectRow.width = selectionRect.width;
        rectRow.height = selectionRect.height;

        rectRow.width = ButtonWidth;

        changeGroupParam = groupParam;

        if (GUI.Button(rectRow, DecrementSign))
        {
            --changeGroupParam;
        }

        // depth field
        rectRow.x = rectRow.x + ButtonWidth;
        rectRow.width = IntFieldWidth;

        tempColor = GUI.backgroundColor;
        GUI.backgroundColor = backgroundColor;
        changeGroupParam = EditorGUI.IntField(rectRow, changeGroupParam);

        GUI.backgroundColor = tempColor;
        // "-" button
        rectRow.x = rectRow.x + IntFieldWidth;
        rectRow.width = ButtonWidth;

        if (GUI.Button(rectRow, IncrementSign))
        {
            ++changeGroupParam;
        }
    }
}

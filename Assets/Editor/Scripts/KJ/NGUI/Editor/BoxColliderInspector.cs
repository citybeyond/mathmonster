﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(BoxCollider))]
public class BoxColliderInspector : Editor
{

	BoxCollider _target;
	void OnEnable()
	{
		_target = target as BoxCollider;
	}

	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		EditorGUILayout.BeginVertical();
		{
			if (GUILayout.Button("Resize box collider"))
			{
				//Undo.RegisterUndo(_target.transform, "Resize box collider");
				Undo.RecordObject(_target.transform, "Resize box collider");

				//NGUITools.AddWidgetCollider(_target.gameObject);
				NGUITools.UpdateWidgetCollider(_target.gameObject);
			}
		}
		EditorGUILayout.EndHorizontal();

	}
}
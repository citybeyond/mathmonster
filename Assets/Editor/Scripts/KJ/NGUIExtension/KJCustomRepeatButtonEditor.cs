﻿using UnityEngine;
using UnityEditor;


[CanEditMultipleObjects]
#if UNITY_3_5
[CustomEditor(typeof(KJCustomRepeatButton))]
#else
[CustomEditor(typeof(KJCustomRepeatButton), true)]
#endif
public class KJCustomRepeatButtonEditor : UIButtonEditor
{
    protected override void DrawProperties()
    {
        base.DrawProperties();

        NGUIEditorTools.DrawProperty("Delay", serializedObject, "delay");
        NGUIEditorTools.DrawProperty("Interval", serializedObject, "interval");

        //SerializedProperty spDelay = serializedObject.FindProperty("delay");
        //SerializedProperty spInterval = serializedObject.FindProperty("interval");
    }
}
